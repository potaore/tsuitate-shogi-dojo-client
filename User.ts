//Copyright (c) 2015-2016 potalong_oreo All rights reserved

import Artifact = require('../potaore/artifact');

export class User extends Artifact.Artifact {
  constructor() {
    super('user');
  }

  startGame(type) {
    this.notify('user-game-start', type);
  }

  createCustomRule() {
    this.notify('user-rule-create');
  }

  startCustomRule(gameId) {
    this.notify('user-custom-start', gameId);
  }
  
  cancelGame() {
    this.notify('user-game-cancel');
  }

  ruleModalOk() {
    this.notify('user-rule-ok');
  }

  ruleModalCancel() {
    this.notify('user-rule-cancel');
  }

  watch(gameId) {
    this.notify('user-game-watch', gameId);
  }
  
  watchByGameId() {
    this.notify('user-watch-by-game-id');
  }

  changeViewPoint() {
    this.notify('user-game-change-viewpoint');
  }

  tryResign() {
    this.notify('user-game-try-resign');
  }

  resign() {
    this.notify('user-game-resign');
  }
  
  back() {
    this.notify('user-back');
  }
  
  listNext() {
    this.notify('user-list-next');
  }
  
  listBack() {
    this.notify('user-list-back');
  }

  exitRoom() {
    this.notify('user-game-exit');
  }

  nariModalNari() {
    this.notify('user-nari-modal-nari');
  }
  
  nariModalNotNari() {
    this.notify('user-nari-modal-not-nari');
  }

  nariModalCancel() {
    this.notify('user-nari-modal-cancel');
  }

  kifuToStart() {
    this.notify('kifu-to-start');
  }

  kifuNext() {
    this.notify('kifu-next');
  }

  kifuBack() {
    this.notify('kifu-back');
  }

  kifuToEnd() {
    this.notify('kifu-to-end');
  }
  
  showGameId(){
    this.notify('user-need-game-id');
  }
  
  showEmbeddedTag(){
    this.notify('user-need-embedded-tag');
  }

  modalOk() {
    this.notify('user-modal-ok');
  }

  modalCancel() {
    this.notify('user-modal-cancel');
  }

  requestConnect(type) {
    this.notify('user-request-connect', type);
    return false;
  }
  
  logout(type) {
    this.notify('user-logout');
    return false;
  }

  requestPlayingGames() {
    this.notify('user-request-playing-games');
  }

  requestOwnHistory() {
    this.notify('user-request-own-history');
  }
  
  requestOwnProfile() {
    this.notify('user-request-own-profile');
  }

  sendMessage(message) {
    this.notify('user-send-message');
  }
}
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var _window = window;
var SoundManager = (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager() {
        _super.call(this, 'sound-manager');
        this.sounds = {};
        this.context = null;
        this.audio = null;
        this.soundsDefs = [
            { name: "koma", system: true, source: "./sound/japanese-chess-piece1.mp3" },
            { name: "foul", system: true, source: "./sound/cancel2.mp3" },
            { name: "start", system: true, source: "./sound/drum-japanese1.mp3" },
            { name: "oute", system: true, source: "./sound/decision10.mp3" },
            { name: "get", system: true, source: "./sound/cursor9.mp3" },
            { name: "拍手1", system: false, source: "./sound/clapping-hands1.mp3" },
            { name: "拍手2", system: false, source: "./sound/clapping-hands2.mp3" }
        ];
        this.loadSounds();
        this.setEvent();
    }
    SoundManager.prototype.loadSounds = function () {
        var _this = this;
        _window.AudioContext = _window.AudioContext || _window.webkitAudioContext;
        if (_window.AudioContext) {
            this.context = new AudioContext();
            this.soundsDefs.forEach(function (soundDef) {
                _this.loadSound(soundDef.source, function (buffer) { _this.sounds[soundDef.name] = buffer; });
            });
        }
        else if (Audio) {
            this.audio = new Audio();
            if (this.audio.canPlayType("audio/mp3") == 'maybe') {
                this.soundsDefs.forEach(function (soundDef) {
                    _this.sounds[soundDef.name] = new Audio(soundDef.source);
                });
            }
            else {
                alert("ご利用のブラウザは、音声出力に対応していません。");
            }
        }
        else {
            alert("ご利用のブラウザは、音声出力に対応していません。");
        }
    };
    SoundManager.prototype.loadSound = function (url, cont) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.responseType = 'arraybuffer';
        request.onload = function () {
            _this.context.decodeAudioData(request.response, function (buffer) {
                cont(buffer);
            }, function (err) {
                console.log(err);
            });
        };
        request.send();
    };
    SoundManager.prototype.playSound = function (name) {
        if (_window.AudioContext) {
            var buffer = this.sounds[name];
            if (buffer) {
                var source = this.context.createBufferSource();
                source.buffer = buffer;
                source.connect(this.context.destination);
                source.start(0);
            }
        }
        else if (Audio) {
            var _audio = this.sounds[name];
            if (_audio)
                _audio.play();
        }
        else {
        }
    };
    SoundManager.prototype.setEvent = function () {
        var _this = this;
        this.on("sound-start", function () { return _this.playSound("start"); });
        this.on("sound-koma", function () { return _this.playSound("koma"); });
        this.on("sound-oute", function () { return _this.playSound("oute"); });
        this.on("sound-get", function () { return _this.playSound("get"); });
        this.on("sound-foul", function () { return _this.playSound("foul"); });
    };
    return SoundManager;
})(Artifact.Artifact);
exports.SoundManager = SoundManager;

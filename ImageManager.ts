//Copyright (c) 2015-2016 potalong_oreo All rights reserved

class Prom {
  nextProm : Prom;
  predicate;
  next(predicate) {
    this.predicate = predicate;
    this.nextProm = new Prom();
    this.nextProm.predicate = predicate();
    return this.nextProm;
  }
  
  exec(cont){
    this.predicate()
  }
}

export class ImageManager {
  komaImages;
  constructor() {
    this.komaImages = {
      'koma1': {},
      'koma2': {}
    };
    /*
    this.komaImages = {
      'koma1': {
        "ou": ImageManager.createKomaImage("sgl01.png"),
        "hi": ImageManager.createKomaImage("sgl02.png"),
        "ka": ImageManager.createKomaImage("sgl03.png"),
        "ki": ImageManager.createKomaImage("sgl04.png"),
        "gi": ImageManager.createKomaImage("sgl05.png"),
        "ke": ImageManager.createKomaImage("sgl06.png"),
        "ky": ImageManager.createKomaImage("sgl07.png"),
        "hu": ImageManager.createKomaImage("sgl08.png"),
        "ry": ImageManager.createKomaImage("sgl12.png"),
        "um": ImageManager.createKomaImage("sgl13.png"),
        "ng": ImageManager.createKomaImage("sgl15.png"),
        "nk": ImageManager.createKomaImage("sgl16.png"),
        "ny": ImageManager.createKomaImage("sgl17.png"),
        "to": ImageManager.createKomaImage("sgl18.png")
      },
      'koma2': {
        "ou": ImageManager.createKomaImage("sgl31.png"),
        "hi": ImageManager.createKomaImage("sgl32.png"),
        "ka": ImageManager.createKomaImage("sgl33.png"),
        "ki": ImageManager.createKomaImage("sgl34.png"),
        "gi": ImageManager.createKomaImage("sgl35.png"),
        "ke": ImageManager.createKomaImage("sgl36.png"),
        "ky": ImageManager.createKomaImage("sgl37.png"),
        "hu": ImageManager.createKomaImage("sgl38.png"),
        "ry": ImageManager.createKomaImage("sgl42.png"),
        "um": ImageManager.createKomaImage("sgl43.png"),
        "ng": ImageManager.createKomaImage("sgl45.png"),
        "nk": ImageManager.createKomaImage("sgl46.png"),
        "ny": ImageManager.createKomaImage("sgl47.png"),
        "to": ImageManager.createKomaImage("sgl48.png")
      }
    };
    */
  }

  load(cont) {
    this.loadKoma('koma1', 'ou', 'sgl01.png')
      .next(() => this.loadKoma('koma1', 'hi', 'sgl02.png')
      .next(() => this.loadKoma('koma1', 'ka', 'sgl03.png')
      .next(() => this.loadKoma('koma1', 'ki', 'sgl04.png')
      .next(() => this.loadKoma('koma1', 'gi', 'sgl05.png')
      .next(() => this.loadKoma('koma1', 'ke', 'sgl06.png')
      .next(() => this.loadKoma('koma1', 'ky', 'sgl07.png')
      .next(() => this.loadKoma('koma1', 'hu', 'sgl08.png')
      .next(() => this.loadKoma('koma1', 'ry', 'sgl12.png')
      .next(() => this.loadKoma('koma1', 'um', 'sgl13.png')
      .next(() => this.loadKoma('koma1', 'ng', 'sgl15.png')
      .next(() => this.loadKoma('koma1', 'nk', 'sgl16.png')
      .next(() => this.loadKoma('koma1', 'ny', 'sgl17.png')
      .next(() => this.loadKoma('koma1', 'to', 'sgl18.png')
      .next(() => this.loadKoma('koma2', 'ou', 'sgl31.png')
      .next(() => this.loadKoma('koma2', 'hi', 'sgl32.png')
      .next(() => this.loadKoma('koma2', 'ka', 'sgl33.png')
      .next(() => this.loadKoma('koma2', 'ki', 'sgl34.png')
      .next(() => this.loadKoma('koma2', 'gi', 'sgl35.png')
      .next(() => this.loadKoma('koma2', 'ke', 'sgl36.png')
      .next(() => this.loadKoma('koma2', 'ky', 'sgl37.png')
      .next(() => this.loadKoma('koma2', 'hu', 'sgl38.png')
      .next(() => this.loadKoma('koma2', 'ry', 'sgl42.png')
      .next(() => this.loadKoma('koma2', 'um', 'sgl43.png')
      .next(() => this.loadKoma('koma2', 'ng', 'sgl45.png')
      .next(() => this.loadKoma('koma2', 'nk', 'sgl46.png')
      .next(() => this.loadKoma('koma2', 'ny', 'sgl47.png')
      .next(() => this.loadKoma('koma2', 'to', 'sgl48.png')
      .next(() => cont()))))))))))))))))))))))))))));
  }

  loadKoma(key, name, fileName) {
    let next = null;
    let cont = {
      next: (predicate) => {
        next = predicate;
        
      }
    };
    let img = ImageManager.createKomaImage(fileName);
    img.onload = () => {
      this.komaImages[key][name] = img;
      img = null;
      if (next) next();
    };
    return cont;
  }

  static createKomaImage(name) {
    let image = new Image()
    image.src = "./images/koma/60x64/" + name;
    image.width = 60;
    image.height = 64;
    return image;
  }

  getKomaImage(koma, flip) {
    if (flip) {
      if (koma.playerNumber === 2) {
        return this.komaImages.koma1[koma.komaType];
      } else {
        return this.komaImages.koma2[koma.komaType];
      }
    } else {
      if (koma.playerNumber === 1) {
        return this.komaImages.koma1[koma.komaType];
      } else {
        return this.komaImages.koma2[koma.komaType];
      }
    }
  }

  getIconImage(url, character): HTMLImageElement {
    var str;
    let image = new Image();
    if (character !== void 0 && character !== null) {
      str = ("0000" + character).slice(-4);
      image.src = "./images/icon/snap" + str + ".png";
      return image;
    } else if (url) {
      image.src = url;
      image.onerror = () =>{
        image.src = "./images/icon/snap0014.png";
      };
      return image;
    } else {
      image.src = './images/icon/noname.jpeg';
      image.onerror = () =>{
        image.src = "./images/icon/snap0014.png";
      };
      return image;
    }
  }

  getRandomCharacter() {
    return Math.floor(Math.random() * 55);
  }

  getRandomNums(length) {
    let result = [];
    let newNum = 0;
    while (result.length < length) {
      newNum = this.getRandomCharacter();
      if (!result.some(item => item == newNum)) {
        result.push(newNum);
      }
    }
    return result;
  }

  getRandomCharacters(num: number): { num: number, image: HTMLImageElement }[] {
    return this.getRandomNums(10).map((num) => { return { num: num, image: this.getIconImage(null, num) }; });
  }
}

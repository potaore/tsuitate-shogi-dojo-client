//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var Socket = (function (_super) {
    __extends(Socket, _super);
    function Socket() {
        var _this = this;
        _super.call(this, 'socket');
        this.on('user-message-send', function (eventName, sender, message) { return _this.socket.emit('message', message); });
        this.on('user-request-playing-games', function (eventName, sender, data) { return _this.socket.emit('getPlayingGames', data); });
        this.on('user-request-own-history', function (eventName, sender, data) { return _this.socket.emit('getHistory', data); });
        this.on('user-request-own-profile', function (eventName, sender, data) { return _this.socket.emit('getProfile', data); });
        this.on('user-game-start', function (eventName, sender, type) {
            console.log('start:' + type);
            _this.socket.emit('room', { method: 'startGame', type: type });
        });
        this.on('user-game-create', function (eventName, sender, type) {
            console.log('start:' + type);
            _this.socket.emit('room', { method: 'startGame', type: type });
        });
        this.on('create-rute', function (eventName, sender, custom) {
            console.log('create rule:' + custom);
            _this.socket.emit('room', { method: 'createGameRule', custom: custom, type: 'custom' });
        });
        this.on('user-custom-start', function (eventName, sender, gameId) {
            console.log('start :' + 'custom');
            _this.socket.emit('room', { method: 'asignCustomGameRule', type: 'custom', gameId: gameId });
        });
        this.on('user-game-restart', function (eventName, sender, type) { return _this.socket.emit('restartGame'); });
        this.on('user-game-cancel', function () { return _this.socket.emit('room', { method: 'cancelGame' }); });
        this.on('user-game-exit', function () { return _this.socket.emit('room', { method: 'exitRoom' }); });
        this.on('user-game-watch', function (eventName, sender, gameId) { return _this.socket.emit('room', { method: 'watch', gameId: gameId }); });
        this.on('user-game-resign', function (eventName, sender, options) { return _this.socket.emit('game', { method: 'resign' }); });
        this.on('user-game-try-move-koma', function (eventName, sender, moveInfo) { return _this.socket.emit('game', { method: 'moveKoma', args: moveInfo }); });
        this.on('user-game-try-put-koma', function (eventName, sender, moveInfo) { return _this.socket.emit('game', { method: 'putKoma', args: moveInfo }); });
        this.on('connect-account', function (eventName, sender, options) {
            _this.connectSocket();
            _this.socket.emit('auth.login', { auth: options.loginstr, character: options.character });
        });
        this.on('connect-guest', function (eventName, sender, options) {
            _this.connectSocket();
            _this.socket.emit('auth.guestlogin', { name: options.loginstr, character: options.character });
        });
        this.on('create-account', function (eventName, sender, options) {
            _this.connectSocket();
            _this.socket.emit('auth.create', { name: options.loginstr, character: options.character });
        });
        this.on('connect-session', function (eventName, sender, sessionId) {
            _this.connectSocket();
            _this.socket.emit('auth.session', sessionId);
        });
        this.on('user-logout', function () { return _this.socket.disconnect(); });
        this.on('game-timeup', function (eventName, sender, options) { return _this.socket.emit('game', options); });
        this.on('game-need-synchronize', function (eventName, sender, options) { return _this.socket.emit('game', { method: 'synchronize' }); });
    }
    Socket.prototype.connectSocket = function () {
        var _this = this;
        if (this.socket) {
            this.socket.connect();
        }
        else {
            //var socket = io("http://133.130.72.92/");
            var socket = io();
            this.socket = socket;
            socket.on('auth.login-ok', function (arg) { return _this.notify('receive-login-ok', arg); });
            socket.on('publishSessionId', function (arg) { return _this.notify('receive-session-id', arg); });
            socket.on('auth.login-ng', function (arg) { return _this.notify('receive-show-modal', { messages: [arg], options: { ok: true } }); });
            socket.on('auth.login-create', function (arg) { return _this.notify('receive-account-created', arg); });
            socket.on('disconnect', function (arg) { return _this.notify('logout', arg); });
            socket.on('message', function (arg) { return _this.notify('receive-message', arg); });
            socket.on('connectionInfo', function (info) { return _this.notify('receive-connection-info', info); });
            socket.on('receivePlayingGames', function (data) { return _this.notify('receive-playing-games', data); });
            socket.on('receiveProfile', function (data) { return _this.notify('receive-profile', data); });
            socket.on('noticePlayerNumber', function (arg) { return _this.notify('receive-game-start', arg); });
            socket.on('gameMessage', function (message) { return _this.notify('receive-game-message', message); });
            socket.on('command', function (command) { return _this.notify('receive-game-command', command); });
            socket.on('synchronize', function (command) { return _this.notify('receive-game-synchronize', command); });
            socket.on('needRestartGame', function (command) { return _this.notify('receive-game-need-restart'); });
            socket.on('restartGame', function (command) { return _this.notify('receive-game-restart', command); });
            socket.on('endGame', function (arg) { return _this.notify('receive-game-end', arg); });
            socket.on('kifu', function (arg) { return _this.notify('receive-kifu-replay', arg); });
            socket.on('distributeKifu', function (arg) {
                _this.notify('kifu-add', arg);
                _this.notify('kifu-to-end', arg);
            });
        }
    };
    return Socket;
})(Artifact.Artifact);
exports.Socket = Socket;

//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var img = require('./ImageManager');
var sound = require('./SoundManager');
var usr = require('./User');
var base = require('./Base');
var board = require('./BoardGraphic');
var _window = window;
var playSound;
var domFinder = {
    getPlayerNameDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerImageDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getGameTurnDiv: function () {
        return $('#game-turn-label');
    },
    getPlayerTimeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeLabel: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getFlipItem: function (playerNumber, flip) {
        return $('.flip-item');
    },
    getNotFlipItem: function (playerNumber, flip) {
        return $('.not-flip-item');
    }
};
$(window).load(function () {
    var imageManager = new img.ImageManager();
    var user = new usr.User();
    var view = new View(imageManager, user);
    view.listen(user);
    var gameInfoView = new GameInfoView();
    gameInfoView.connect(view).listen(user);
    var gameBoard = new board.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager, domFinder);
    gameBoard.listen(user).connect(view).connect(gameInfoView);
    var soundManager = new sound.SoundManager();
    soundManager
        .listen(user)
        .listen(view)
        .listen(gameInfoView)
        .listen(gameBoard);
    app = user;
    {
        var logger = new Artifact.Listner({ info: function (data) { return console.log(data); } }, false);
        logger.listen(soundManager);
        logger.listen(user);
        logger.listen(view);
    }
    imageManager.load(function () { return view.getKifu(); });
});
var View = (function (_super) {
    __extends(View, _super);
    function View(imageManager, user) {
        _super.call(this, 'view');
        this.imageManager = imageManager;
        this.user = user;
    }
    View.prototype.getKifu = function () {
        var _this = this;
        var arg = {};
        var pair = location.search.substring(1).split('&');
        for (var i = 0; pair[i]; i++) {
            var kv = pair[i].split('=');
            arg[kv[0]] = kv[1];
        }
        $.ajax({
            url: 'kifu/find?gameId=' + arg.gameId
        }).done(function (data) {
            if (data.error) {
                _this.showModal(['棋譜が見つかりませんでした'], [], { ok: false, cancel: false, notApplyToGameMessageArea: true });
            }
            else {
                _this.notify('receive-kifu-replay', { kifu: data });
                if (arg.viewpoint) {
                    _this.notify('change-viewpoint', arg.viewpoint);
                }
                if (arg.index) {
                    _this.notify('kifu-to', arg.index);
                }
            }
            console.log(data);
        }).fail(function (data) {
            _this.showModal(['棋譜が見つかりませんでした'], [], { ok: false, cancel: false, notApplyToGameMessageArea: true });
        });
    };
    View.prototype.showModal = function (texts, boxes, option, afterOk, afterCancel) {
        if (afterOk === void 0) { afterOk = function () { }; }
        if (afterCancel === void 0) { afterCancel = function () { }; }
        this.afterModalOk = afterOk;
        this.afterModalCancel = afterCancel;
        if (option.ok) {
            $("#modalConfirmButton").show();
        }
        else {
            $("#modalConfirmButton").hide();
        }
        if (option.cancel) {
            $("#modalCancelButton").show();
        }
        else {
            $("#modalCancelButton").hide();
        }
        if (!option.notApplyToGameMessageArea) {
            $("#game-message-area").empty();
        }
        $("#modal-message").empty();
        $("#modal-box").empty();
        $("#modal-window").show();
        $("#modal-overlay").show();
        $("#modal-window").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-window").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
        $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
        texts.forEach(function (text) {
            if (!option.notApplyToGameMessageArea) {
                $("#game-message-area").append($('<span>').text(text));
            }
            $("#modal-message").append($('<div>').text(text));
        });
        if (boxes) {
            boxes.forEach(function (text) {
                if (!option.notApplyToGameMessageArea) {
                    $("#game-message-area").append($('<span>').text(text));
                }
                if (option.boxReadonly) {
                    $("#modal-box").append($('<input type="text" class="modal-textbox" readonly>').val(text));
                }
                else {
                    $("#modal-box").append($('<input type="text" class="modal-textbox">').val(text));
                }
            });
        }
    };
    return View;
})(Artifact.Artifact);
var GameInfoView = (function (_super) {
    __extends(GameInfoView, _super);
    function GameInfoView() {
        var _this = this;
        _super.call(this, 'game-info-view');
        this.playerInfoMemo = null;
        this.on('receive-game-message', function (eventName, sender, message) { return _this.showGameMessage(message); });
        this.on('receive-kifu-replay', function (eventName, sender, data) { return _this.replayKifu(data.kifu); });
        this.on('kifu-add', function (eventName, sender, data) { return _this.addKifu(data.te); });
        this.on('kifu-to-start', function (eventName, sender, options) { return _this.kifuToStart(); });
        this.on('kifu-back', function (eventName, sender, options) { return _this.kifuBack(); });
        this.on('kifu-next', function (eventName, sender, options) { return _this.kifuNext(); });
        this.on('kifu-to-end', function (eventName, sender, options) { return _this.kifuToEnd(); });
        this.on('kifu-to', function (eventName, sender, index) { return _this.kifu.toIndex(index); });
        this.on('change-viewpoint', function (eventName, sender, viewPointCode) {
            if (viewPointCode == '1') {
                $('#viewPointSelectBox').val('先手視点');
            }
            else if (viewPointCode == '2') {
                $('#viewPointSelectBox').val('後手視点');
            }
            else if (viewPointCode == '3') {
                $('#viewPointSelectBox').val('先手視点（ブラインド）');
            }
            else if (viewPointCode == '4') {
                $('#viewPointSelectBox').val('後手視点（ブラインド）');
            }
            _this.notify('game-blind-koma', _this.getViewPoint());
        });
        this.on('user-game-change-viewpoint', function () {
            _this.notify('game-blind-koma', _this.getViewPoint());
        });
    }
    GameInfoView.prototype.showGameMessage = function (message) {
        this.showGameMessageConsole(message);
    };
    GameInfoView.prototype.showGameMessageConsole = function (message) {
        $("#game-message-area").empty();
        message.texts.forEach(function (text) { return $("#game-message-area").append($('<li>').text(text + '.　')); });
        message.texts.forEach(function (text) { return $("#game-history").append($('<li>').text(text + '.　')); });
        $('#game-history-div').scrollTop($("#game-history-area").height());
    };
    GameInfoView.prototype.showEvents = function (board, blind) {
        var texts = [];
        var myTurn = (board.turn % 2 + 1) == blind;
        var foul = false;
        var end = false;
        if (blind != 0 && board.events) {
            board.events.forEach(function (ev) {
                if (ev.type == "get") {
                    if (myTurn) {
                        texts.push(base.getKomaName(ev.komaType) + "を取りました");
                    }
                    else {
                        texts.push(base.komaTypes[ev.komaType].name + "を取られました");
                    }
                }
                else if (ev.type == "oute") {
                    if (myTurn) {
                        texts.push("王手をかけました");
                    }
                    else {
                        texts.push("王手をかけられました");
                    }
                }
                else if (ev.type == "foul") {
                    foul = true;
                    if (myTurn) {
                        texts.push("反則です");
                    }
                    else {
                        texts.push("相手が反則手を指しました");
                    }
                }
                else if (ev.type == "endGame") {
                    end = true;
                    if (ev.winPlayerNumber == 3 - blind) {
                        texts.push("あなたの勝ちです");
                    }
                    else {
                        texts.push("あなたの負けです");
                    }
                }
            });
            if (!end) {
                if ((myTurn && !foul) || (!myTurn && foul)) {
                    texts.push("相手の手番です");
                }
                else {
                    texts.push("あなたの手番です");
                }
            }
        }
        this.showGameMessage({ type: 'plain', texts: texts });
    };
    GameInfoView.prototype.getViewPoint = function () {
        var value = $('#viewPointSelectBox option:selected').val();
        if (value == "先手視点") {
            return { "flip": false, "blind": 0 };
        }
        else if (value == "後手視点") {
            return { "flip": true, "blind": 0 };
        }
        else if (value == "先手視点（ブラインド）") {
            return { "flip": false, "blind": 2 };
        }
        else if (value == "後手視点（ブラインド）") {
            return { "flip": true, "blind": 1 };
        }
    };
    GameInfoView.prototype.replayKifu = function (arg) {
        var _this = this;
        this.notify('game-board-init');
        $('#kifuSelectBox').empty();
        $("#kifuInfoDiv").show();
        $("#kifuReplayDiv").show();
        $('.game-playing').hide();
        $('.game-watching').show();
        var refresh = function (index, board) {
            _this.notify('game-update-info', { player1: board.player1, player2: board.player2 });
            _this.notify('game-set-turn', board.turn ? board.turn : 0);
            $('#kifuSelectBox').val(index);
            var kifuLength = $("#kifuSelectBox").children().length - 1;
            _this.notify('game-board-set-invated-position', board.emphasisPosition);
            _this.notify('game-redraw-board', { board: board, turn: 1, state: 'replay' });
            $('#toStartButton').prop('disabled', index == 0);
            $('#backButton').prop('disabled', index == 0);
            $('#nextButton').prop('disabled', kifuLength == index);
            $('#toEndButton').prop('disabled', kifuLength == index);
            var viewPoint = _this.getViewPoint();
            _this.showEvents(board, viewPoint.blind);
        };
        this.disposeKifu();
        this.kifu = new base.Kifu(arg.kifu, 1, refresh, arg.playerInfo.player1, arg.playerInfo.player2, arg.custom);
        this.currentGameId = arg.gameId;
        this.kifu.synchronizeConnectionInfo(this).connect(this);
        this.kifu.activateUpdateTimer();
        this.notify('game-redraw-board', { board: this.kifu.getCurrentBoard(), turn: 1, state: "replay" });
        var index = 0;
        $('#kifuSelectBox').unbind("change");
        $('#kifuSelectBox').bind("change", function () {
            index = $('#kifuSelectBox option:selected').val();
            _this.kifu.toIndex(parseInt(index));
        });
        var option = $("<option value='" + index + "'>対局開始</option>");
        index++;
        $("#kifuSelectBox").append(option);
        arg.kifu.forEach(function (te) {
            if (te.info.type == "moveKoma") {
                if (!te.foul) {
                    var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
                }
                else {
                    var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
                }
                option = $("<option value='" + index + "'>" + value + "</option>");
            }
            else if (te.info.type == "endGame") {
                value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
                option = $("<option value='" + index + "'>" + value + "</option>");
            }
            index++;
            $("#kifuSelectBox").append(option);
        });
        this.kifu.toEnd();
        this.notify('game-blind-koma', this.getViewPoint());
    };
    GameInfoView.prototype.disposeKifu = function () {
        if (this.kifu) {
            this.kifu.disconnectAll();
        }
    };
    GameInfoView.prototype.addKifu = function (te) {
        this.kifu.addKifu(te);
        var i = $("#kifuSelectBox").children().length;
        if (te.info.type == "moveKoma") {
            if (!te.foul) {
                this.notify("sound-koma");
                var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
            }
            else {
                this.notify("sound-foul");
                var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
            }
            var option = $("<option value='" + i + "'>" + value + "</option>");
        }
        else if (te.info.type == "endGame") {
            value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
            var option = $("<option value=' " + i + "}'>" + value + "</option>");
        }
        $("#kifuSelectBox").append(option);
    };
    GameInfoView.prototype.kifuToStart = function () {
        this.kifu.toStart();
    };
    GameInfoView.prototype.kifuBack = function () {
        this.kifu.back();
    };
    GameInfoView.prototype.kifuNext = function () {
        this.kifu.next();
    };
    GameInfoView.prototype.kifuToEnd = function () {
        this.kifu.toEnd();
    };
    return GameInfoView;
})(Artifact.Artifact);

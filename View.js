//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var img = require('./ImageManager');
var sound = require('./SoundManager');
var usr = require('./User');
var base = require('./Base');
var wbase = require('./WebBase');
var sckt = require('./Socket');
var board = require('./BoardGraphic');
var session = require('./SessionManager');
var _window = window;
var playSound;
var domFinder = {
    getPlayerNameDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerImageDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getGameTurnDiv: function () {
        return $('#game-turn-label');
    },
    getPlayerTimeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeLabel: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getFlipItem: function (playerNumber, flip) {
        return $('.flip-item');
    },
    getNotFlipItem: function (playerNumber, flip) {
        return $('.not-flip-item');
    }
};
$(window).load(function () {
    var imageManager = new img.ImageManager();
    var user = new usr.User();
    var pages = new Pages(user);
    pages.listen(user);
    var chatSender = new ChatSender();
    var view = new View(imageManager, pages, user);
    view.listen(user);
    var gameInfoView = new GameInfoView();
    gameInfoView.connect(view).listen(user);
    var socket = new sckt.Socket();
    socket.listen(user).connect(view).connect(gameInfoView).listen(chatSender);
    var gameBoard = new board.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager, domFinder);
    gameBoard.listen(user).connect(socket).connect(view).connect(gameInfoView);
    var soundManager = new sound.SoundManager();
    soundManager
        .listen(user)
        .listen(view)
        .listen(gameInfoView)
        .listen(socket)
        .listen(gameBoard);
    app = user;
    {
        var logger = new Artifact.Listner({ info: function (data) { return console.log(data); } }, false);
        logger.listen(soundManager);
        logger.listen(user);
        logger.listen(view);
        logger.listen(socket);
    }
    imageManager.load(function () { return view.setIconSelector(); });
    var sessionId = session.get('sessionId');
    if (sessionId) {
        view.notify('connect-session', { sessionId: sessionId });
    }
});
var Paging = (function () {
    function Paging() {
    }
    Paging.login = function () {
        $('.state-logout').hide();
        $('#navbar').show();
        Paging.topMenu();
    };
    Paging.logout = function () {
        Paging.currentPage = Paging.pages.loginForm;
        $('.state-login').hide();
        $('.state-logout').show();
        Paging.history = [{ name: Paging.pages.loginForm, options: {} }];
    };
    Paging.topMenu = function () {
        if (Paging.currentPage == Paging.pages.gameBoard) {
            $('.lobby-chace').empty();
        }
        Paging.currentPage = Paging.pages.topMenu;
        $('#top-menu').show();
        $('#game-board').hide();
        $('#playing-games').hide();
        $('#own-history').hide();
        $('#own-profile').hide();
        $('.lobby-item').show();
        $('.disable-playing').prop('disabled', false);
        Paging.history.push({ name: Paging.pages.topMenu, options: {} });
    };
    Paging.playingGames = function () {
        if (Paging.currentPage == Paging.pages.gameBoard) {
            $('.lobby-chace').empty();
        }
        Paging.currentPage = Paging.pages.playingGames;
        $('#top-menu').hide();
        $('#game-board').hide();
        $('#playing-games').show();
        $('#own-history').hide();
        $('#own-profile').hide();
        $('.lobby-item').show();
        $('.disable-playing').prop('disabled', false);
        Paging.history.push({ name: Paging.pages.playingGames, options: {} });
    };
    Paging.ownHistory = function () {
        if (Paging.currentPage == Paging.pages.gameBoard) {
            $('.lobby-cache').empty();
        }
        Paging.currentPage = Paging.pages.ownHistory;
        $('#top-menu').hide();
        $('#game-board').hide();
        $('#playing-games').hide();
        $('#own-history').show();
        $('#own-profile').hide();
        $('.lobby-item').show();
        $('.disable-playing').prop('disabled', false);
        Paging.history.push({ name: Paging.pages.ownHistory, options: {} });
    };
    Paging.ownProfile = function () {
        if (Paging.currentPage == Paging.pages.gameBoard) {
            $('.lobby-cache').empty();
        }
        Paging.currentPage = Paging.pages.ownProfile;
        $('#top-menu').hide();
        $('#game-board').hide();
        $('#playing-games').hide();
        $('#own-history').hide();
        $('#own-profile').show();
        $('.lobby-item').show();
        $('.disable-playing').prop('disabled', false);
        Paging.history.push({ name: Paging.pages.ownProfile, options: {} });
    };
    Paging.gameBoard = function () {
        if (Paging.currentPage == Paging.pages.gameBoard)
            return;
        Paging.currentPage = Paging.pages.gameBoard;
        $('#top-menu').hide();
        $('#game-board').show();
        $('#playing-games').hide();
        $('#own-history').hide();
        $('#own-profile').hide();
        $('.lobby-item').hide();
        $('.lobby-cache').empty();
        $('input[name=blind]').val(["先手視点"]);
        $('.disable-playing').prop('disabled', true);
        Paging.history.push({ name: Paging.pages.gameBoard, options: {} });
    };
    Paging.exitRoom = function () {
        if (Paging.currentPage == Paging.pages.gameBoard) {
            $('.lobby-cache').empty();
            $('.game-cache').empty();
            Paging.back();
        }
    };
    Paging.back = function () {
        if (Paging.history.length >= 2) {
            var page = Paging.history[Paging.history.length - 2];
            Paging.history.pop();
            Paging.history.pop();
            switch (page.name) {
                case Paging.pages.loginForm:
                    //Paging.logout()
                    break;
                case Paging.pages.topMenu:
                    Paging.topMenu();
                    break;
                case Paging.pages.playingGames:
                    Paging.playingGames();
                    break;
                case Paging.pages.ownHistory:
                    Paging.ownHistory();
                    break;
                case Paging.pages.gameBoard:
                    Paging.gameBoard();
                    break;
                default:
                    Paging.topMenu();
                    break;
            }
        }
    };
    Paging.pages = {
        loginForm: 'login-form',
        topMenu: 'top-menu',
        playingGames: 'playing-games',
        waitingGames: 'waiting-games',
        ownHistory: 'own-history',
        ownProfile: 'own-profile',
        gameBoard: 'game-board'
    };
    Paging.currentPage = Paging.pages.loginForm;
    Paging.history = [{ name: Paging.pages.loginForm, options: {} }];
    return Paging;
})();
var View = (function (_super) {
    __extends(View, _super);
    function View(imageManager, pages, user) {
        var _this = this;
        _super.call(this, 'view');
        this.imageManager = imageManager;
        this.pages = pages;
        this.user = user;
        this.waiting = false;
        this.on('user-request-connect', function (eventName, sender, type) { _this.login(type); });
        this.on('user-logout', function (eventName, sender, type) {
            _this.cancelWaiting();
            Paging.logout();
        });
        this.on('logout', function () {
            if (Paging.currentPage != Paging.pages.loginForm) {
                _this.cancelWaiting();
                _this.showModal(['サーバーとの接続が切れました'], null, { ok: true, cancel: false }, function () { return Paging.logout(); });
            }
        });
        this.on('receive-login-ok', function (eventName, sender, arg) {
            Paging.login();
            _this.updateLoginInfo(arg);
            if (arg.account && arg.account.tid) {
                $(".account-only").show();
            }
            else {
                $(".account-only").hide();
            }
        });
        this.on('receive-connection-info', function (eventName, sender, options) { _this.updateConnectionInfo(options); });
        this.on('user-request-playing-games', function () {
            _this.pages.initPage();
            Paging.playingGames();
        });
        this.on('receive-playing-games', function (eventName, sender, data) { return _this.showPlayingGames(data); });
        this.on('user-request-own-history', function () {
            _this.pages.initPage();
            Paging.ownHistory();
        });
        this.on('user-watch-by-game-id', function () {
            _this.showModal(['ゲームIDを入力してください'], [''], { ok: true, cancel: true }, function () {
                user.watch($('#modal-box').find('input').val());
            });
        });
        this.on('receive-account-created', function (eventName, sender, arg) {
            _this.showModal([arg.message], [arg.account.tid + ':' + arg.account.password], { ok: true, cancel: false, boxReadonly: true }, function () {
                user.watch($('#modal-box').find('input').val());
            });
        });
        this.on('user-request-own-profile', function () {
            Paging.ownProfile();
        });
        this.on('receive-message', function (eventName, sender, message) { _this.addMessage(message); });
        this.on('receive-profile', function (eventName, sender, data) { _this.setProfile(data); });
        this.on('user-back', function (eventName, sender, type) { return Paging.back(); });
        this.on('user-game-start', function (eventName, sender, type) { return _this.gameStart(type); });
        this.on('user-custom-start', function (eventName, sender, gameId) { return _this.customStart(gameId); });
        this.on('user-rule-create', function (eventName, sender, type) { return _this.showCustomRuleModal(); });
        this.on('user-rule-ok', function (eventName, sender, type) { return _this.createRule(); });
        this.on('user-rule-cancel', function (eventName, sender, type) { return _this.hideCustomRuleModal(); });
        this.on('user-game-cancel', function (eventName, sender, type) { return _this.cancelWaiting(); });
        this.on('user-game-try-resign', function (eventName, sender, type) { return _this.showModal(['投了しますか？'], [], { ok: true, cancel: true, notApplyToGameMessageArea: true }, function () { return _this.notify('user-game-resign'); }, function () { }); });
        this.on('receive-game-end', function (eventName, sender, options) { return _this.notify('user-game-cancel'); });
        this.on('receive-kifu-replay', function () {
            $('.game-cache').empty();
            Paging.gameBoard();
        });
        this.on('receive-game-start', function (eventName, sender, arg) {
            _this.cancelWaiting();
            Paging.gameBoard();
            _this.hideModal();
            $('.game-cache').empty();
        });
        this.on('receive-game-need-restart', function (eventName, sender, options) {
            _this.showModal(['ゲームを再開します'], [], { ok: true, cancel: false, notApplyToGameMessageArea: true }, function () { return _this.notify('user-game-restart'); }, function () { });
        });
        this.on('receive-game-restart', function (eventName, sender, arg) {
            Paging.gameBoard();
            _this.hideModal();
            $('.game-cache').empty();
        });
        this.on('user-game-exit', function (eventName, sender, type) { return Paging.exitRoom(); });
        this.on('view-modal-show', function (eventName, sender, data) {
            if (data.options.error) {
                _this.showModal(data.texts, data.boxes, { ok: data.options.ok, cancel: data.options.cancel }, function () { return Paging.exitRoom(); }, function () { return Paging.exitRoom(); });
            }
            else {
                _this.showModal(data.texts, data.boxes, { ok: data.options.ok, cancel: data.options.cancel }, function () { }, function () { });
            }
        });
        this.on('receive-show-modal', function (eventName, sender, data) { return _this.showModal(data.messages, data.boxes, data.options); });
        this.on('show-modal', function (eventName, sender, data) { return _this.showModal(data.messages, data.boxes, data.options, data.afterOk, data.afterCancel); });
        this.on('user-modal-ok', function (eventName, sender, options) {
            _this.hideModal();
            if (_this.afterModalOk)
                _this.afterModalOk();
        });
        this.on('receive-session-id', function (eventName, sender, sessionId) {
            session.set('sessionId', sessionId, 30);
        });
        this.on('user-modal-cancel', function (eventName, sender, options) {
            _this.hideModal();
            if (_this.afterModalCancel)
                _this.afterModalCancel();
        });
    }
    View.prototype.setIconSelector = function () {
        var _this = this;
        $("#icon-selector").empty();
        var images = this.imageManager.getRandomCharacters(10).map(function (data) { return { num: data.num, image: $(data.image) }; });
        images.forEach(function (data) {
            data.image.addClass("iconCandidate");
            $("#icon-selector").append(data.image);
            data.image.on("click", function () {
                _this.selectedCharacter = data.num;
                $(".iconCandidate").css({ border: "none" });
                data.image.css({ border: "solid" });
            });
        });
    };
    ;
    View.prototype.updateLoginInfo = function (arg) {
        $("#account-icon").empty();
        $("#account-icon").append(this.imageManager.getIconImage(arg.account.profile_url, arg.account.character));
        $("#user-name").empty();
        $("#user-name").append(wbase.replaceToRuby(arg.account.name));
        $("#lobby-profile").hide();
    };
    View.prototype.gameStart = function (type) {
        $('#game-waiting').show();
        $('.game-start-button').prop('disabled', true);
        $('#game-waiting-label-free').hide();
        $('#game-waiting-label-ai').hide();
        $('#game-waiting-label-rating').hide();
        if (type == 'free') {
            $('#game-waiting-label-free').show();
        }
        else if (type == 'bot') {
            $('#game-waiting-label-ai').show();
        }
        else if (type = 'rating') {
            $('#game-waiting-label-rating').show();
        }
        this.waiting = true;
    };
    View.prototype.customStart = function (gameId) {
    };
    View.prototype.createRule = function () {
        var hirate_teai = $('input[name="hirate-teai"]:checked').val();
        var turn = $('input[name="turn"]:checked').val();
        var time = $('#customrule-time').val();
        var life = $('#customrule-life').val();
        var time2 = $('#customrule-time2').val();
        var life2 = $('#customrule-life2').val();
        if (time === "")
            time = 10;
        if (time < 1)
            time = 1;
        if (time > 60)
            time = 60;
        if (life === "")
            life = 9;
        if (life < 0)
            life = 0;
        if (life > 99)
            life = 99;
        if (hirate_teai == "hirate") {
            time2 = time;
            life2 = time;
        }
        else {
            if (time2 === "")
                time2 = 10;
            if (time2 < 1)
                time2 = 1;
            if (time2 > 60)
                time2 = 60;
            if (life2 === "")
                life2 = 9;
            if (life2 < 0)
                life2 = 0;
            if (life2 > 99)
                life2 = 99;
        }
        this.hideCustomRuleModal();
        $('#game-waiting').show();
        $('.game-start-button').prop('disabled', true);
        $('#game-waiting-label-free').hide();
        $('#game-waiting-label-ai').hide();
        $('#game-waiting-label-rating').hide();
        $('#game-waiting-label-custom').show();
        this.notify('create-rute', { turn: turn, hirate_teai: hirate_teai, player1: { time: time, life: life }, player2: { time: time2, life: life2 } });
        this.waiting = true;
    };
    View.prototype.cancelWaiting = function () {
        $('#game-waiting').hide();
        $('.game-start-button').prop('disabled', false);
        this.waiting = false;
    };
    View.prototype.addMessage = function (arg) {
        if (arg.type === "system-info") {
            $(".chat-messages").append($('<li>').addClass('system-info').text(arg.message));
        }
        else {
            $(".chat-messages").append($('<li>').text(arg.message));
        }
        $('.chat-messages-div').scrollTop(View.max($('#messages').height(), $('#messages-lobby').height()));
    };
    View.max = function (a, b) {
        return a > b ? a : b;
    };
    View.prototype.updateConnectionInfo = function (info) {
        $("#connection-count").empty();
        $("#connection-count").append(info.connectionCount);
        $("#playing-game-count").empty();
        $("#playing-game-count").append(info.playingGameCount);
        $("#matching-game-count-free").empty();
        $("#matching-game-count-bot").empty();
        $("#matching-game-count-rating").empty();
        $("#matching-game-count-free").append(info.matchingGameCount);
        $("#matching-game-count-bot").append(info.matchingGameBotCount);
        $("#matching-game-count-rating").append(info.matchingGameRatingCount);
    };
    View.prototype.setProfile = function (data) {
        $("#table-own-profile-body").empty();
        this.appendProfile('ユーザー名', data.account.name);
        if (data.account.rate) {
            this.appendProfile('レート', data.account.rate);
            this.appendProfile('レーティング対局：勝数', data.account.results.rating.win);
            this.appendProfile('レーティング対局：負数', data.account.results.rating.loose);
            this.appendProfile('フリー対局：勝数', data.account.results.free.win);
            this.appendProfile('フリー対局：負数', data.account.results.free.loose);
        }
    };
    View.prototype.appendProfileTextbox = function (key, value) {
        var tr = $("<tr>");
        tr.append($("<td>").append(key));
        tr.append($("<td>").append($('<input type="text" id="profile-' + key + '" style="width:320px">').val(value)));
        $("#table-own-profile-body").append(tr);
    };
    View.prototype.appendProfile = function (key, value) {
        var tr = $("<tr>");
        tr.append($("<td>").append(key));
        tr.append($("<td>").append(value));
        $("#table-own-profile-body").append(tr);
    };
    View.prototype.login = function (type) {
        if (type === "account") {
            this.notify('connect-account', { loginstr: $("#loginstr").val().trim(), character: this.selectedCharacter, subCharacter: this.imageManager.getRandomCharacters(1)[0].num });
        }
        else if (type === "create") {
            this.notify('create-account', { loginstr: $("#loginstr").val().trim(), character: this.selectedCharacter });
        }
        else {
            if (this.selectedCharacter === null || this.selectedCharacter === undefined) {
                this.selectedCharacter = this.imageManager.getRandomCharacters(1)[0].num;
            }
            this.notify('connect-guest', { loginstr: $("#loginstr").val().trim(), character: this.selectedCharacter });
        }
    };
    ;
    View.prototype.showPlayingGames = function (data) {
        this.pages.contents = data.playingGames;
        if ($(".table-rooms-body").children().length == 0) {
            this.pages.drawContents();
        }
        else {
            this.pages.drawContents();
        }
        this.pages.activatePageButton();
        if (this.waiting) {
            $('.game-start-button').prop('disabled', true);
        }
        else {
            $('.game-start-button').prop('disabled', false);
        }
    };
    View.prototype.showCustomRuleModal = function () {
        $("#customrule-modal-window").show();
        $("#modal-overlay").show();
    };
    View.prototype.hideCustomRuleModal = function () {
        $("#customrule-modal-window").hide();
        $("#modal-overlay").hide();
    };
    View.prototype.showModal = function (texts, boxes, option, afterOk, afterCancel) {
        if (afterOk === void 0) { afterOk = function () { }; }
        if (afterCancel === void 0) { afterCancel = function () { }; }
        this.afterModalOk = afterOk;
        this.afterModalCancel = afterCancel;
        if (option.ok) {
            $("#modalConfirmButton").show();
        }
        else {
            $("#modalConfirmButton").hide();
        }
        if (option.cancel) {
            $("#modalCancelButton").show();
        }
        else {
            $("#modalCancelButton").hide();
        }
        if (!option.notApplyToGameMessageArea) {
            $("#game-message-area").empty();
        }
        $("#modal-message").empty();
        $("#modal-box").empty();
        $("#modal-window").show();
        $("#modal-overlay").show();
        $("#modal-window").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-window").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
        $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
        texts.forEach(function (text) {
            if (!option.notApplyToGameMessageArea) {
                $("#game-message-area").append($('<span>').text(text));
            }
            $("#modal-message").append($('<div>').text(text));
        });
        if (boxes) {
            boxes.forEach(function (text) {
                if (!option.notApplyToGameMessageArea) {
                    $("#game-message-area").append($('<span>').text(text));
                }
                if (option.boxReadonly) {
                    $("#modal-box").append($('<input type="text" class="modal-textbox" onclick="this.select(0,this.value.length)" readonly>').val(text));
                }
                else {
                    $("#modal-box").append($('<input type="text" class="modal-textbox">').val(text));
                }
            });
        }
    };
    View.prototype.hideModal = function () {
        $("#modal-window").hide();
        $("#modal-overlay").hide();
    };
    return View;
})(Artifact.Artifact);
var GameInfoView = (function (_super) {
    __extends(GameInfoView, _super);
    function GameInfoView() {
        var _this = this;
        _super.call(this, 'game-info-view');
        this.playerInfoMemo = null;
        this.on('user-nari-modal-nari', function (eventName, sender, options) {
            _this.hideNariModal();
            if (_this.afterNariModalNari)
                _this.afterNariModalNari();
        });
        this.on('user-nari-modal-not-nari', function (eventName, sender, options) {
            _this.hideNariModal();
            if (_this.afterNariModalNotNari)
                _this.afterNariModalNotNari();
        });
        this.on('user-nari-modal-cancel', function (eventName, sender, options) {
            _this.hideNariModal();
            if (_this.afterNariModalCancel)
                _this.afterNariModalCancel();
        });
        this.on('board-show-nari-modal', function (eventName, sender, options) { return _this.showNariModal(options.nari, options.notNari, options.cancel); });
        this.on("game-show-message", function (eventName, sender, options) { return _this.showGameMessageConsole(options); });
        this.on('receive-game-message', function (eventName, sender, message) { return _this.showGameMessage(message); });
        this.on('receive-game-start', function (eventName, sender, options) { return _this.startGame(options); });
        this.on('receive-game-end', function (eventName, sender, options) { return _this.endGame(options); });
        this.on('receive-game-restart', function (eventName, sender, options) { return _this.restartGame(options); });
        this.on('receive-kifu-replay', function (eventName, sender, data) { return _this.replayKifu(data.kifu); });
        this.on('kifu-add', function (eventName, sender, data) { return _this.addKifu(data.te); });
        this.on('kifu-to-start', function (eventName, sender, options) { return _this.kifuToStart(); });
        this.on('kifu-back', function (eventName, sender, options) { return _this.kifuBack(); });
        this.on('kifu-next', function (eventName, sender, options) { return _this.kifuNext(); });
        this.on('kifu-to-end', function (eventName, sender, options) { return _this.kifuToEnd(); });
        this.on('user-need-game-id', function () {
            _this.notify('show-modal', {
                messages: ['ゲームID'],
                boxes: [_this.currentGameId],
                options: { ok: true, cancel: false, notApplyToGameMessageArea: true, boxReadonly: true }
            });
        });
        this.on('user-need-embedded-tag', function () {
            var gameId = _this.currentGameId;
            var index = $('#kifuSelectBox option:selected').val();
            var viewPointCode = _this.getViewPointCode();
            var tag = '<iframe width="580" height="420" src="http://potaore.github.io/apps/tsuitate-shogi-dojo/tsuitate-embedded.html?gameId=' + gameId + '&amp;index=' + index + '&amp;viewpoint=' + viewPointCode + '" frameborder="0"></iframe>';
            _this.notify('show-modal', {
                messages: ['埋め込みタグ', '※現在の局面、視点が再現されます'],
                boxes: [tag],
                options: { ok: true, cancel: false, notApplyToGameMessageArea: true, boxReadonly: true }
            });
        });
        this.on('user-game-change-viewpoint', function () {
            _this.notify('game-blind-koma', _this.getViewPoint());
        });
    }
    GameInfoView.prototype.showGameMessage = function (message) {
        if (message.type == "plain") {
            this.showGameMessageConsole(message);
        }
        else if (message.type == "modal") {
            this.notify('view-modal-show', { texts: message.texts, options: { ok: true, cancel: false, error: message.error } });
        }
    };
    GameInfoView.prototype.showNariModal = function (afterNari, afterNotNari, afterCancel) {
        if (afterNari === void 0) { afterNari = function () { }; }
        if (afterNotNari === void 0) { afterNotNari = function () { }; }
        if (afterCancel === void 0) { afterCancel = function () { }; }
        this.afterNariModalNari = afterNari;
        this.afterNariModalNotNari = afterNotNari;
        this.afterNariModalCancel = afterCancel;
        $("#nariDiv").show();
        $("#modal-overlay").show();
        $("#nariDiv").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
        $("#nariDiv").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
        $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
    };
    GameInfoView.prototype.hideNariModal = function () {
        $("#modal-overlay").hide();
        $("#nariDiv").hide();
    };
    GameInfoView.prototype.showGameMessageConsole = function (message) {
        $("#game-message-area").empty();
        message.texts.forEach(function (text) { return $("#game-message-area").append($('<span>').text(text + '.　')); });
        message.texts.forEach(function (text) { return $("#game-history").append($('<li>').text(text + '.　')); });
        $('#game-history-div').scrollTop($("#game-history-area").height());
    };
    GameInfoView.prototype.startGame = function (args) {
        this.disposeGame();
        this.disposeKifu();
        this.game = new base.Game(args.playerNumber, args.playerInfo.player1, args.playerInfo.player2);
        this.currentGameId = args.gameId;
        this.game.synchronizeConnectionInfo(this).connect(this);
        this.game.start();
        this.notify('game-board-init');
        this.notify('game-board-flip', args.playerNumber != 1);
        this.notify('game-redraw-board', this.game);
        $('.game-playing').show();
        $('.game-watching').hide();
        this.notify('sound-start');
    };
    GameInfoView.prototype.restartGame = function (args) {
        this.disposeGame();
        this.disposeKifu();
        this.game = new base.Game(args.playerNumber, args.playerInfo.player1, args.playerInfo.player2);
        this.currentGameId = args.gameId;
        this.game.synchronizeConnectionInfo(this).connect(this);
        this.game.start();
        this.game.synchronizeGameInfo(args.board, args.turn, args.playerInfo, args.elapsedTime);
        this.notify('game-board-init');
        this.notify('game-board-flip', args.playerNumber != 1);
        this.notify('game-restart', args);
        this.notify('game-redraw-board', this.game);
        $('.game-playing').show();
        $('.game-watching').hide();
        this.showGameMessage(args.message);
    };
    GameInfoView.prototype.disposeGame = function () {
        if (this.game) {
            this.game.disconnectAll();
        }
    };
    GameInfoView.prototype.endGame = function (args) {
        $('.game-playing').hide();
        $('.game-watching').show();
        if (this.game.playerNumber == 1) {
            $('input[name=blind]').val(["先手視点"]);
        }
        else {
            $('input[name=blind]').val(["後手視点"]);
        }
        this.replayKifu(args);
    };
    GameInfoView.prototype.showEvents = function (board, blind) {
        var texts = [];
        var myTurn = (board.turn % 2 + 1) == blind;
        var foul = false;
        var end = false;
        if (blind != 0 && board.events) {
            board.events.forEach(function (ev) {
                if (ev.type == "get") {
                    if (myTurn) {
                        texts.push(base.getKomaName(ev.komaType) + "を取りました");
                    }
                    else {
                        texts.push(base.komaTypes[ev.komaType].name + "を取られました");
                    }
                }
                else if (ev.type == "oute") {
                    if (myTurn) {
                        texts.push("王手をかけました");
                    }
                    else {
                        texts.push("王手をかけられました");
                    }
                }
                else if (ev.type == "foul") {
                    foul = true;
                    if (myTurn) {
                        texts.push("反則です");
                    }
                    else {
                        texts.push("相手が反則手を指しました");
                    }
                }
                else if (ev.type == "endGame") {
                    end = true;
                    if (ev.winPlayerNumber == 3 - blind) {
                        texts.push("あなたの勝ちです");
                    }
                    else {
                        texts.push("あなたの負けです");
                    }
                }
            });
            if (!end) {
                if ((myTurn && !foul) || (!myTurn && foul)) {
                    texts.push("相手の手番です");
                }
                else {
                    texts.push("あなたの手番です");
                }
            }
        }
        this.showGameMessage({ type: 'plain', texts: texts });
    };
    GameInfoView.prototype.getViewPoint = function () {
        var value = $("input[name='blind']:checked").val();
        if (value == "先手視点") {
            return { "flip": false, "blind": 0 };
        }
        else if (value == "後手視点") {
            return { "flip": true, "blind": 0 };
        }
        else if (value == "先手視点（ブラインド）") {
            return { "flip": false, "blind": 2 };
        }
        else if (value == "後手視点（ブラインド）") {
            return { "flip": true, "blind": 1 };
        }
    };
    GameInfoView.prototype.getViewPointCode = function () {
        var value = $("input[name='blind']:checked").val();
        if (value == "先手視点") {
            return '1';
        }
        else if (value == "後手視点") {
            return '2';
        }
        else if (value == "先手視点（ブラインド）") {
            return '3';
        }
        else if (value == "後手視点（ブラインド）") {
            return '4';
        }
    };
    GameInfoView.prototype.replayKifu = function (arg) {
        var _this = this;
        this.notify('game-board-init');
        $('#kifuSelectBox').empty();
        $("#kifuInfoDiv").show();
        $("#kifuReplayDiv").show();
        $('.game-playing').hide();
        $('.game-watching').show();
        var refresh = function (index, board) {
            _this.notify('game-update-info', { player1: board.player1, player2: board.player2 });
            _this.notify('game-set-turn', board.turn ? board.turn : 0);
            $('#kifuSelectBox').val(index);
            var kifuLength = $("#kifuSelectBox").children().length - 1;
            _this.notify('game-board-set-invated-position', board.emphasisPosition);
            _this.notify('game-redraw-board', { board: board, turn: 1, state: 'replay' });
            $('#toStartButton').prop('disabled', index == 0);
            $('#backButton').prop('disabled', index == 0);
            $('#nextButton').prop('disabled', kifuLength == index);
            $('#toEndButton').prop('disabled', kifuLength == index);
            var viewPoint = _this.getViewPoint();
            _this.showEvents(board, viewPoint.blind);
        };
        this.disposeGame();
        this.disposeKifu();
        this.kifu = new base.Kifu(arg.kifu, 1, refresh, arg.playerInfo.player1, arg.playerInfo.player2, arg.custom);
        this.currentGameId = arg.gameId;
        this.kifu.synchronizeConnectionInfo(this).connect(this);
        this.kifu.activateUpdateTimer();
        this.notify('game-redraw-board', { board: this.kifu.getCurrentBoard(), turn: 1, state: "replay" });
        var index = 0;
        $('#kifuSelectBox').unbind("change");
        $('#kifuSelectBox').bind("change", function () {
            index = $('#kifuSelectBox option:selected').val();
            _this.kifu.toIndex(parseInt(index));
        });
        var option = $("<option value='" + index + "'>対局開始</option>");
        index++;
        $("#kifuSelectBox").append(option);
        arg.kifu.forEach(function (te) {
            if (te.info.type == "moveKoma") {
                if (!te.foul) {
                    var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
                }
                else {
                    var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
                }
                option = $("<option value='" + index + "'>" + value + "</option>");
            }
            else if (te.info.type == "endGame") {
                value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
                option = $("<option value='" + index + "'>" + value + "</option>");
            }
            index++;
            $("#kifuSelectBox").append(option);
        });
        this.kifu.toEnd();
        this.notify('game-blind-koma', this.getViewPoint());
    };
    GameInfoView.prototype.disposeKifu = function () {
        if (this.kifu) {
            this.kifu.disconnectAll();
        }
    };
    GameInfoView.prototype.addKifu = function (te) {
        this.kifu.addKifu(te);
        var i = $("#kifuSelectBox").children().length;
        if (te.info.type == "moveKoma") {
            if (!te.foul) {
                this.notify("sound-koma");
                var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
            }
            else {
                this.notify("sound-foul");
                var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
            }
            var option = $("<option value='" + i + "'>" + value + "</option>");
        }
        else if (te.info.type == "endGame") {
            value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
            var option = $("<option value=' " + i + "}'>" + value + "</option>");
        }
        $("#kifuSelectBox").append(option);
    };
    GameInfoView.prototype.kifuToStart = function () {
        this.kifu.toStart();
    };
    GameInfoView.prototype.kifuBack = function () {
        this.kifu.back();
    };
    GameInfoView.prototype.kifuNext = function () {
        this.kifu.next();
    };
    GameInfoView.prototype.kifuToEnd = function () {
        if (this.kifu.index >= this.kifu.boards.length - 2) {
            this.kifu.toEnd();
        }
    };
    return GameInfoView;
})(Artifact.Artifact);
var ChatSender = (function (_super) {
    __extends(ChatSender, _super);
    function ChatSender() {
        _super.call(this, 'chat-sender');
        this.initializeFormEvent();
    }
    ChatSender.prototype.initializeFormEvent = function () {
        var self = this;
        $('.form-chat').unbind('submit');
        $('.form-chat').submit(function () {
            var message = $(this).find('input').val();
            if (message) {
                $(this).find('input').val('');
                self.notify('user-message-send', message);
            }
            return false;
        });
    };
    return ChatSender;
})(Artifact.Artifact);
var Pages = (function (_super) {
    __extends(Pages, _super);
    function Pages(user) {
        var _this = this;
        _super.call(this, 'pages');
        this.itemNum = 15;
        this.currentPage = 0;
        this.contents = [];
        this.user = user;
        this.on('user-list-next', function () { return _this.pageNext(); });
        this.on('user-list-back', function () { return _this.pageBack(); });
    }
    Pages.prototype.getPageMax = function () {
        return Math.ceil(this.contents.length / this.itemNum);
    };
    Pages.prototype.activatePageButton = function () {
        $('.back-button').prop('disabled', this.currentPage <= 0);
        $('.next-button').prop('disabled', this.currentPage >= this.getPageMax() - 1);
    };
    Pages.prototype.fixPageIndex = function () {
        var pageMax = this.getPageMax();
        if (this.currentPage > pageMax)
            this.currentPage = pageMax;
        if (this.currentPage < 0)
            this.currentPage = 0;
    };
    Pages.prototype.pageNext = function () {
        this.currentPage++;
        this.fixPageIndex();
        this.activatePageButton();
        this.drawContents();
    };
    Pages.prototype.pageBack = function () {
        this.currentPage--;
        this.fixPageIndex();
        this.activatePageButton();
        this.drawContents();
    };
    Pages.prototype.initPage = function () {
        this.currentPage = 0;
        this.contents = [];
    };
    Pages.prototype.drawContents = function () {
        var _this = this;
        $(".table-rooms-body").empty();
        var contentCount = 0;
        this.contents.map(function (game, index) {
            if (_this.currentPage * _this.itemNum <= index && index < (_this.currentPage + 1) * _this.itemNum) {
                contentCount++;
                var tr = $("<tr style='height:39px;'>");
                if (game.waiting) {
                    tr.append($("<td>").append("対戦待ち"));
                }
                else {
                    tr.append($("<td>").append(game.end ? "終了" : "対局中"));
                }
                if (game.custom) {
                    if (game.custom.hirate_teai == 'teai') {
                        tr.append($("<td>").append("<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-time'  aria-hidden='true'></span><span>" + game.custom.player1.time + ":00</span></span> "
                            + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span><span></span>" + game.custom.player1.life + "</span>　"
                            + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-time'  aria-hidden='true'></span><span>" + game.custom.player2.time + ":00</span></span> "
                            + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span><span></span>" + game.custom.player2.life + "</span>"));
                    }
                    else {
                        tr.append($("<td>").append("<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-time'  aria-hidden='true'></span><span>" + game.custom.player1.time + ":00</span></span> "
                            + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span><span></span>" + game.custom.player1.life + "</span>"));
                    }
                }
                else {
                    tr.append($("<td>").append(game.type));
                }
                if (game.waiting) {
                    tr.append($("<td>").append(""));
                    if (game.custom.hirate_teai == 'teai') {
                        if (game.custom.turn == 'player1') {
                            tr.append($("<td>").append("先手:" + wbase.replaceToRuby(game.account.player1.name)));
                        }
                        else {
                            tr.append($("<td>").append("後手:" + wbase.replaceToRuby(game.account.player1.name)));
                        }
                    }
                    else {
                        tr.append($("<td>").append(wbase.replaceToRuby(game.account.player1.name)));
                    }
                    tr.append($("<td>").append("<button type='button' style='color:#FFFFFF;' class='btn game-start-button'><span class='glyphicon glyphicon-flash' aria-hidden='true'></span>　対戦する</button>")
                        .on("click", (function (id) { return function () { return _this.user.startCustomRule(id); }; })(game.gameId)));
                }
                else {
                    tr.append($("<td>").append(game.startDateTime));
                    tr.append($("<td>").append("先手:" + wbase.replaceToRuby(game.account.player1.name)
                        + "  後手：" + wbase.replaceToRuby(game.account.player2.name)));
                    tr.append($("<td>").append("<button type='button' style='color:#FFFFFF;' class='btn'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'</span>　観戦する</button>")
                        .bind("click", (function (id) { return function () { return _this.user.watch(id); }; })(game.gameId)));
                }
                $(".table-rooms-body").append(tr);
            }
        });
        while (contentCount < this.itemNum) {
            contentCount++;
            var tr = $("<tr style='height:39px;'>");
            tr.append($("<td>"));
            tr.append($("<td>"));
            tr.append($("<td>"));
            tr.append($("<td>"));
            tr.append($("<td>"));
            $(".table-rooms-body").append(tr);
        }
        $('.page-number').empty();
        $('.page-number').append((this.currentPage + 1) + '/' + this.getPageMax());
    };
    return Pages;
})(Artifact.Artifact);

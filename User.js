//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var User = (function (_super) {
    __extends(User, _super);
    function User() {
        _super.call(this, 'user');
    }
    User.prototype.startGame = function (type) {
        this.notify('user-game-start', type);
    };
    User.prototype.createCustomRule = function () {
        this.notify('user-rule-create');
    };
    User.prototype.startCustomRule = function (gameId) {
        this.notify('user-custom-start', gameId);
    };
    User.prototype.cancelGame = function () {
        this.notify('user-game-cancel');
    };
    User.prototype.ruleModalOk = function () {
        this.notify('user-rule-ok');
    };
    User.prototype.ruleModalCancel = function () {
        this.notify('user-rule-cancel');
    };
    User.prototype.watch = function (gameId) {
        this.notify('user-game-watch', gameId);
    };
    User.prototype.watchByGameId = function () {
        this.notify('user-watch-by-game-id');
    };
    User.prototype.changeViewPoint = function () {
        this.notify('user-game-change-viewpoint');
    };
    User.prototype.tryResign = function () {
        this.notify('user-game-try-resign');
    };
    User.prototype.resign = function () {
        this.notify('user-game-resign');
    };
    User.prototype.back = function () {
        this.notify('user-back');
    };
    User.prototype.listNext = function () {
        this.notify('user-list-next');
    };
    User.prototype.listBack = function () {
        this.notify('user-list-back');
    };
    User.prototype.exitRoom = function () {
        this.notify('user-game-exit');
    };
    User.prototype.nariModalNari = function () {
        this.notify('user-nari-modal-nari');
    };
    User.prototype.nariModalNotNari = function () {
        this.notify('user-nari-modal-not-nari');
    };
    User.prototype.nariModalCancel = function () {
        this.notify('user-nari-modal-cancel');
    };
    User.prototype.kifuToStart = function () {
        this.notify('kifu-to-start');
    };
    User.prototype.kifuNext = function () {
        this.notify('kifu-next');
    };
    User.prototype.kifuBack = function () {
        this.notify('kifu-back');
    };
    User.prototype.kifuToEnd = function () {
        this.notify('kifu-to-end');
    };
    User.prototype.showGameId = function () {
        this.notify('user-need-game-id');
    };
    User.prototype.showEmbeddedTag = function () {
        this.notify('user-need-embedded-tag');
    };
    User.prototype.modalOk = function () {
        this.notify('user-modal-ok');
    };
    User.prototype.modalCancel = function () {
        this.notify('user-modal-cancel');
    };
    User.prototype.requestConnect = function (type) {
        this.notify('user-request-connect', type);
        return false;
    };
    User.prototype.logout = function (type) {
        this.notify('user-logout');
        return false;
    };
    User.prototype.requestPlayingGames = function () {
        this.notify('user-request-playing-games');
    };
    User.prototype.requestOwnHistory = function () {
        this.notify('user-request-own-history');
    };
    User.prototype.requestOwnProfile = function () {
        this.notify('user-request-own-profile');
    };
    User.prototype.sendMessage = function (message) {
        this.notify('user-send-message');
    };
    return User;
})(Artifact.Artifact);
exports.User = User;

//Copyright (c) 2015-2016 potalong_oreo All rights reserved

import Artifact = require('../potaore/artifact');
import img = require('./ImageManager');
import sound = require('./SoundManager');
import usr = require('./User');
import base = require('./Base');
import wbase = require('./WebBase');
import sckt = require('./Socket');
import board = require('./BoardGraphic');
import session = require('./SessionManager');
declare let app: any;
declare let $: any;

let _window: any = window;
var playSound;


let domFinder = {
  getPlayerNameDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerImageDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getGameTurnDiv: function () {
    return $('#game-turn-label');
  },
  getPlayerTimeDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerLifeLabel: function (playerNumber, flip) {
    if (flip) {
      return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerLifeDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getFlipItem: function (playerNumber, flip) {
    return $('.flip-item');
  },
  getNotFlipItem: function (playerNumber, flip) {
    return $('.not-flip-item');
  }
};


$(window).load(function () {



  let imageManager = new img.ImageManager();
  let user = new usr.User();
  let pages = new Pages(user);
  pages.listen(user);
  let chatSender = new ChatSender();
  let view = new View(imageManager, pages, user);
  view.listen(user);
  let gameInfoView = new GameInfoView();
  gameInfoView.connect(view).listen(user);
  let socket = new sckt.Socket();
  socket.listen(user).connect(view).connect(gameInfoView).listen(chatSender);
  let gameBoard = new board.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager, domFinder);
  gameBoard.listen(user).connect(socket).connect(view).connect(gameInfoView);

  let soundManager = new sound.SoundManager();
  soundManager
    .listen(user)
    .listen(view)
    .listen(gameInfoView)
    .listen(socket)
    .listen(gameBoard);


  app = user;

  {
    let logger = new Artifact.Listner({ info: (data) => console.log(data) }, false);
    logger.listen(soundManager);
    logger.listen(user);
    logger.listen(view);
    logger.listen(socket);
  }


  imageManager.load(() => view.setIconSelector());
  let sessionId = session.get('sessionId');
  if (sessionId) {
    view.notify('connect-session', { sessionId: sessionId });
  }
});


interface Page {
  name: string;
  options: any;
}

class Paging {
  static pages = {
    loginForm: 'login-form',
    topMenu: 'top-menu',
    playingGames: 'playing-games',
    waitingGames: 'waiting-games',
    ownHistory: 'own-history',
    ownProfile: 'own-profile',
    gameBoard: 'game-board'
  }

  static currentPage = Paging.pages.loginForm;
  static history: Page[] = [{ name: Paging.pages.loginForm, options: {} }];
  static login() {
    $('.state-logout').hide();
    $('#navbar').show();
    Paging.topMenu();
  }
  static logout() {
    Paging.currentPage = Paging.pages.loginForm;
    $('.state-login').hide();
    $('.state-logout').show();
    Paging.history = [{ name: Paging.pages.loginForm, options: {} }];
  }
  static topMenu() {
    if (Paging.currentPage == Paging.pages.gameBoard) {
      $('.lobby-chace').empty();
    }
    Paging.currentPage = Paging.pages.topMenu;
    $('#top-menu').show();
    $('#game-board').hide();
    $('#playing-games').hide();
    $('#own-history').hide();
    $('#own-profile').hide();
    $('.lobby-item').show();
    $('.disable-playing').prop('disabled', false);
    Paging.history.push({ name: Paging.pages.topMenu, options: {} });
  }

  static playingGames() {
    if (Paging.currentPage == Paging.pages.gameBoard) {
      $('.lobby-chace').empty();
    }
    Paging.currentPage = Paging.pages.playingGames;
    $('#top-menu').hide();
    $('#game-board').hide();
    $('#playing-games').show();
    $('#own-history').hide();
    $('#own-profile').hide();
    $('.lobby-item').show();
    $('.disable-playing').prop('disabled', false);
    Paging.history.push({ name: Paging.pages.playingGames, options: {} });
  }

  static ownHistory() {
    if (Paging.currentPage == Paging.pages.gameBoard) {
      $('.lobby-cache').empty();
    }
    Paging.currentPage = Paging.pages.ownHistory;
    $('#top-menu').hide();
    $('#game-board').hide();
    $('#playing-games').hide();
    $('#own-history').show();
    $('#own-profile').hide();
    $('.lobby-item').show();
    $('.disable-playing').prop('disabled', false);
    Paging.history.push({ name: Paging.pages.ownHistory, options: {} });
  }

  static ownProfile() {
    if (Paging.currentPage == Paging.pages.gameBoard) {
      $('.lobby-cache').empty();
    }
    Paging.currentPage = Paging.pages.ownProfile;
    $('#top-menu').hide();
    $('#game-board').hide();
    $('#playing-games').hide();
    $('#own-history').hide();
    $('#own-profile').show();
    $('.lobby-item').show();
    $('.disable-playing').prop('disabled', false);
    Paging.history.push({ name: Paging.pages.ownProfile, options: {} });
  }

  static gameBoard() {
    if (Paging.currentPage == Paging.pages.gameBoard) return;
    Paging.currentPage = Paging.pages.gameBoard;
    $('#top-menu').hide();
    $('#game-board').show();
    $('#playing-games').hide();
    $('#own-history').hide();
    $('#own-profile').hide();
    $('.lobby-item').hide();
    $('.lobby-cache').empty();
    $('input[name=blind]').val(["先手視点"]);
    $('.disable-playing').prop('disabled', true);
    Paging.history.push({ name: Paging.pages.gameBoard, options: {} });
  }

  static exitRoom() {
    if (Paging.currentPage == Paging.pages.gameBoard) {
      $('.lobby-cache').empty();
      $('.game-cache').empty();
      Paging.back();
    }
  }

  static back() {
    if (Paging.history.length >= 2) {
      let page = Paging.history[Paging.history.length - 2];
      Paging.history.pop();
      Paging.history.pop();
      switch (page.name) {
        case Paging.pages.loginForm:
          //Paging.logout()
          break;
        case Paging.pages.topMenu:
          Paging.topMenu();
          break;
        case Paging.pages.playingGames:
          Paging.playingGames();
          break;
        case Paging.pages.ownHistory:
          Paging.ownHistory();
          break;
        case Paging.pages.gameBoard:
          Paging.gameBoard();
          break;
        default:
          Paging.topMenu();
          break;
      }
    }
  }
}

class View extends Artifact.Artifact {
  imageManager: img.ImageManager
  afterModalOk: { (): any };
  afterModalCancel: { (): any };
  selectedCharacter: number;
  pages: Pages;
  user: usr.User;
  waiting : boolean;

  constructor(imageManager: img.ImageManager, pages: Pages, user: usr.User) {
    super('view');
    this.imageManager = imageManager;
    this.pages = pages;
    this.user = user;
    this.waiting = false;

    this.on('user-request-connect', (eventName, sender, type) => { this.login(type); });
    this.on('user-logout', (eventName, sender, type) => {
      this.cancelWaiting();
      Paging.logout();
    });

    this.on('logout', () => {
      if (Paging.currentPage != Paging.pages.loginForm) {
        this.cancelWaiting();
        this.showModal(['サーバーとの接続が切れました'], null,
          { ok: true, cancel: false },
          () => Paging.logout());
      }
    });

    this.on('receive-login-ok', (eventName, sender, arg) => {
      Paging.login();
      this.updateLoginInfo(arg);
      if (arg.account && arg.account.tid) {
        $(".account-only").show();
      } else {
        $(".account-only").hide();
      }
    });

    this.on('receive-connection-info', (eventName, sender, options) => { this.updateConnectionInfo(options); });

    this.on('user-request-playing-games', () => {
      this.pages.initPage();
      Paging.playingGames();
    });
    this.on('receive-playing-games', (eventName, sender, data) => this.showPlayingGames(data));

    this.on('user-request-own-history', () => {
      this.pages.initPage();
      Paging.ownHistory();
    });

    this.on('user-watch-by-game-id', () => {
      this.showModal(
        ['ゲームIDを入力してください'],
        [''],
        { ok: true, cancel: true },
        () => {
          user.watch($('#modal-box').find('input').val());
        });
    });

    this.on('receive-account-created', (eventName, sender, arg) => {
      this.showModal(
        [arg.message],
        [arg.account.tid + ':' + arg.account.password],
        { ok: true, cancel: false, boxReadonly: true },
        () => {
          user.watch($('#modal-box').find('input').val());
        });
    });

    this.on('user-request-own-profile', () => {
      Paging.ownProfile();
    });

    this.on('receive-message', (eventName, sender, message) => { this.addMessage(message); });

    this.on('receive-profile', (eventName, sender, data) => { this.setProfile(data); });

    this.on('user-back', (eventName, sender, type) => Paging.back());

    this.on('user-game-start', (eventName, sender, type) => this.gameStart(type));

    this.on('user-custom-start', (eventName, sender, gameId) => this.customStart(gameId));

    this.on('user-rule-create', (eventName, sender, type) => this.showCustomRuleModal());

    this.on('user-rule-ok', (eventName, sender, type) => this.createRule());

    this.on('user-rule-cancel', (eventName, sender, type) => this.hideCustomRuleModal());

    this.on('user-game-cancel', (eventName, sender, type) => this.cancelWaiting());

    this.on('user-game-try-resign', (eventName, sender, type) => this.showModal(
      ['投了しますか？'],
      [],
      { ok: true, cancel: true, notApplyToGameMessageArea: true },
      () => this.notify('user-game-resign'),
      () => { }
    )
    );

    this.on('receive-game-end', (eventName, sender, options) => this.notify('user-game-cancel'));

    this.on('receive-kifu-replay', () => {
      $('.game-cache').empty();
      Paging.gameBoard();
    });

    this.on('receive-game-start', (eventName, sender, arg) => {
      this.cancelWaiting();
      Paging.gameBoard();
      this.hideModal();
      $('.game-cache').empty();
    });

    this.on('receive-game-need-restart', (eventName, sender, options) => {
      this.showModal(
        ['ゲームを再開します'],
        [],
        { ok: true, cancel: false, notApplyToGameMessageArea: true },
        () => this.notify('user-game-restart'),
        () => { }
      );
    });

    this.on('receive-game-restart', (eventName, sender, arg) => {
      Paging.gameBoard();
      this.hideModal();
      $('.game-cache').empty();
    });

    this.on('user-game-exit', (eventName, sender, type) => Paging.exitRoom());

    this.on('view-modal-show', (eventName, sender, data) => {
      if (data.options.error) {
        this.showModal(
          data.texts,
          data.boxes,
          { ok: data.options.ok, cancel: data.options.cancel },
          () => Paging.exitRoom(),
          () => Paging.exitRoom()
        );
      } else {
        this.showModal(
          data.texts,
          data.boxes,
          { ok: data.options.ok, cancel: data.options.cancel },
          () => { },
          () => { }
        );
      }
    });

    this.on('receive-show-modal', (eventName, sender, data) => this.showModal(data.messages, data.boxes, data.options));

    this.on('show-modal', (eventName, sender, data) => this.showModal(data.messages, data.boxes, data.options, data.afterOk, data.afterCancel));

    this.on('user-modal-ok', (eventName, sender, options) => {
      this.hideModal();
      if (this.afterModalOk) this.afterModalOk();
    });

    this.on('receive-session-id', (eventName, sender, sessionId) => {
      session.set('sessionId', sessionId, 30);
    });

    this.on('user-modal-cancel', (eventName, sender, options) => {
      this.hideModal();
      if (this.afterModalCancel) this.afterModalCancel();
    });
  }

  setIconSelector() {
    $("#icon-selector").empty();
    let images = this.imageManager.getRandomCharacters(10).map(data => { return { num: data.num, image: $(data.image) }; });
    images.forEach(data => {
      data.image.addClass("iconCandidate");
      $("#icon-selector").append(data.image)
      data.image.on("click", () => {
        this.selectedCharacter = data.num;
        $(".iconCandidate").css({ border: "none" });
        data.image.css({ border: "solid" });
      });
    });
  };

  updateLoginInfo(arg) {
    $("#account-icon").empty();
    $("#account-icon").append(this.imageManager.getIconImage(arg.account.profile_url, arg.account.character));
    $("#user-name").empty();
    $("#user-name").append(wbase.replaceToRuby(arg.account.name));
    $("#lobby-profile").hide();
  }

  gameStart(type) {
    $('#game-waiting').show();
    $('.game-start-button').prop('disabled', true);
    $('#game-waiting-label-free').hide();
    $('#game-waiting-label-ai').hide();
    $('#game-waiting-label-rating').hide();
    if (type == 'free') {
      $('#game-waiting-label-free').show();
    } else if (type == 'bot') {
      $('#game-waiting-label-ai').show();
    } else if (type = 'rating') {
      $('#game-waiting-label-rating').show();
    }
    this.waiting = true;
  }

  customStart(gameId) {
  }

  createRule() {
    let hirate_teai = $('input[name="hirate-teai"]:checked').val();
    let turn = $('input[name="turn"]:checked').val();
    let time = $('#customrule-time').val();
    let life = $('#customrule-life').val();
    let time2 = $('#customrule-time2').val();
    let life2 = $('#customrule-life2').val();

    if (time === "") time = 10;
    if (time < 1) time = 1;
    if (time > 60) time = 60;
    if (life === "") life = 9;
    if (life < 0) life = 0;
    if (life > 99) life = 99;

    if(hirate_teai == "hirate") {
      time2 = time;
      life2 = time;
    } else {
      if (time2 === "") time2 = 10;
      if (time2 < 1) time2 = 1;
      if (time2 > 60) time2 = 60;
      if (life2 === "") life2 = 9;
      if (life2 < 0) life2 = 0;
      if (life2 > 99) life2 = 99;
    }

    this.hideCustomRuleModal();

    $('#game-waiting').show();
    $('.game-start-button').prop('disabled', true);
    $('#game-waiting-label-free').hide();
    $('#game-waiting-label-ai').hide();
    $('#game-waiting-label-rating').hide();

    $('#game-waiting-label-custom').show();
    this.notify('create-rute', { turn : turn, hirate_teai : hirate_teai, player1: { time: time, life: life }, player2: { time: time2, life: life2 } });
    this.waiting = true;
  }

  cancelWaiting() {
    $('#game-waiting').hide();
    $('.game-start-button').prop('disabled', false);
    this.waiting = false;
  }

  addMessage(arg) {
    if (arg.type === "system-info") {
      $(".chat-messages").append($('<li>').addClass('system-info').text(arg.message));
    } else {
      $(".chat-messages").append($('<li>').text(arg.message));
    }
    $('.chat-messages-div').scrollTop(View.max($('#messages').height(), $('#messages-lobby').height()));
  }

  static max(a, b) {
    return a > b ? a : b;
  }

  updateConnectionInfo(info) {
    $("#connection-count").empty();
    $("#connection-count").append(info.connectionCount);
    $("#playing-game-count").empty();
    $("#playing-game-count").append(info.playingGameCount);

    $("#matching-game-count-free").empty();
    $("#matching-game-count-bot").empty();
    $("#matching-game-count-rating").empty();
    $("#matching-game-count-free").append(info.matchingGameCount);
    $("#matching-game-count-bot").append(info.matchingGameBotCount);
    $("#matching-game-count-rating").append(info.matchingGameRatingCount);
  }

  setProfile(data) {
    $("#table-own-profile-body").empty();
    this.appendProfile('ユーザー名', data.account.name);
    if (data.account.rate) {
      this.appendProfile('レート', data.account.rate);
      this.appendProfile('レーティング対局：勝数', data.account.results.rating.win);
      this.appendProfile('レーティング対局：負数', data.account.results.rating.loose);
      this.appendProfile('フリー対局：勝数', data.account.results.free.win);
      this.appendProfile('フリー対局：負数', data.account.results.free.loose);
    }
  }

  appendProfileTextbox(key, value) {
    var tr = $("<tr>");
    tr.append($("<td>").append(key));
    tr.append($("<td>").append($('<input type="text" id="profile-' + key + '" style="width:320px">').val(value)));
    $("#table-own-profile-body").append(tr);
  }

  appendProfile(key, value) {
    var tr = $("<tr>");
    tr.append($("<td>").append(key));
    tr.append($("<td>").append(value));
    $("#table-own-profile-body").append(tr);
  }

  login(type) {
    if (type === "account") {
      this.notify('connect-account', { loginstr: $("#loginstr").val().trim(), character: this.selectedCharacter, subCharacter: this.imageManager.getRandomCharacters(1)[0].num });
    } else if (type === "create") {
      this.notify('create-account', { loginstr: $("#loginstr").val().trim(), character: this.selectedCharacter });
    } else {
      if (this.selectedCharacter === null || this.selectedCharacter === undefined) {
        this.selectedCharacter = this.imageManager.getRandomCharacters(1)[0].num;
      }
      this.notify('connect-guest', { loginstr: $("#loginstr").val().trim(), character: this.selectedCharacter });
    }
  };

  showPlayingGames(data) {
    this.pages.contents = data.playingGames;

    if ($(".table-rooms-body").children().length == 0) {
      this.pages.drawContents();
    } else {
      this.pages.drawContents();
    }
    this.pages.activatePageButton();

    if(this.waiting) {
      $('.game-start-button').prop('disabled', true);
    } else {
      $('.game-start-button').prop('disabled', false);
    }
  }

  showCustomRuleModal() {
    $("#customrule-modal-window").show();
    $("#modal-overlay").show();
  }

  hideCustomRuleModal() {
    $("#customrule-modal-window").hide();
    $("#modal-overlay").hide();
  }

  showModal(texts, boxes, option, afterOk = () => { }, afterCancel = () => { }) {
    this.afterModalOk = afterOk;
    this.afterModalCancel = afterCancel;
    if (option.ok) {
      $("#modalConfirmButton").show();
    } else {
      $("#modalConfirmButton").hide();
    }
    if (option.cancel) {
      $("#modalCancelButton").show();
    } else {
      $("#modalCancelButton").hide();
    }

    if (!option.notApplyToGameMessageArea) {
      $("#game-message-area").empty();
    }
    $("#modal-message").empty();
    $("#modal-box").empty();
    $("#modal-window").show();
    $("#modal-overlay").show();
    $("#modal-window").css({ "background-color": "#000000", "opacity": 0 });
    $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
    $("#modal-window").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
    $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
    texts.forEach(text => {
      if (!option.notApplyToGameMessageArea) {
        $("#game-message-area").append($('<span>').text(text));
      }
      $("#modal-message").append($('<div>').text(text));
    });

    if (boxes) {
      boxes.forEach(text => {
        if (!option.notApplyToGameMessageArea) {
          $("#game-message-area").append($('<span>').text(text));
        }
        if (option.boxReadonly) {
          $("#modal-box").append($('<input type="text" class="modal-textbox" onclick="this.select(0,this.value.length)" readonly>').val(text));
        } else {
          $("#modal-box").append($('<input type="text" class="modal-textbox">').val(text));
        }

      });
    }
  }

  hideModal() {
    $("#modal-window").hide();
    $("#modal-overlay").hide();
  }
}

class GameInfoView extends Artifact.Artifact {
  afterNariModalNari: { (): any };
  afterNariModalNotNari: { (): any };
  afterNariModalCancel: { (): any };
  kifu: base.Kifu;
  game: base.Game;
  playerInfoMemo = null;
  currentGameId: string;

  constructor() {
    super('game-info-view');
    this.on('user-nari-modal-nari', (eventName, sender, options) => {
      this.hideNariModal();
      if (this.afterNariModalNari) this.afterNariModalNari();
    });

    this.on('user-nari-modal-not-nari', (eventName, sender, options) => {
      this.hideNariModal();
      if (this.afterNariModalNotNari) this.afterNariModalNotNari();
    });

    this.on('user-nari-modal-cancel', (eventName, sender, options) => {
      this.hideNariModal();
      if (this.afterNariModalCancel) this.afterNariModalCancel();
    });

    this.on('board-show-nari-modal', (eventName, sender, options) => this.showNariModal(options.nari, options.notNari, options.cancel));
    this.on("game-show-message", (eventName, sender, options) => this.showGameMessageConsole(options));
    this.on('receive-game-message', (eventName, sender, message) => this.showGameMessage(message));
    this.on('receive-game-start', (eventName, sender, options) => this.startGame(options));
    this.on('receive-game-end', (eventName, sender, options) => this.endGame(options));
    this.on('receive-game-restart', (eventName, sender, options) => this.restartGame(options));


    this.on('receive-kifu-replay', (eventName, sender, data) => this.replayKifu(data.kifu));
    this.on('kifu-add', (eventName, sender, data) => this.addKifu(data.te));
    this.on('kifu-to-start', (eventName, sender, options) => this.kifuToStart());
    this.on('kifu-back', (eventName, sender, options) => this.kifuBack());
    this.on('kifu-next', (eventName, sender, options) => this.kifuNext());
    this.on('kifu-to-end', (eventName, sender, options) => this.kifuToEnd());

    this.on('user-need-game-id', () => {
      this.notify('show-modal', {
        messages: ['ゲームID'],
        boxes: [this.currentGameId],
        options: { ok: true, cancel: false, notApplyToGameMessageArea: true, boxReadonly: true }
      });
    });

    this.on('user-need-embedded-tag', () => {
      let gameId = this.currentGameId;
      let index = $('#kifuSelectBox option:selected').val();
      let viewPointCode = this.getViewPointCode();
      let tag = '<iframe width="580" height="420" src="http://potaore.github.io/apps/tsuitate-shogi-dojo/tsuitate-embedded.html?gameId=' + gameId + '&amp;index=' + index + '&amp;viewpoint=' + viewPointCode + '" frameborder="0"></iframe>'
      this.notify('show-modal', {
        messages: ['埋め込みタグ', '※現在の局面、視点が再現されます'],
        boxes: [tag],
        options: { ok: true, cancel: false, notApplyToGameMessageArea: true, boxReadonly: true }
      });
    });

    this.on('user-game-change-viewpoint', () => {
      this.notify('game-blind-koma', this.getViewPoint());
    });
  }

  showGameMessage(message) {
    if (message.type == "plain") {
      this.showGameMessageConsole(message);
    } else if (message.type == "modal") {
      this.notify('view-modal-show', { texts: message.texts, options: { ok: true, cancel: false, error: message.error } });
    }
  }


  showNariModal(afterNari = () => { }, afterNotNari = () => { }, afterCancel = () => { }) {
    this.afterNariModalNari = afterNari;
    this.afterNariModalNotNari = afterNotNari;
    this.afterNariModalCancel = afterCancel;
    $("#nariDiv").show();
    $("#modal-overlay").show();
    $("#nariDiv").css({ "background-color": "#000000", "opacity": 0 });
    $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
    $("#nariDiv").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
    $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
  }

  hideNariModal() {
    $("#modal-overlay").hide();
    $("#nariDiv").hide();
  }

  showGameMessageConsole(message) {
    $("#game-message-area").empty();
    message.texts.forEach(text => $("#game-message-area").append($('<span>').text(text + '.　')));
    message.texts.forEach(text => $("#game-history").append($('<li>').text(text + '.　')));
    $('#game-history-div').scrollTop($("#game-history-area").height());
  }

  startGame(args) {
    this.disposeGame();
    this.disposeKifu();
    this.game = new base.Game(args.playerNumber, args.playerInfo.player1, args.playerInfo.player2);
    this.currentGameId = args.gameId;
    this.game.synchronizeConnectionInfo(this).connect(this);
    this.game.start();
    this.notify('game-board-init');
    this.notify('game-board-flip', args.playerNumber != 1);
    this.notify('game-redraw-board', this.game);
    $('.game-playing').show();
    $('.game-watching').hide();
    this.notify('sound-start')
  }

  restartGame(args) {
    this.disposeGame();
    this.disposeKifu();
    this.game = new base.Game(args.playerNumber, args.playerInfo.player1, args.playerInfo.player2);
    this.currentGameId = args.gameId;
    this.game.synchronizeConnectionInfo(this).connect(this);
    this.game.start();
    this.game.synchronizeGameInfo(args.board, args.turn, args.playerInfo, args.elapsedTime);
    this.notify('game-board-init');
    this.notify('game-board-flip', args.playerNumber != 1);
    this.notify('game-restart', args);
    this.notify('game-redraw-board', this.game);
    $('.game-playing').show();
    $('.game-watching').hide();
    this.showGameMessage(args.message)
  }

  disposeGame() {
    if (this.game) {
      this.game.disconnectAll();
    }
  }

  endGame(args) {
    $('.game-playing').hide();
    $('.game-watching').show();

    if (this.game.playerNumber == 1) {
      $('input[name=blind]').val(["先手視点"]);
    } else {
      $('input[name=blind]').val(["後手視点"]);
    }
    this.replayKifu(args);
  }

  showEvents(board, blind) {
    let texts = [];
    let myTurn = (board.turn % 2 + 1) == blind;
    let foul = false;
    let end = false;
    if (blind != 0 && board.events) {
      board.events.forEach(ev => {
        if (ev.type == "get") {
          if (myTurn) {
            texts.push(base.getKomaName(ev.komaType) + "を取りました");
          } else {
            texts.push(base.komaTypes[ev.komaType].name + "を取られました");
          }
        } else if (ev.type == "oute") {
          if (myTurn) {
            texts.push("王手をかけました");
          } else {
            texts.push("王手をかけられました");
          }
        } else if (ev.type == "foul") {
          foul = true
          if (myTurn) {
            texts.push("反則です")
          } else {
            texts.push("相手が反則手を指しました")
          }
        } else if (ev.type == "endGame") {
          end = true
          if (ev.winPlayerNumber == 3 - blind) {
            texts.push("あなたの勝ちです");
          } else {
            texts.push("あなたの負けです");
          }
        }
      });
      if (!end) {
        if ((myTurn && !foul) || (!myTurn && foul)) {
          texts.push("相手の手番です");
        } else {
          texts.push("あなたの手番です");
        }
      }
    }

    this.showGameMessage({ type: 'plain', texts: texts })
  }

  getViewPoint() {
    var value = $("input[name='blind']:checked").val();
    if (value == "先手視点") {
      return { "flip": false, "blind": 0 };
    } else if (value == "後手視点") {
      return { "flip": true, "blind": 0 };
    } else if (value == "先手視点（ブラインド）") {
      return { "flip": false, "blind": 2 };
    } else if (value == "後手視点（ブラインド）") {
      return { "flip": true, "blind": 1 };
    }
  }

  getViewPointCode() {
    var value = $("input[name='blind']:checked").val();
    if (value == "先手視点") {
      return '1';
    } else if (value == "後手視点") {
      return '2';
    } else if (value == "先手視点（ブラインド）") {
      return '3';
    } else if (value == "後手視点（ブラインド）") {
      return '4';
    }
  }

  replayKifu(arg) {
    this.notify('game-board-init');
    $('#kifuSelectBox').empty();
    $("#kifuInfoDiv").show();
    $("#kifuReplayDiv").show();
    $('.game-playing').hide();
    $('.game-watching').show();
    let refresh = (index, board) => {
      this.notify('game-update-info', { player1: board.player1, player2: board.player2 });
      this.notify('game-set-turn', board.turn ? board.turn : 0);
      $('#kifuSelectBox').val(index);
      let kifuLength = $("#kifuSelectBox").children().length - 1;

      this.notify('game-board-set-invated-position', board.emphasisPosition);

      this.notify('game-redraw-board', { board: board, turn: 1, state: 'replay' })
      $('#toStartButton').prop('disabled', index == 0)
      $('#backButton').prop('disabled', index == 0)
      $('#nextButton').prop('disabled', kifuLength == index)
      $('#toEndButton').prop('disabled', kifuLength == index)
      let viewPoint = this.getViewPoint();
      this.showEvents(board, viewPoint.blind);
    }

    this.disposeGame();
    this.disposeKifu();
    this.kifu = new base.Kifu(arg.kifu, 1, refresh, arg.playerInfo.player1, arg.playerInfo.player2, arg.custom);
    this.currentGameId = arg.gameId;
    this.kifu.synchronizeConnectionInfo(this).connect(this);

    this.kifu.activateUpdateTimer();
    this.notify('game-redraw-board', { board: this.kifu.getCurrentBoard(), turn: 1, state: "replay" })
    let index: any = 0

    $('#kifuSelectBox').unbind("change");
    $('#kifuSelectBox').bind("change", () => {
      index = $('#kifuSelectBox option:selected').val();
      this.kifu.toIndex(parseInt(index));
    });
    let option = $("<option value='" + index + "'>対局開始</option>");
    index++;
    $("#kifuSelectBox").append(option);
    arg.kifu.forEach(te => {
      if (te.info.type == "moveKoma") {
        if (!te.foul) {
          var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
        } else {
          var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
        }
        option = $("<option value='" + index + "'>" + value + "</option>");
      } else if (te.info.type == "endGame") {
        value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
        option = $("<option value='" + index + "'>" + value + "</option>");
      }
      index++;
      $("#kifuSelectBox").append(option);
    });
    this.kifu.toEnd();
    this.notify('game-blind-koma', this.getViewPoint());
  }

  disposeKifu() {
    if (this.kifu) {
      this.kifu.disconnectAll();
    }
  }

  addKifu(te) {
    this.kifu.addKifu(te);
    let i = $("#kifuSelectBox").children().length;
    if (te.info.type == "moveKoma") {
      if (!te.foul) {
        this.notify("sound-koma");
        var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma
      } else {
        this.notify("sound-foul");
        var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma
      }
      var option = $("<option value='" + i + "'>" + value + "</option>");
    } else if (te.info.type == "endGame") {
      value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
      var option = $("<option value=' " + i + "}'>" + value + "</option>");
    }
    $("#kifuSelectBox").append(option);
  }

  kifuToStart() {
    this.kifu.toStart();
  }

  kifuBack() {
    this.kifu.back();
  }

  kifuNext() {
    this.kifu.next();
  }

  kifuToEnd() {
    if(this.kifu.index >= this.kifu.boards.length - 2) {
      this.kifu.toEnd();
    }
  }
}

class ChatSender extends Artifact.Artifact {
  constructor() {
    super('chat-sender');
    this.initializeFormEvent();
  }

  initializeFormEvent() {
    let self = this;
    $('.form-chat').unbind('submit');
    $('.form-chat').submit(function () {
      let message = $(this).find('input').val();
      if (message) {
        $(this).find('input').val('');
        self.notify('user-message-send', message);
      }
      return false;
    });
  }
}

class Pages extends Artifact.Artifact {
  user: usr.User;
  itemNum = 15;
  currentPage = 0;
  contents = [];
  constructor(user: usr.User) {
    super('pages')
    this.user = user;

    this.on('user-list-next', () => this.pageNext());

    this.on('user-list-back', () => this.pageBack());
  }

  getPageMax() {
    return Math.ceil(this.contents.length / this.itemNum);
  }
  activatePageButton() {
    $('.back-button').prop('disabled', this.currentPage <= 0);
    $('.next-button').prop('disabled', this.currentPage >= this.getPageMax() - 1);
  }
  fixPageIndex() {
    var pageMax = this.getPageMax();
    if (this.currentPage > pageMax) this.currentPage = pageMax;
    if (this.currentPage < 0) this.currentPage = 0;
  }
  pageNext() {
    this.currentPage++;
    this.fixPageIndex();
    this.activatePageButton();
    this.drawContents();
  }
  pageBack() {
    this.currentPage--;
    this.fixPageIndex();
    this.activatePageButton();
    this.drawContents();
  }
  initPage() {
    this.currentPage = 0;
    this.contents = [];
  }
  drawContents() {
    $(".table-rooms-body").empty();
    let contentCount = 0;
    this.contents.map((game, index) => {
      if (this.currentPage * this.itemNum <= index && index < (this.currentPage + 1) * this.itemNum) {
        contentCount++;
        var tr = $("<tr style='height:39px;'>");

        if (game.waiting) {
          tr.append($("<td>").append("対戦待ち"));
        } else {
          tr.append($("<td>").append(game.end ? "終了" : "対局中"));
        }

        if (game.custom) {
          if(game.custom.hirate_teai == 'teai') {
            tr.append($("<td>").append(
              "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-time'  aria-hidden='true'></span><span>" + game.custom.player1.time + ":00</span></span> "
              + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span><span></span>" + game.custom.player1.life + "</span>　"
              + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-time'  aria-hidden='true'></span><span>" + game.custom.player2.time + ":00</span></span> "
              + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span><span></span>" + game.custom.player2.life + "</span>"
            ));
          } else {
            tr.append($("<td>").append("<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-time'  aria-hidden='true'></span><span>" + game.custom.player1.time + ":00</span></span> "
              + "<span class='label label-primary game-info-label'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span><span></span>" + game.custom.player1.life + "</span>"));
          }
        } else {
          tr.append($("<td>").append(game.type));
        }

        if (game.waiting) {
          tr.append($("<td>").append(""));
          if(game.custom.hirate_teai == 'teai') {
            if(game.custom.turn == 'player1') {
              tr.append($("<td>").append("先手:" + wbase.replaceToRuby(game.account.player1.name)));
            }  else {
              tr.append($("<td>").append("後手:" + wbase.replaceToRuby(game.account.player1.name)));
            }
          } else {
            tr.append($("<td>").append(wbase.replaceToRuby(game.account.player1.name)));
          }
          
          tr.append($("<td>").append("<button type='button' style='color:#FFFFFF;' class='btn game-start-button'><span class='glyphicon glyphicon-flash' aria-hidden='true'></span>　対戦する</button>")
            .on("click", ((id) => () => this.user.startCustomRule(id))(game.gameId)) );
        } else {
          tr.append($("<td>").append(game.startDateTime));
          tr.append($("<td>").append("先手:" + wbase.replaceToRuby(game.account.player1.name)
            + "  後手：" + wbase.replaceToRuby(game.account.player2.name)));
          tr.append($("<td>").append("<button type='button' style='color:#FFFFFF;' class='btn'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'</span>　観戦する</button>")
            .bind("click", ((id) => () => this.user.watch(id))(game.gameId)));
        }

        $(".table-rooms-body").append(tr);
      }
    });
    while (contentCount < this.itemNum) {
      contentCount++;
      var tr = $("<tr style='height:39px;'>");
      tr.append($("<td>"));
      tr.append($("<td>"));
      tr.append($("<td>"));
      tr.append($("<td>"));
      tr.append($("<td>"));
      $(".table-rooms-body").append(tr);
    }
    $('.page-number').empty();
    $('.page-number').append((this.currentPage + 1) + '/' + this.getPageMax());
  }
}

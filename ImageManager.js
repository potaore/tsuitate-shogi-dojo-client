//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var Prom = (function () {
    function Prom() {
    }
    Prom.prototype.next = function (predicate) {
        this.predicate = predicate;
        this.nextProm = new Prom();
        this.nextProm.predicate = predicate();
        return this.nextProm;
    };
    Prom.prototype.exec = function (cont) {
        this.predicate();
    };
    return Prom;
})();
var ImageManager = (function () {
    function ImageManager() {
        this.komaImages = {
            'koma1': {},
            'koma2': {}
        };
        /*
        this.komaImages = {
          'koma1': {
            "ou": ImageManager.createKomaImage("sgl01.png"),
            "hi": ImageManager.createKomaImage("sgl02.png"),
            "ka": ImageManager.createKomaImage("sgl03.png"),
            "ki": ImageManager.createKomaImage("sgl04.png"),
            "gi": ImageManager.createKomaImage("sgl05.png"),
            "ke": ImageManager.createKomaImage("sgl06.png"),
            "ky": ImageManager.createKomaImage("sgl07.png"),
            "hu": ImageManager.createKomaImage("sgl08.png"),
            "ry": ImageManager.createKomaImage("sgl12.png"),
            "um": ImageManager.createKomaImage("sgl13.png"),
            "ng": ImageManager.createKomaImage("sgl15.png"),
            "nk": ImageManager.createKomaImage("sgl16.png"),
            "ny": ImageManager.createKomaImage("sgl17.png"),
            "to": ImageManager.createKomaImage("sgl18.png")
          },
          'koma2': {
            "ou": ImageManager.createKomaImage("sgl31.png"),
            "hi": ImageManager.createKomaImage("sgl32.png"),
            "ka": ImageManager.createKomaImage("sgl33.png"),
            "ki": ImageManager.createKomaImage("sgl34.png"),
            "gi": ImageManager.createKomaImage("sgl35.png"),
            "ke": ImageManager.createKomaImage("sgl36.png"),
            "ky": ImageManager.createKomaImage("sgl37.png"),
            "hu": ImageManager.createKomaImage("sgl38.png"),
            "ry": ImageManager.createKomaImage("sgl42.png"),
            "um": ImageManager.createKomaImage("sgl43.png"),
            "ng": ImageManager.createKomaImage("sgl45.png"),
            "nk": ImageManager.createKomaImage("sgl46.png"),
            "ny": ImageManager.createKomaImage("sgl47.png"),
            "to": ImageManager.createKomaImage("sgl48.png")
          }
        };
        */
    }
    ImageManager.prototype.load = function (cont) {
        var _this = this;
        this.loadKoma('koma1', 'ou', 'sgl01.png')
            .next(function () { return _this.loadKoma('koma1', 'hi', 'sgl02.png')
            .next(function () { return _this.loadKoma('koma1', 'ka', 'sgl03.png')
            .next(function () { return _this.loadKoma('koma1', 'ki', 'sgl04.png')
            .next(function () { return _this.loadKoma('koma1', 'gi', 'sgl05.png')
            .next(function () { return _this.loadKoma('koma1', 'ke', 'sgl06.png')
            .next(function () { return _this.loadKoma('koma1', 'ky', 'sgl07.png')
            .next(function () { return _this.loadKoma('koma1', 'hu', 'sgl08.png')
            .next(function () { return _this.loadKoma('koma1', 'ry', 'sgl12.png')
            .next(function () { return _this.loadKoma('koma1', 'um', 'sgl13.png')
            .next(function () { return _this.loadKoma('koma1', 'ng', 'sgl15.png')
            .next(function () { return _this.loadKoma('koma1', 'nk', 'sgl16.png')
            .next(function () { return _this.loadKoma('koma1', 'ny', 'sgl17.png')
            .next(function () { return _this.loadKoma('koma1', 'to', 'sgl18.png')
            .next(function () { return _this.loadKoma('koma2', 'ou', 'sgl31.png')
            .next(function () { return _this.loadKoma('koma2', 'hi', 'sgl32.png')
            .next(function () { return _this.loadKoma('koma2', 'ka', 'sgl33.png')
            .next(function () { return _this.loadKoma('koma2', 'ki', 'sgl34.png')
            .next(function () { return _this.loadKoma('koma2', 'gi', 'sgl35.png')
            .next(function () { return _this.loadKoma('koma2', 'ke', 'sgl36.png')
            .next(function () { return _this.loadKoma('koma2', 'ky', 'sgl37.png')
            .next(function () { return _this.loadKoma('koma2', 'hu', 'sgl38.png')
            .next(function () { return _this.loadKoma('koma2', 'ry', 'sgl42.png')
            .next(function () { return _this.loadKoma('koma2', 'um', 'sgl43.png')
            .next(function () { return _this.loadKoma('koma2', 'ng', 'sgl45.png')
            .next(function () { return _this.loadKoma('koma2', 'nk', 'sgl46.png')
            .next(function () { return _this.loadKoma('koma2', 'ny', 'sgl47.png')
            .next(function () { return _this.loadKoma('koma2', 'to', 'sgl48.png')
            .next(function () { return cont(); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); });
    };
    ImageManager.prototype.loadKoma = function (key, name, fileName) {
        var _this = this;
        var next = null;
        var cont = {
            next: function (predicate) {
                next = predicate;
            }
        };
        var img = ImageManager.createKomaImage(fileName);
        img.onload = function () {
            _this.komaImages[key][name] = img;
            img = null;
            if (next)
                next();
        };
        return cont;
    };
    ImageManager.createKomaImage = function (name) {
        var image = new Image();
        image.src = "./images/koma/60x64/" + name;
        image.width = 60;
        image.height = 64;
        return image;
    };
    ImageManager.prototype.getKomaImage = function (koma, flip) {
        if (flip) {
            if (koma.playerNumber === 2) {
                return this.komaImages.koma1[koma.komaType];
            }
            else {
                return this.komaImages.koma2[koma.komaType];
            }
        }
        else {
            if (koma.playerNumber === 1) {
                return this.komaImages.koma1[koma.komaType];
            }
            else {
                return this.komaImages.koma2[koma.komaType];
            }
        }
    };
    ImageManager.prototype.getIconImage = function (url, character) {
        var str;
        var image = new Image();
        if (character !== void 0 && character !== null) {
            str = ("0000" + character).slice(-4);
            image.src = "./images/icon/snap" + str + ".png";
            return image;
        }
        else if (url) {
            image.src = url;
            image.onerror = function () {
                image.src = "./images/icon/snap0014.png";
            };
            return image;
        }
        else {
            image.src = './images/icon/noname.jpeg';
            image.onerror = function () {
                image.src = "./images/icon/snap0014.png";
            };
            return image;
        }
    };
    ImageManager.prototype.getRandomCharacter = function () {
        return Math.floor(Math.random() * 55);
    };
    ImageManager.prototype.getRandomNums = function (length) {
        var result = [];
        var newNum = 0;
        while (result.length < length) {
            newNum = this.getRandomCharacter();
            if (!result.some(function (item) { return item == newNum; })) {
                result.push(newNum);
            }
        }
        return result;
    };
    ImageManager.prototype.getRandomCharacters = function (num) {
        var _this = this;
        return this.getRandomNums(10).map(function (num) { return { num: num, image: _this.getIconImage(null, num) }; });
    };
    return ImageManager;
})();
exports.ImageManager = ImageManager;

(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var UTIL = require('./utility');
var IDOBJ = require('./identified-object');
var Artifact = (function (_super) {
    __extends(Artifact, _super);
    function Artifact(name) {
        _super.call(this, name);
        this._connectedArtifacts = {};
        this._listnerArtifacts = {};
        this._broadcasterArtifacts = {};
        this._events = {};
        this._loopEvents = {};
    }
    Artifact.prototype.connect = function (anotherArtifacts) {
        anotherArtifacts._connectedArtifacts[this.id] = this;
        this._connectedArtifacts[anotherArtifacts.id] = anotherArtifacts;
        return this;
    };
    Artifact.prototype.listen = function (anotherArtifacts) {
        anotherArtifacts._listnerArtifacts[this.id] = this;
        this._broadcasterArtifacts[anotherArtifacts.id] = anotherArtifacts;
        return this;
    };
    Artifact.prototype.disconnect = function (anotherArtifacts) {
        delete anotherArtifacts._connectedArtifacts[this.id];
        delete this._connectedArtifacts[anotherArtifacts.id];
        delete anotherArtifacts._listnerArtifacts[this.id];
        delete this._broadcasterArtifacts[anotherArtifacts.id];
        return this;
    };
    Artifact.prototype.disconnectAll = function () {
        for (var anotherCellId in this._connectedArtifacts) {
            this.disconnect(this._connectedArtifacts[anotherCellId]);
        }
        return this;
    };
    Artifact.prototype.synchronizeConnectionInfo = function (anotherArtifact) {
        for (var key in anotherArtifact._listnerArtifacts) {
            anotherArtifact._listnerArtifacts[key].listen(this);
        }
        for (var key in anotherArtifact._broadcasterArtifacts) {
            this.listen(anotherArtifact._broadcasterArtifacts[key]);
        }
        for (var key in anotherArtifact._connectedArtifacts) {
            this.connect(anotherArtifact._connectedArtifacts[key]);
        }
        return this;
    };
    Artifact.prototype.on = function (eventName, eventImpl) {
        this._events[eventName] = eventImpl;
        return this;
    };
    Artifact.prototype.fire = function (eventName, sender, options, callback) {
        var _this = this;
        if (callback === void 0) { callback = Artifact.voidCallback; }
        if (this.localPointcut)
            this.localPointcut.fire(eventName, sender, options);
        var conversedEventName = this.convertEventName(eventName);
        if (this._events.hasOwnProperty(conversedEventName)) {
            this._events[conversedEventName](eventName, sender, options, function (result) { return callback({ artifact: _this, options: result }); });
        }
    };
    Artifact.prototype.notify = function (eventName, options, callback) {
        if (options === void 0) { options = {}; }
        if (callback === void 0) { callback = Artifact.voidCallback; }
        this.fireAllWithoutSender(eventName, this, options, callback);
    };
    Artifact.prototype.fireAllWithoutSender = function (eventName, sender, options, callback) {
        if (callback === void 0) { callback = Artifact.voidCallback; }
        for (var id in this._connectedArtifacts) {
            var cell = this._connectedArtifacts[id];
            if (cell.notEquals(sender)) {
                var result = cell.fire(eventName, sender, options, callback);
            }
        }
        for (var id in this._listnerArtifacts) {
            var cell = this._listnerArtifacts[id];
            if (cell.notEquals(sender)) {
                var result = cell.fire(eventName, sender, options, callback);
            }
        }
    };
    Artifact.prototype.convertEventName = function (eventName) {
        return eventName;
    };
    Artifact.prototype.startRepeat = function (eventName, impl, interval) {
        if (this._loopEvents.hasOwnProperty(eventName)) {
            return;
        }
        this._loopEvents[eventName] = new Repeater(eventName, interval, impl);
        this._loopEvents[eventName].start();
    };
    Artifact.prototype.stopRepeat = function (eventName) {
        if (this._loopEvents.hasOwnProperty(eventName)) {
            return;
        }
        this._loopEvents[eventName].stop();
        delete this._loopEvents[eventName];
    };
    Artifact.prototype.destroy = function () {
        for (var id in this._connectedArtifacts) {
            this.disconnect(this._connectedArtifacts[id]);
        }
        delete this._events;
    };
    Artifact.voidCallback = function () { };
    return Artifact;
})(IDOBJ.IdentifiedObject);
exports.Artifact = Artifact;
var Listner = (function (_super) {
    __extends(Listner, _super);
    function Listner(logger, showdata) {
        if (showdata === void 0) { showdata = false; }
        _super.call(this, 'listner');
        this.localPointcut = {
            fire: function (eventName, sender, options) {
                var cache = [];
                if (showdata) {
                    logger.info('"' + sender.name + '"' + '::' + '"' + eventName + '"' + '::' + JSON.stringify(options, function (key, value) {
                        if ((typeof value == 'object') && (value != null)) {
                            if (-1 < cache.indexOf(value))
                                return;
                            cache.push(value);
                        }
                        return value;
                    }));
                }
                else {
                    logger.info('"' + sender.name + '"' + '::' + '"' + eventName + '"');
                }
            }
        };
    }
    return Listner;
})(Artifact);
exports.Listner = Listner;
var Repeater = (function (_super) {
    __extends(Repeater, _super);
    function Repeater(name, interval, impl) {
        _super.call(this, name);
        this.running = false;
        this.impl = impl;
        this.interval = interval;
    }
    Repeater.prototype.start = function () {
        var _this = this;
        if (this.running)
            return;
        this.running = true;
        var loop = function () {
            if (!_this.running)
                return;
            var startTime = UTIL.Utility.getTime();
            _this.impl();
            var endTime = UTIL.Utility.getTime();
            var delay = _this.interval - endTime + startTime;
            console.log('delay : ' + delay);
            setTimeout(loop, delay < 0 ? 0 : delay);
        };
        loop();
    };
    Repeater.prototype.stop = function () {
        this.running = false;
    };
    return Repeater;
})(IDOBJ.IdentifiedObject);
exports.Repeater = Repeater;
var ScenarioPublisher = (function (_super) {
    __extends(ScenarioPublisher, _super);
    function ScenarioPublisher(name) {
        _super.call(this, name);
        this.scenarios = {};
    }
    ScenarioPublisher.prototype.createScenario = function (scenarioName, buildAction) {
        var _this = this;
        var startEvent = new ScenarioEventStart();
        buildAction(new EventChainBuilder(startEvent));
        return this.scenarios[scenarioName] = function (options) { return startEvent.execute({ artifact: _this, options: options }, function () { }); };
    };
    ScenarioPublisher.prototype.executeScenario = function (scenarioName, options) {
        this.scenarios[scenarioName](options);
    };
    return ScenarioPublisher;
})(Artifact);
exports.ScenarioPublisher = ScenarioPublisher;
var ScenarioEvent = (function () {
    function ScenarioEvent() {
        this.nexts = [];
    }
    ScenarioEvent.prototype.setNext = function (next) {
        this.nexts.push(next);
    };
    ScenarioEvent.prototype.execute = function (eventContext, callback) {
        var _this = this;
        this.executeImpl(eventContext, function (result) {
            var i = 0;
            while (i < _this.nexts.length) {
                _this.nexts[i].execute(result, callback);
                i = (i + 1) | 0;
            }
        });
    };
    return ScenarioEvent;
})();
exports.ScenarioEvent = ScenarioEvent;
var ScenarioEventStart = (function (_super) {
    __extends(ScenarioEventStart, _super);
    function ScenarioEventStart() {
        _super.call(this);
    }
    ScenarioEventStart.prototype.executeImpl = function (eventContext, callback) {
        callback(eventContext);
    };
    return ScenarioEventStart;
})(ScenarioEvent);
exports.ScenarioEventStart = ScenarioEventStart;
var ScenarioEventFire = (function (_super) {
    __extends(ScenarioEventFire, _super);
    function ScenarioEventFire(eventName) {
        _super.call(this);
        this.eventName = eventName;
    }
    ScenarioEventFire.prototype.executeImpl = function (eventContext, callback) {
        eventContext.artifact.fire(this.eventName, eventContext.artifact, eventContext.options, callback);
    };
    return ScenarioEventFire;
})(ScenarioEvent);
exports.ScenarioEventFire = ScenarioEventFire;
var ScenarioEventNotify = (function (_super) {
    __extends(ScenarioEventNotify, _super);
    function ScenarioEventNotify(eventName) {
        _super.call(this);
        this.eventName = eventName;
    }
    ScenarioEventNotify.prototype.executeImpl = function (eventContext, callback) {
        eventContext.artifact.notify(this.eventName, eventContext.options, callback);
    };
    return ScenarioEventNotify;
})(ScenarioEvent);
exports.ScenarioEventNotify = ScenarioEventNotify;
var ScenarioEventWhere = (function (_super) {
    __extends(ScenarioEventWhere, _super);
    function ScenarioEventWhere(guard) {
        _super.call(this);
        this.guard = guard;
    }
    ScenarioEventWhere.prototype.executeImpl = function (eventContext, callback) {
        if (this.guard(eventContext.options)) {
            callback(eventContext);
        }
    };
    return ScenarioEventWhere;
})(ScenarioEvent);
exports.ScenarioEventWhere = ScenarioEventWhere;
var ScenarioEventPointcut = (function (_super) {
    __extends(ScenarioEventPointcut, _super);
    function ScenarioEventPointcut(pointcut) {
        _super.call(this);
        this.pointcut = pointcut;
    }
    ScenarioEventPointcut.prototype.executeImpl = function (eventContext, callback) {
        var newEventContext = this.pointcut(eventContext);
        callback(newEventContext);
    };
    return ScenarioEventPointcut;
})(ScenarioEvent);
exports.ScenarioEventPointcut = ScenarioEventPointcut;
var EventChainBuilder = (function () {
    function EventChainBuilder(prevEvent) {
        this.prevEvent = prevEvent;
    }
    EventChainBuilder.prototype.fire = function (eventName) {
        var ev = new ScenarioEventFire(eventName);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    EventChainBuilder.prototype.notify = function (eventName) {
        var ev = new ScenarioEventNotify(eventName);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    EventChainBuilder.prototype.where = function (guard) {
        var ev = new ScenarioEventWhere(guard);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    EventChainBuilder.prototype.pointcut = function (pointcut) {
        var ev = new ScenarioEventPointcut(pointcut);
        this.prevEvent.setNext(ev);
        return new EventChainBuilder(ev);
    };
    return EventChainBuilder;
})();
exports.EventChainBuilder = EventChainBuilder;

},{"./identified-object":2,"./utility":3}],2:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var UTIL = require('./utility');
var IdentifiedObject = (function () {
    function IdentifiedObject(name) {
        this.id = UTIL.Utility.uuid();
        this.name = name;
    }
    IdentifiedObject.prototype.equals = function (another) {
        return this.id === another.id;
    };
    IdentifiedObject.prototype.notEquals = function (another) {
        return this.id !== another.id;
    };
    return IdentifiedObject;
})();
exports.IdentifiedObject = IdentifiedObject;
var ReversibleMap = (function () {
    function ReversibleMap() {
        this.map = {};
        this.reverseMap = {};
    }
    ReversibleMap.prototype.put = function (key, value) {
        this.map[key] = value;
        if (!this.reverseMap.hasOwnProperty(value)) {
            this.reverseMap[value] = {};
        }
        this.reverseMap[value][key] = value;
    };
    ReversibleMap.prototype.remove = function (key) {
        var value = this.map[key];
        delete this.reverseMap[value][key];
        delete this.map[key];
    };
    ReversibleMap.prototype.getReverse = function (value) {
        if (!this.reverseMap.hasOwnProperty(value)) {
            this.reverseMap[value] = {};
        }
        return this.reverseMap[value];
    };
    return ReversibleMap;
})();
exports.ReversibleMap = ReversibleMap;
var IdentifiedObjectContainer = (function () {
    function IdentifiedObjectContainer() {
        this.glovalMap = {};
        this.groupMap = new ReversibleMap();
        this.tagMap = {};
    }
    IdentifiedObjectContainer.prototype.put = function (object, group) {
        if (group === void 0) { group = ''; }
        this.glovalMap[object.id] = object;
        this.groupMap.put(object.id, group);
    };
    IdentifiedObjectContainer.prototype.get = function (id) { return this.glovalMap[id]; };
    IdentifiedObjectContainer.prototype.changeGroup = function (object, group) {
        if (group === void 0) { group = ''; }
        this.groupMap.remove(object.id);
        this.groupMap.put(object.id, group);
    };
    IdentifiedObjectContainer.prototype.remove = function (object) {
        this.groupMap.remove(object.id);
        delete this.glovalMap[object.id];
    };
    IdentifiedObjectContainer.prototype.getGroup = function (group) {
        return this.groupMap.getReverse(group);
    };
    return IdentifiedObjectContainer;
})();
exports.IdentifiedObjectContainer = IdentifiedObjectContainer;

},{"./utility":3}],3:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var Utility = (function () {
    function Utility() {
    }
    Utility.getTime = function () { return new Date().getTime(); };
    Utility.uuid = function () {
        var uuid = "";
        for (var i = 0; i < 32; i++) {
            var random = Math.random() * 16 | 0;
            if (i == 8 || i == 12 || i == 16 || i == 20)
                uuid += "-";
            uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    };
    return Utility;
})();
exports.Utility = Utility;

},{}],4:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var util = (function () {
    function util() {
    }
    util.isVacant = function (val) {
        return val == null || val == undefined || val == '' || val == 0;
    };
    util.isntVacant = function (val) {
        return val != null && val != undefined && val != '' && val != 0;
    };
    util.reverseVector = function (position, playerNumber) {
        if (playerNumber == 1)
            return position;
        return { x: position.x * -1, y: position.y * -1, length: position.length };
    };
    util.posEq = function (p1, p2) {
        return p1.x == p2.x && p1.y == p2.y;
    };
    util.posAdd = function (p, v) {
        return { x: p.x + v.x, y: p.y + v.y, length: v.length };
    };
    util.posSub = function (p, v) {
        return { x: p.x - v.x, y: p.y - v.y, length: v.length };
    };
    util.posMul = function (p, times) {
        return { x: p.x * times, y: p.y * times, length: times };
    };
    util.posDup = function (p) {
        return { x: p.x, y: p.y, length: p.length };
    };
    util.posAdd1 = function (p) {
        return { x: p.x + 1, y: p.y + 1, length: p.length };
    };
    util.posFlip = function (p, flip) {
        return flip ? { x: 8 - p.x, y: 8 - p.y } : { x: p.x, y: p.y };
    };
    util.posFlipX = function (p) {
        return { x: 8 - p.x, y: p.y };
    };
    util.posToStr = function (p) {
        return p.x + ':' + p.y;
    };
    util.isInRange = function (pos) {
        return 0 <= pos.x && pos.x < 9 && 0 <= pos.y && pos.y < 9;
    };
    util.allPos = function (predicate) {
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                predicate(x, y);
            }
        }
    };
    util.getArroundPos = function (p) {
        return ([
            { x: p.x - 1, y: p.y - 1 },
            { x: p.x + 0, y: p.y - 1 },
            { x: p.x + 1, y: p.y - 1 },
            { x: p.x - 1, y: p.y + 0 },
            { x: p.x + 1, y: p.y + 0 },
            { x: p.x - 1, y: p.y + 1 },
            { x: p.x + 0, y: p.y + 1 },
            { x: p.x + 1, y: p.y + 1 }
        ]).filter(function (pos) { return util.isInRange(pos); });
    };
    util.find = function (array, predicate) {
        if (array == null) {
            return null;
        }
        if (typeof predicate !== 'function') {
            return null;
        }
        var list = Object(array);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;
        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return null;
    };
    return util;
})();
exports.util = util;
var KomaType = (function () {
    function KomaType(id, name, nari, resourceId, nariId, moveDef, value) {
        this.id = id;
        this.name = name;
        this.nari = nari;
        this.resourceId = resourceId;
        this.nariId = nariId;
        this.moveDef = moveDef;
        this.value = value;
    }
    return KomaType;
})();
exports.KomaType = KomaType;
exports.komaTypes = (function () {
    var _komaTypes = {};
    var setResource = function (id, name, nari, resourceId, nariId, moveDef, value) {
        _komaTypes[id] = new KomaType(id, name, nari, resourceId, nariId, moveDef, value);
    };
    setResource('ou', '玉', false, 'ou', '', { str: '12346789', moves: ['1', '2', '3', '4', '6', '7', '8', '9'] }, 9);
    setResource('ki', '金', false, 'ki', '', { str: '123468', moves: ['1', '2', '3', '4', '6', '8'] }, 5);
    setResource('gi', '銀', true, 'gi', 'ng', { str: '12379', moves: ['1', '2', '3', '7', '9'] }, 4);
    setResource('ng', '成銀', false, 'gi', '', { str: '123468', moves: ['1', '2', '3', '4', '6', '8'] }, 4);
    setResource('ke', '桂', true, 'ke', 'nk', { str: 'ab', moves: ['a', 'b'] }, 3);
    setResource('nk', '成桂', false, 'ke', '', { str: '123468', moves: ['1', '2', '3', '4', '6', '8'] }, 3);
    setResource('ky', '香', true, 'ky', 'ny', { str: '2_', moves: ['2_'] }, 2);
    setResource('ny', '成香', false, 'ky', '', { str: '123468', moves: ['1', '2', '3', '4', '6', '8'] }, 2);
    setResource('hi', '飛', true, 'hi', 'ry', { str: '2_4_6_8_', moves: ['2_', '4_', '6_', '8_'] }, 7);
    setResource('ry', '竜', false, 'hi', '', { str: '12_34_6_78_9', moves: ['1', '2_', '3', '4_', '6_', '7', '8_', '9'] }, 7);
    setResource('ka', '角', true, 'ka', 'um', { str: '1_3_7_9_', moves: ['1_', '3_', '7_', '9_'] }, 6);
    setResource('um', '馬', false, 'ka', '', { str: '1_23_467_89_', moves: ['1_', '2', '3_', '4', '6', '7_', '8', '9_'] }, 6);
    setResource('hu', '歩', true, 'hu', 'to', { str: '2', moves: ['2'] }, 1);
    setResource('to', 'と', false, 'hu', '', { str: '123468', moves: ['1', '2', '3', '4', '6', '8'] }, 1);
    return _komaTypes;
})();
exports.getKomaName = function (id) {
    if (exports.komaTypes[id].resourceId)
        return exports.komaTypes[exports.komaTypes[id].resourceId].name;
    else
        return exports.komaTypes[id].name;
};
var MoveConvertor = (function () {
    function MoveConvertor() {
        var _this = this;
        var create8Aray = function (v) {
            return ([1, 2, 3, 4, 5, 6, 7, 8]).map(function (i) { return util.posMul(v, i); });
        };
        this.moveDef = [
            ['1', [{ x: 1, y: -1, length: 1 }]],
            ['2', [{ x: 0, y: -1, length: 1 }]],
            ['3', [{ x: -1, y: -1, length: 1 }]],
            ['4', [{ x: 1, y: 0, length: 1 }]],
            ['6', [{ x: -1, y: 0, length: 1 }]],
            ['7', [{ x: 1, y: 1, length: 1 }]],
            ['8', [{ x: 0, y: 1, length: 1 }]],
            ['9', [{ x: -1, y: 1, length: 1 }]],
            ['a', [{ x: 1, y: -2, length: 1 }]],
            ['b', [{ x: -1, y: -2, length: 1 }]],
            ['1_', create8Aray({ x: 1, y: -1 })],
            ['2_', create8Aray({ x: 0, y: -1 })],
            ['3_', create8Aray({ x: -1, y: -1 })],
            ['4_', create8Aray({ x: 1, y: 0 })],
            ['6_', create8Aray({ x: -1, y: 0 })],
            ['7_', create8Aray({ x: 1, y: 1 })],
            ['8_', create8Aray({ x: 0, y: 1 })],
            ['9_', create8Aray({ x: -1, y: 1 })]
        ];
        this.moveDef1 = {};
        this.moveDef2 = {};
        this.moveDef.forEach(function (pair) {
            _this.moveDef1[pair[0]] = pair[1];
            _this.moveDef2[pair[0]] = pair[1].map(function (v) { return util.reverseVector(v, 2); });
        });
        this.moveKeysShort = { '1': true, '2': true, '3': true, '4': true, '6': true, '7': true, '8': true, '9': true, 'a': true, 'b': true };
        this.moveKeysLong = { '1_': true, '2_': true, '3_': true, '4_': true, '6_': true, '7_': true, '8_': true, '9_': true };
    }
    MoveConvertor.prototype.cutLength = function (playerNumber, positions, gameBoard) {
        var ngLength = 0;
        var ngPos = util.find(positions, function (pos) { return util.isntVacant(gameBoard.getKoma(pos)); });
        if (ngPos) {
            var toKoma = gameBoard.getKoma(ngPos);
            ngLength = toKoma.playerNumber == playerNumber ? ngPos.length : ngPos.length + 1;
        }
        return ngLength != 0 ? positions.filter(function (pos) { return pos.length < ngLength; }) : positions;
    };
    MoveConvertor.prototype.getMovablePos = function (playerNumber, komaType, position, gameBoard) {
        var _this = this;
        var moves = exports.komaTypes[komaType].moveDef.moves;
        var _moveDef = playerNumber == 1 ? this.moveDef1 : this.moveDef2;
        var result = [];
        moves.forEach(function (move) {
            var positions = _moveDef[move].map(function (v) { return util.posAdd(position, v); });
            positions = positions.filter(function (pos) { return util.isInRange(pos); });
            positions = _this.cutLength(playerNumber, positions, gameBoard);
            result = result.concat(positions);
        });
        return result;
    };
    MoveConvertor.prototype.getMovablePosShort = function (playerNumber, komaType, position, gameBoard) {
        var _this = this;
        var moves = exports.komaTypes[komaType].moveDef.moves;
        moves = moves.map(function (move) { return move.replace('_', ''); });
        var _moveDef = playerNumber == 1 ? this.moveDef1 : this.moveDef2;
        var result = [];
        moves.forEach(function (move) {
            var positions = _moveDef[move].map(function (v) { return util.posAdd(position, v); });
            positions = positions.filter(function (pos) { return util.isInRange(pos); });
            positions = _this.cutLength(playerNumber, positions, gameBoard);
            result = result.concat(positions);
        });
        return result;
    };
    MoveConvertor.prototype.getPutableCell = function (playerNumber, komaType, gameBoard) {
        var positions = [];
        util.allPos(function (x, y) {
            if (util.isVacant(gameBoard.getKoma({ x: x, y: y }))) {
                positions.push({ x: x, y: y });
            }
        });
        var filter = null;
        if (komaType == 'hu' || komaType == 'ky') {
            filter = playerNumber == 1 ? function (p) { return p.y != 0; } : function (p) { return p.y != 8; };
        }
        if (komaType == 'ke') {
            filter = playerNumber == 1 ? function (p) { return p.y > 1; } : function (p) { return p.y < 7; };
        }
        if (komaType == 'hu') {
            var columns = gameBoard.getExistsHuColumn(playerNumber);
            positions = positions.filter(function (p) { return columns[p.x] ? false : true; });
        }
        return filter ? positions.filter(filter) : positions;
    };
    MoveConvertor.prototype.getNari = function (playerNumber, komaType, fromPos, toPos) {
        if (exports.komaTypes[komaType].nari == false)
            return 'none';
        if (playerNumber == 1) {
            if (fromPos.y < 3 || toPos.y < 3) {
                if ((komaType == 'hu' || komaType == 'ky') && toPos.y == 0)
                    return 'force';
                if (komaType == 'ke' && toPos.y < 2)
                    return 'force';
                return 'possible';
            }
            else {
                return 'none';
            }
        }
        else {
            if (fromPos.y > 5 || toPos.y > 5) {
                if ((komaType == 'hu' || komaType == 'ky') && toPos.y == 8)
                    return 'force';
                if (komaType == 'ke' && toPos.y > 6)
                    return 'force';
                return 'possible';
            }
            else {
                return 'none';
            }
        }
    };
    return MoveConvertor;
})();
exports.MoveConvertor = MoveConvertor;
exports.moveConvertor = new MoveConvertor();
var Koma = (function () {
    function Koma(playerNumber, komaType) {
        this.playerNumber = playerNumber;
        this.komaType = komaType;
    }
    Koma.km1 = function (komaType) { return new Koma(1, komaType); };
    Koma.km2 = function (komaType) { return new Koma(2, komaType); };
    return Koma;
})();
exports.Koma = Koma;
var Board = (function () {
    function Board(player1, player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.komaDai = [[], []];
        this.board = [[], [], [], [], [], [], [], [], []];
        this.life = [9, 9];
    }
    Board.prototype.clearBoard = function () {
        this.board = [[], [], [], [], [], [], [], [], []];
    };
    Board.prototype.initBoard = function (playerNumber) {
        if (playerNumber == 1) {
            this.board = [
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                [Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu')],
                ['', Koma.km1('hi'), '', '', '', '', '', Koma.km1('ka'), ''],
                [Koma.km1('ky'), Koma.km1('ke'), Koma.km1('gi'), Koma.km1('ki'), Koma.km1('ou'), Koma.km1('ki'), Koma.km1('gi'), Koma.km1('ke'), Koma.km1('ky')]
            ];
        }
        else if (playerNumber == 2) {
            this.board = [
                [Koma.km2('ky'), Koma.km2('ke'), Koma.km2('gi'), Koma.km2('ki'), Koma.km2('ou'), Koma.km2('ki'), Koma.km2('gi'), Koma.km2('ke'), Koma.km2('ky')],
                ['', Koma.km2('ka'), '', '', '', '', '', Koma.km2('hi'), ''],
                [Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu')],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', '']
            ];
        }
        else {
            this.board = [
                [Koma.km2('ky'), Koma.km2('ke'), Koma.km2('gi'), Koma.km2('ki'), Koma.km2('ou'), Koma.km2('ki'), Koma.km2('gi'), Koma.km2('ke'), Koma.km2('ky')],
                ['', Koma.km2('ka'), '', '', '', '', '', Koma.km2('hi'), ''],
                [Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu')],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                ['', '', '', '', '', '', '', '', ''],
                [Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu')],
                ['', Koma.km1('hi'), '', '', '', '', '', Koma.km1('ka'), ''],
                [Koma.km1('ky'), Koma.km1('ke'), Koma.km1('gi'), Koma.km1('ki'), Koma.km1('ou'), Koma.km1('ki'), Koma.km1('gi'), Koma.km1('ke'), Koma.km1('ky')]
            ];
        }
    };
    Board.prototype.duplicate = function () {
        var board = new Board(this.player1, this.player2);
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.board[x][y]) {
                    board.board[x][y] = new Koma(this.board[x][y].playerNumber, this.board[x][y].komaType);
                }
                else {
                    board.board[x][y] = '';
                }
            }
        }
        board.komaDai[0] = board.komaDai[0].concat(this.komaDai[0]);
        board.komaDai[1] = board.komaDai[1].concat(this.komaDai[1]);
        board.life[0] = this.life[0];
        board.life[1] = this.life[1];
        return board;
    };
    Board.prototype.getKoma = function (position) {
        if (util.isInRange(position)) {
            return this.board[position.y][position.x];
        }
        else {
            return "";
        }
    };
    Board.prototype.getExistsHuColumn = function (playerNumber) {
        var result = {};
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                var koma = this.board[y][x];
                if (koma && koma.playerNumber == playerNumber && koma.komaType == 'hu') {
                    result[x] = true;
                }
            }
        }
        return result;
    };
    Board.prototype.putKoma = function (koma, position) {
        this.board[position.y][position.x] = koma;
    };
    Board.prototype.removeKoma = function (position) {
        this.board[position.y][position.x] = undefined;
    };
    Board.prototype.getKomaFromKomadai = function (playerNumber, komaType) {
        var find = false;
        var index = playerNumber == 1 ? 0 : 1;
        var koma = util.find(this.komaDai[index], function (koma) { return koma.komaType == komaType; });
        this.komaDai[index] = this.komaDai[index].filter(function (koma) {
            if (find)
                return true;
            if (koma.komaType == komaType)
                return !(find = true);
            return true;
        });
        this.sortKomadai();
        return koma;
    };
    Board.prototype.putKomaToKomadai = function (playerNumber, koma) {
        koma = new Koma(koma.playerNumber, koma.komaType);
        koma.playerNumber = playerNumber;
        var type = exports.komaTypes[koma.komaType];
        koma.komaType = exports.komaTypes[koma.komaType].resourceId;
        if (playerNumber == 1) {
            this.komaDai[0].push(koma);
            this.komaDai[0] = this.komaDai[0].sort(function (koma) { return exports.komaTypes[koma.komaType].value; });
        }
        else {
            this.komaDai[1].push(koma);
            this.komaDai[1] = this.komaDai[1].sort(function (koma) { return exports.komaTypes[koma.komaType].value; });
        }
        this.sortKomadai();
    };
    Board.prototype.removeKomaFromKomadai = function (playerNumber, komaType) {
        var find = false;
        var index = playerNumber == 1 ? 0 : 1;
        var koma = util.find(this.komaDai[index], function (koma) { return koma.komaType == komaType; });
        this.komaDai[index] = this.komaDai[index].filter(function (koma) {
            if (find)
                return true;
            if (koma.komaType == komaType)
                return !(find = true);
            return true;
        });
        this.sortKomadai();
        return koma;
    };
    Board.prototype.sortKomadai = function () {
        this.komaDai[0] = this.komaDai[0].sort(function (a, b) {
            return exports.komaTypes[a.komaType].value - exports.komaTypes[b.komaType].value;
        });
        this.komaDai[1] = this.komaDai[1].sort(function (a, b) {
            return exports.komaTypes[a.komaType].value - exports.komaTypes[b.komaType].value;
        });
    };
    Board.prototype.getKomaWithPosition = function (predicate) {
        var result = [];
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                var koma = this.board[y][x];
                if (koma && predicate(koma)) {
                    result.push({ koma: koma, position: { x: x, y: y } });
                }
            }
        }
        return result;
    };
    Board.prototype.getPlayerKoma = function (playerNumber) {
        return this.getKomaWithPosition(function (koma) { return koma.playerNumber == playerNumber; });
    };
    Board.prototype.getPlayerKomadai = function (playerNumber) {
        var komadaiKoma = this.komaDai[playerNumber - 1].map(function (koma) {
            return { koma: koma, position: { x: -1, y: -1 } };
        });
        return komadaiKoma;
    };
    Board.prototype.getOu = function (playerNumber) {
        var ou = this.getKomaWithPosition(function (koma) { return koma.playerNumber == playerNumber && koma.komaType == 'ou'; });
        return ou[0] ? ou[0] : null;
    };
    Board.create = function (rowBord, playerInfo) {
        var board = new Board(playerInfo.player1, playerInfo.player2);
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (rowBord.board[x][y]) {
                    board.board[x][y] = new Koma(rowBord.board[x][y].playerNumber, rowBord.board[x][y].komaType);
                }
                else {
                    board.board[x][y] = '';
                }
            }
        }
        board.komaDai[0] = board.komaDai[0].concat(rowBord.komaDai[0]);
        board.komaDai[1] = board.komaDai[1].concat(rowBord.komaDai[1]);
        return board;
    };
    return Board;
})();
exports.Board = Board;
var SyogiJudgement = (function () {
    function SyogiJudgement() {
    }
    SyogiJudgement.prototype.isOute = function (game, playerNumber) {
        var kikiMap = {};
        var enemyPlayerNumber = playerNumber == 1 ? 2 : 1;
        game.board.getPlayerKoma(enemyPlayerNumber).forEach(function (komaInfo) {
            var postions = exports.moveConvertor.getMovablePos(enemyPlayerNumber, komaInfo.koma.komaType, komaInfo.position, game.board);
            postions = postions.map(function (pos) { return util.posToStr(pos); });
            postions.forEach(function (p) { return kikiMap[p] = true; });
        });
        var ou = game.board.getOu(playerNumber);
        var ouPos = util.posToStr(ou.position);
        return kikiMap[ouPos] ? true : false;
    };
    return SyogiJudgement;
})();
exports.SyogiJudgement = SyogiJudgement;
var Game = (function (_super) {
    __extends(Game, _super);
    function Game(playerNumber, player1, player2) {
        var _this = this;
        _super.call(this, 'game');
        this.playerNumber = playerNumber;
        this.player1 = player1;
        this.player2 = player2;
        this.board = new Board(this.player1, this.player2);
        this.board.initBoard(this.playerNumber);
        this.on('get-game', function (eventName, sender, options, callback) { callback(_this); });
        this.on('receive-game-command', function (eventName, sender, commands) { return _this.doCommand(commands); });
    }
    Game.prototype.getPlayerInfoCommand = function () {
        return {
            method: 'adjustTime',
            player1: this.board.player1,
            player2: this.board.player2
        };
    };
    Game.prototype.start = function () {
        this.turn = 1;
        this.state = 'playing';
        this.currentTime = Date.now();
        this.repeatUpdateTimer();
    };
    Game.prototype.repeatUpdateTimer = function () {
        var _this = this;
        var updateTimerLoop = function () {
            if (_this.state != 'playing')
                return;
            _this.updateTimer();
            window.setTimeout(updateTimerLoop, 500);
        };
        window.setTimeout(updateTimerLoop, 200);
    };
    Game.prototype.updateTimer = function () {
        if (this.state != 'playing')
            return;
        var oldTime = this.currentTime;
        this.currentTime = Date.now();
        var player = this.getCurrentPlayer();
        player.time -= this.currentTime - oldTime;
        this.notify('game-update-info', this.getPlayerInfoCommand());
        if (player.time < 0) {
            this.notify('game-timeup', {
                method: 'timeout',
                playerNumber: 2 - this.turn % 2
            });
        }
    };
    Game.prototype.isPlayerTurn = function () {
        return (2 - this.turn % 2) == this.playerNumber;
    };
    Game.prototype.nextTurn = function () {
        this.turn++;
    };
    Game.prototype.end = function () {
        this.state = 'end';
    };
    Game.prototype.getCurrentPlayer = function () {
        return this.turn % 2 == 1 ? this.board.player1 : this.board.player2;
    };
    Game.prototype.synchronizeGameInfo = function (bord, turn, playerInfo) {
        this.turn = turn;
        this.board = Board.create(bord, playerInfo);
    };
    Game.prototype.doCommand = function (commands) {
        var _this = this;
        var isFoul = false;
        var invatedPosition = null;
        var getKomaFlag = false;
        var outeFlag = false;
        var contradiction = false;
        commands.forEach(function (command) {
            if (command.method == 'removeKomaFromKomadai') {
                _this.board.removeKomaFromKomadai(command.playerNumber, command.koma.komaType);
            }
            else if (command.method == 'putKomaToKomadai') {
                _this.board.putKomaToKomadai(command.playerNumber, command.koma);
                _this.notify('game-get-koma', command);
            }
            else if (command.method == 'removeKoma') {
                _this.board.removeKoma(command.position);
                invatedPosition = command.position;
            }
            else if (command.method == 'putKoma') {
                _this.board.putKoma(command.koma, command.position);
                invatedPosition = command.position;
            }
            else if (command.method == 'adjustTime') {
                _this.board.player1.time = command.player1.time;
                _this.board.player2.time = command.player2.time;
                _this.notify('game-update-info', command);
            }
            else if (command.method == 'foul') {
                _this.getCurrentPlayer().life--;
                _this.notify('game-update-info', _this.getPlayerInfoCommand());
                isFoul = true;
            }
            else if (command.method == 'noticeOute') {
                outeFlag = command.oute;
                _this.notify('game-notice-oute', command);
            }
            else if (command.method == 'noticeGetKoma') {
                getKomaFlag = command.getKoma;
            }
            else if (command.method == 'contradiction') {
                contradiction = true;
                _this.notify('game-need-synchronize');
            }
        });
        if (!contradiction) {
            if (isFoul) {
                this.notify('sound-foul');
                this.notify('game-board-initialize-table-state');
            }
            else {
                this.notify('game-board-set-invated-position', invatedPosition);
                this.notify('game-set-turn', this.turn);
                if (outeFlag) {
                    this.notify('sound-oute');
                }
                else if (getKomaFlag) {
                    this.notify('sound-get');
                }
                else {
                    this.notify('sound-koma');
                }
                this.nextTurn();
            }
            this.notify('game-redraw-board', this);
        }
    };
    return Game;
})(Artifact.Artifact);
exports.Game = Game;
var Kifu = (function (_super) {
    __extends(Kifu, _super);
    function Kifu(kifu, playerNumber, refresh, player1, player2) {
        var _this = this;
        _super.call(this, 'kifu-player');
        this.state = 'watch';
        this.index = 0;
        this.elapsedTime = 0;
        this.boards = [];
        this.syogiJudgement = new SyogiJudgement();
        this.continueUpdateTimer = false;
        this.kifu = kifu;
        this.playerNumber = playerNumber;
        this.refresh = refresh;
        this.player1 = player1;
        this.player2 = player2;
        this.index = 0;
        this.currentTime = Date.now();
        this.board = new Board(player1, player2);
        this.board.initBoard();
        this.currentBoard = this.board;
        this.boards.push(this.board);
        this.kifu.forEach(function (te) { return _this.readTe(te); });
    }
    Kifu.prototype.getPlayerInfo = function () {
        var board = this.boards[this.boards.length - 1];
        var player1, player2;
        if (this.currentPlayerNumber == 1) {
            player1 = { life: board.player1.life, time: board.player1.time + this.currentTime - Date.now() - this.elapsedTime };
            player2 = board.player2;
        }
        else {
            player1 = board.player1;
            player2 = { life: board.player2.life, time: board.player2.time + this.currentTime - Date.now() - this.elapsedTime };
        }
        return { player1: player1, player2: player2 };
    };
    Kifu.prototype.getCurrentBoard = function () {
        return this.boards[this.index];
    };
    Kifu.prototype.addKifu = function (te) { this.readTe(te); };
    Kifu.prototype.readTe = function (te) {
        var currentBoard = this.boards[this.boards.length - 1];
        currentBoard = currentBoard.duplicate();
        var currentPlayerNumber = 2 - te.turn % 2;
        currentBoard.player1 = te.playerInfo.player1;
        currentBoard.player2 = te.playerInfo.player2;
        currentBoard.events = [];
        this.currentTime = Date.now();
        currentBoard.turn = te.turn;
        if (te.info.type == 'moveKoma') {
            if (te.foul) {
                currentBoard.events.push({ 'type': 'foul' });
                if (currentPlayerNumber == 1) {
                    currentBoard.player1.life--;
                }
                else {
                    currentBoard.player2.life--;
                }
            }
            if (!te.foul) {
                if (te.info.from.x != -1) {
                    currentBoard.removeKoma(te.info.from);
                    var toKoma = currentBoard.getKoma(te.info.to);
                }
                else {
                    currentBoard.removeKomaFromKomadai(currentPlayerNumber, te.info.koma);
                }
                if (toKoma) {
                    currentBoard.removeKoma(te.info.to);
                    currentBoard.putKomaToKomadai(currentPlayerNumber, new Koma(currentPlayerNumber, toKoma.komaType));
                    currentBoard.events.push({ 'type': 'get', 'komaType': toKoma.komaType });
                }
                currentBoard.putKoma(new Koma(currentPlayerNumber, te.info.koma), te.info.to);
                currentBoard.emphasisPosition = te.info.to;
                this.currentPlayerNumber = 2 - (te.turn + 1) % 2;
                if (this.syogiJudgement.isOute({ 'board': currentBoard }, this.currentPlayerNumber)) {
                    currentBoard.events.push({ 'type': 'oute' });
                }
            }
            else {
                currentBoard.life[currentPlayerNumber] = currentBoard.life[currentPlayerNumber] - 1;
                this.currentPlayerNumber = 2 - te.turn % 2;
            }
        }
        else if (te.info.type == 'endGame') {
            currentBoard.endGame = true;
            currentBoard.events.push({ 'type': 'endGame', 'winPlayerNumber': te.info.winPlayerNumber });
        }
        this.boards.push(currentBoard);
    };
    Kifu.prototype.toStart = function () {
        this.toIndex(0);
    };
    Kifu.prototype.next = function () {
        this.toIndex(this.index + 1);
    };
    Kifu.prototype.back = function () {
        this.toIndex(this.index - 1);
    };
    Kifu.prototype.toEnd = function () {
        this.toIndex(this.boards.length - 1);
    };
    Kifu.prototype.toIndex = function (index) {
        if (index < 0)
            index = 0;
        if (index >= this.boards.length)
            index = this.boards.length - 1;
        if (this.index != index) {
            this.index = index;
            this.boards[index];
            this.refresh(this.index, this.boards[this.index]);
            if (this.index == this.boards.length - 1) {
                this.activateUpdateTimer();
            }
            else {
                this.stopUpdateTimer();
            }
        }
    };
    Kifu.prototype.stopUpdateTimer = function () {
        this.continueUpdateTimer = false;
    };
    Kifu.prototype.activateUpdateTimer = function () {
        var _this = this;
        this.continueUpdateTimer = true;
        var updateTimer = function () {
            if (!_this.continueUpdateTimer)
                return updateTimer = null;
            if (_this.index != _this.boards.length - 1)
                return updateTimer = null;
            if (_this.boards[_this.boards.length - 1].endGame)
                return updateTimer = null;
            if (_this.state != 'watch')
                return updateTimer = null;
            _this.notify('game-update-info', _this.getPlayerInfo());
            window.setTimeout(updateTimer, 500);
        };
        updateTimer();
    };
    return Kifu;
})(Artifact.Artifact);
exports.Kifu = Kifu;

},{"../potaore/artifact":1}],5:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var base = require('./Base');
var BoardCanvas = (function (_super) {
    __extends(BoardCanvas, _super);
    function BoardCanvas(canvas) {
        var _this = this;
        _super.call(this, 'board-canvas');
        this.eventEffective = true;
        this.canvas = canvas;
        this.canvas.get(0).width = 640;
        this.canvas.get(0).height = 640;
        canvas.on('mousemove', function (e) {
            var rect = e.target.getBoundingClientRect();
            _this.mouseX = e.clientX - rect.left;
            _this.mouseY = e.clientY - rect.top;
        });
        canvas.on('mousedown', function (e) {
            if (_this.eventEffective) {
                var pos = base.util.posFlipX({ x: Math.floor((_this.mouseX - 5) / 70), y: Math.floor((_this.mouseY - 5) / 70) });
                _this.notify('user-mousedown-on-board', pos);
            }
            else {
                if (_this.mouseX < 320) {
                    _this.notify('board-kifu-back');
                }
                else {
                    _this.notify('board-kifu-next');
                }
            }
        });
    }
    BoardCanvas.prototype.init = function () {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
        this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
        this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    };
    BoardCanvas.prototype.putKoma = function (image, physicalPosition) {
        this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
    };
    BoardCanvas.prototype.checkCell = function (position) {
        this.ctx.fillStyle = 'rgba(256, 150, 150, 0.5)';
        this.ctx.fillRect(5 + position.x * 70, 5 + position.y * 70, 70, 70);
    };
    BoardCanvas.prototype.noticeOute = function () {
        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = 'rgba(256, 80, 80, 0.9)';
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0.7)';
        this.ctx.font = 'bold 48px "Arial"';
        this.ctx.textAlign = 'center';
        this.ctx.strokeText('王手！', this.canvas.get(0).width / 2, this.canvas.get(0).height / 2);
    };
    return BoardCanvas;
})(Artifact.Artifact);
exports.BoardCanvas = BoardCanvas;
var KomadaiCanvas = (function (_super) {
    __extends(KomadaiCanvas, _super);
    function KomadaiCanvas(canvas, num, imageManager) {
        var _this = this;
        _super.call(this, 'komadai');
        this.rects = [];
        this.eventEffective = false;
        this.num = num;
        this.canvas = canvas;
        this.imageManager = imageManager;
        this.canvas.get(0).width = 310;
        this.canvas.get(0).height = 64;
        canvas.on('mousemove', function (e) {
            if (!_this.eventEffective)
                return;
            var rect = e.target.getBoundingClientRect();
            _this.mouseX = e.clientX - rect.left;
            _this.mouseY = e.clientY - rect.top;
        });
        canvas.on('mousedown', function (e) {
            if (!_this.eventEffective)
                return;
            var hittedRect = _this.rects.filter(function (rect) { return rect.x < _this.mouseX && _this.mouseX < rect.x + rect.width && rect.y < _this.mouseY && _this.mouseY < rect.y + rect.height; });
            if (hittedRect.length > 0) {
                _this.notify('user-mousedown-on-komadai', hittedRect[0]);
            }
        });
    }
    KomadaiCanvas.prototype.init = function () {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
        this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
        this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    };
    KomadaiCanvas.prototype.drawKoma = function (komalist, flip) {
        var _this = this;
        this.init();
        var tmp = '';
        var list = null;
        var komaTypeList = [];
        komalist.forEach(function (koma) {
            if (tmp != koma.komaType) {
                if (list != null)
                    komaTypeList.push(list);
                list = [koma];
            }
            else {
                list.push(koma);
            }
            tmp = koma.komaType;
        });
        if (list != null)
            komaTypeList.push(list);
        var typeCount = 0;
        var ctx = this.ctx;
        this.rects = [];
        var rects = this.rects;
        komaTypeList.forEach(function (list) {
            var x = 10 + typeCount * 42;
            var y = 10;
            ctx.drawImage(_this.imageManager.getKomaImage(list[0], flip), x, y, 45, 45);
            var rect = { koma: list[0], x: x, width: 42, y: y, height: 45 };
            rects.push(rect);
            if (_this.emphasizeKoma) {
                if (_this.emphasizeKoma.komaType == list[0].komaType) {
                    _this.emphasizeRect(rect);
                    _this.emphasizeKoma = null;
                }
            }
            if (list.length > 1) {
                _this.circle(25 + typeCount * 42, 10, 7);
                _this.ctx.fillStyle = 'rgba(255, 255, 255, 0.99)';
                _this.ctx.font = 'bold 15px "Arial"';
                _this.ctx.textAlign = 'center';
                _this.ctx.fillText(list.length, 25 + typeCount * 42, 15);
            }
            typeCount++;
        });
    };
    KomadaiCanvas.prototype.circle = function (x, y, r) {
        var ctx = this.canvas.get(0).getContext('2d');
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#0275d8';
        ctx.fill();
        ctx.lineWidth = 5;
        ctx.strokeStyle = '#0275d8';
        ctx.stroke();
    };
    KomadaiCanvas.prototype.checkRect = function (rect) {
        this.ctx.lineWidth = 5;
        this.ctx.strokeStyle = 'rgba(256, 150, 150, 0.5)';
        this.ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
    };
    KomadaiCanvas.prototype.emphasizeRect = function (rect) {
        this.ctx.lineWidth = 5;
        this.ctx.font = 'bold 15px "Arial"';
        this.ctx.strokeStyle = 'rgba(0, 255, 255, 0.8)';
        this.ctx.strokeRect(rect.x + 2, rect.y, rect.width, rect.height);
    };
    KomadaiCanvas.prototype.emphasize = function (koma) {
        this.emphasizeKoma = koma;
    };
    return KomadaiCanvas;
})(Artifact.Artifact);
exports.KomadaiCanvas = KomadaiCanvas;
var BoardGraphicManager = (function (_super) {
    __extends(BoardGraphicManager, _super);
    function BoardGraphicManager(board, komadai1, komadai2, imageManager, domFinder) {
        _super.call(this, 'BoardGraphicManager');
        this.params = { flip: false, oute: false, blind: 0 };
        this.memo = { game: null };
        this.playerInfoMemo = null;
        this.gameInfoMemo = null;
        this.moveToPositions = [];
        this.putToPositions = [];
        this.boardCanvas = new BoardCanvas(board);
        this.komadai1Canvas = new KomadaiCanvas(komadai1, 1, imageManager);
        this.komadai2Canvas = new KomadaiCanvas(komadai2, 2, imageManager);
        this.imageManager = imageManager;
        this.domFinder = domFinder;
        this.addEvents();
        this.listen(this.boardCanvas)
            .listen(this.komadai1Canvas)
            .listen(this.komadai2Canvas);
    }
    BoardGraphicManager.prototype.addEvents = function () {
        var _this = this;
        this.on('game-redraw-board', function (eventName, sender, options) { return _this.redrawKoma(options); });
        this.on('game-board-initialize-table-state', function () { return _this.initializeTableState(); });
        this.on('game-board-set-invated-position', function (eventName, sender, invatedPosition) {
            _this.invatedPosition = invatedPosition;
        });
        //graphicApi.blindKoma
        this.on('game-blind-koma', function (eventName, sender, arg) {
            _this.params.blind = arg.blind;
            _this.flipBoard(arg.flip);
            _this.resetPlayerInfo();
            _this.resetGameInfo();
        });
        this.on('game-board-init', function () { return _this.init(); });
        this.on('game-board-flip', function (eventName, sender, flip) { return _this.flipBoard(flip); });
        this.on('game-notice-oute', function (eventName, sender, command) { return _this.params.oute = command.oute; });
        this.on('receive-game-start', function (eventName, sender, arg) {
            _this.invatedPosition = null;
            _this.boardCanvas.eventEffective = true;
            _this.komadai1Canvas.eventEffective = true;
            _this.komadai2Canvas.eventEffective = true;
            _this.setPlayerInfo(arg);
            _this.setTurn(0);
        });
        this.on('receive-kifu-replay', function (eventName, sender, arg) {
            _this.boardCanvas.eventEffective = false;
            _this.komadai1Canvas.eventEffective = false;
            _this.komadai2Canvas.eventEffective = false;
            _this.setPlayerInfo(arg.kifu);
        });
        this.on('receive-game-end', function (eventName, sender, arg) {
            _this.boardCanvas.eventEffective = false;
            _this.komadai1Canvas.eventEffective = false;
            _this.komadai2Canvas.eventEffective = false;
            _this.eraseTurnEmphasization();
        });
        this.on('game-set-turn', function (eventName, sender, turn) {
            _this.setTurn(turn);
        });
        this.on('game-update-info', function (eventName, sender, data) {
            _this.updateGameInfo(data);
        });
        this.on('game-get-koma', function (eventName, sender, command) {
            if (command.playerNumber == 1) {
                var komadai1 = _this.params.flip ? _this.komadai2Canvas : _this.komadai1Canvas;
                komadai1.emphasize(command.koma);
            }
            else {
                var komadai2 = _this.params.flip ? _this.komadai1Canvas : _this.komadai2Canvas;
                komadai2.emphasize(command.koma);
            }
        });
        this.on('user-mousedown-on-board', function (eventName, sender, pos) {
            pos = base.util.posFlip(pos, _this.params.flip);
            var moveToPos = base.util.find(_this.moveToPositions, function (p) { return base.util.posEq(pos, p); });
            if (moveToPos) {
                _this.moveKoma(moveToPos);
                return;
            }
            var putToPos = base.util.find(_this.putToPositions, function (p) { return base.util.posEq(pos, p); });
            if (putToPos) {
                _this.putKoma(putToPos);
                return;
            }
            _this.showMovableCell(pos);
        });
        this.on('user-mousedown-on-komadai', function (eventName, sender, rect) {
            _this.showPutableCell(rect);
        });
        this.on('board-kifu-next', function () { return _this.notify('kifu-next'); });
        this.on('board-kifu-back', function () { return _this.notify('kifu-back'); });
    };
    BoardGraphicManager.prototype.init = function () {
        this.params = { flip: false, oute: false, blind: 0 };
        this.memo = { game: null };
        this.flipBoard(false);
    };
    BoardGraphicManager.prototype.redrawKoma = function (arg) {
        this.memo.game = arg;
        this.initializeTableState();
        this.clearKoma();
        this.drawKoma(arg);
    };
    BoardGraphicManager.prototype.reredrawKoma = function () {
        if (this.memo.game) {
            this.redrawKoma(this.memo.game);
        }
    };
    BoardGraphicManager.prototype.drawKoma = function (arg) {
        var _this = this;
        var blind = arg.board.endGame ? 0 : this.params.blind;
        if (this.invatedPosition) {
            var cancel = false;
            if (blind) {
                var myTurn = (arg.board.turn % 2 + 1) == this.params.blind;
                if (!arg.board.events || !myTurn && !arg.board.events.some(function (ev) { return ev.type == 'get'; })) {
                    cancel = true;
                }
            }
            if (!cancel) {
                this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(this.invatedPosition, this.params.flip)));
            }
        }
        var komadai1 = this.params.flip ? this.komadai2Canvas : this.komadai1Canvas;
        var komadai2 = this.params.flip ? this.komadai1Canvas : this.komadai2Canvas;
        if (blind != 1) {
            komadai1.drawKoma(arg.board.komaDai[0], this.params.flip);
        }
        if (blind != 2) {
            komadai2.drawKoma(arg.board.komaDai[1], this.params.flip);
        }
        base.util.allPos(function (x, y) {
            var koma = arg.board.getKoma({ x: x, y: y });
            if (koma && koma.playerNumber != blind) {
                _this.boardCanvas.putKoma(_this.imageManager.getKomaImage(koma, _this.params.flip), base.util.posFlipX(base.util.posFlip({ x: x, y: y }, _this.params.flip)));
            }
        });
        if (this.params.oute) {
            this.boardCanvas.noticeOute();
        }
    };
    BoardGraphicManager.prototype.showMovableCell = function (fromPos) {
        var _this = this;
        //gameApi.getGame
        this.notify('get-game', null, function (result) {
            var game = result.options;
            var koma = game.board.getKoma(fromPos);
            if (!koma || koma.playerNumber == (game.turn % 2 + 1) || game.state != 'playing')
                return;
            _this.initializeTableState();
            _this.fromPosition = fromPos;
            var positions = base.moveConvertor.getMovablePos(koma.playerNumber, koma.komaType, fromPos, game.board);
            _this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(fromPos, _this.params.flip)));
            positions.forEach(function (pos) { return _this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(pos, _this.params.flip))); });
            _this.moveToPositions = positions;
            _this.drawKoma(game);
        });
    };
    BoardGraphicManager.prototype.moveKoma = function (pos) {
        var _this = this;
        this.notify('get-game', null, function (result) {
            var game = result.options;
            var fromPos = base.util.posDup(_this.fromPosition);
            var toPos = base.util.posDup(pos);
            var fromKoma = game.board.getKoma(fromPos);
            var moveInfo = {
                from: {
                    position: fromPos,
                    komaType: fromKoma.komaType
                },
                to: {
                    position: toPos,
                    komaType: fromKoma.komaType
                }
            };
            var nari = base.moveConvertor.getNari(fromKoma.playerNumber, fromKoma.komaType, moveInfo.from.position, moveInfo.to.position);
            if (nari == 'force') {
                moveInfo.to.komaType = base.komaTypes[moveInfo.to.komaType].nariId;
            }
            else if (nari == 'possible') {
                _this.notify('board-show-nari-modal', {
                    nari: function () {
                        moveInfo.to.komaType = base.komaTypes[moveInfo.to.komaType].nariId;
                        _this.notify('user-game-try-move-koma', moveInfo);
                    },
                    notNari: function () {
                        _this.notify('user-game-try-move-koma', moveInfo);
                    },
                    cancel: function () {
                    }
                });
                return;
            }
            _this.notify('user-game-try-move-koma', moveInfo);
        });
    };
    BoardGraphicManager.prototype.showPutableCell = function (rect) {
        var _this = this;
        var playerNumber = rect.koma.playerNumber;
        var komaType = rect.koma.komaType;
        this.notify('get-game', null, function (result) {
            var game = result.options;
            if (playerNumber == (game.turn % 2 + 1) || game.state != 'playing')
                return;
            _this.initializeTableState();
            var positions = base.moveConvertor.getPutableCell(playerNumber, komaType, game.board);
            if (komaType == 'hu') {
                var columns = game.board.getExistsHuColumn(playerNumber);
                positions = positions.filter(function (p) { return columns[p.x] ? false : true; });
            }
            positions.forEach(function (pos) { return _this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(pos, _this.params.flip))); });
            _this.putToPositions = positions;
            _this.drawKoma(game);
            _this.putKomaType = komaType;
            _this.komadai1Canvas.checkRect(rect);
        });
    };
    BoardGraphicManager.prototype.putKoma = function (pos) {
        var moveInfo = {
            to: {
                position: pos,
                komaType: this.putKomaType
            }
        };
        //gameApi.tryPutKoma
        this.notify('user-game-try-put-koma', moveInfo);
    };
    BoardGraphicManager.prototype.clearKoma = function () {
        this.boardCanvas.init();
        this.komadai1Canvas.init();
        this.komadai2Canvas.init();
    };
    BoardGraphicManager.prototype.initializeTableState = function () {
        this.moveToPositions = [];
        this.fromPosition = null;
        this.putToPositions = [];
        this.putKomaType = '';
        this.boardCanvas.init();
    };
    BoardGraphicManager.prototype.flipBoard = function (flip) {
        this.params.flip = flip;
        this.reredrawKoma();
        if (flip) {
            this.domFinder.getFlipItem().show();
            this.domFinder.getNotFlipItem().hide();
        }
        else {
            this.domFinder.getFlipItem().hide();
            this.domFinder.getNotFlipItem().show();
        }
    };
    BoardGraphicManager.prototype.setPlayerInfo = function (command) {
        this.playerInfoMemo = command.account;
        this.domFinder.getPlayerImageDiv(1, this.params.flip).empty();
        this.domFinder.getPlayerImageDiv(2, this.params.flip).empty();
        this.domFinder.getPlayerNameDiv(1, this.params.flip).empty();
        this.domFinder.getPlayerNameDiv(2, this.params.flip).empty();
        this.domFinder.getPlayerImageDiv(1, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player1.profile_url, this.playerInfoMemo.player1.character));
        this.domFinder.getPlayerNameDiv(1, this.params.flip).append(this.playerInfoMemo.player1.name);
        this.domFinder.getPlayerImageDiv(2, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player2.profile_url, this.playerInfoMemo.player2.character));
        this.domFinder.getPlayerNameDiv(2, this.params.flip).append(this.playerInfoMemo.player2.name);
    };
    BoardGraphicManager.prototype.resetPlayerInfo = function () {
        if (this.playerInfoMemo)
            this.setPlayerInfo({ account: this.playerInfoMemo });
    };
    BoardGraphicManager.prototype.setTurn = function (turn) {
        this.domFinder.getGameTurnDiv().empty();
        if (turn) {
            this.domFinder.getGameTurnDiv().append(turn + '手');
        }
        else {
            this.domFinder.getGameTurnDiv().append('対局開始');
        }
        var playerTurn = turn % 2;
        this.domFinder.getPlayerNameDiv(1 + playerTurn, this.params.flip).addClass('label-success');
        this.domFinder.getPlayerNameDiv(playerTurn, this.params.flip).removeClass('label-success');
    };
    BoardGraphicManager.prototype.eraseTurnEmphasization = function () {
        this.domFinder.getPlayerNameDiv(1, this.params.flip).removeClass('label-success');
        this.domFinder.getPlayerNameDiv(2, this.params.flip).removeClass('label-success');
    };
    BoardGraphicManager.prototype.updateGameInfo = function (command) {
        var emphasize1 = false;
        var emphasize2 = false;
        if (this.gameInfoMemo) {
            emphasize1 = command.player1.life < this.gameInfoMemo.player1.life;
            emphasize2 = command.player2.life < this.gameInfoMemo.player2.life;
        }
        this.gameInfoMemo = {
            player1: {
                time: command.player1.time,
                life: command.player1.life
            },
            player2: {
                time: command.player2.time,
                life: command.player2.life
            }
        };
        this.domFinder.getPlayerTimeDiv(1, this.params.flip).empty();
        this.domFinder.getPlayerTimeDiv(2, this.params.flip).empty();
        this.domFinder.getPlayerLifeLabel(1, this.params.flip).empty();
        this.domFinder.getPlayerLifeLabel(2, this.params.flip).empty();
        this.domFinder.getPlayerTimeDiv(1, this.params.flip).append(this.computeDuration(command.player1.time));
        this.domFinder.getPlayerTimeDiv(2, this.params.flip).append(this.computeDuration(command.player2.time));
        this.domFinder.getPlayerLifeLabel(1, this.params.flip).append(command.player1.life);
        this.domFinder.getPlayerLifeLabel(2, this.params.flip).append(command.player2.life);
        if (emphasize1) {
            console.log('emphasize1');
            this.emphasizePlayerLife(1);
        }
        if (emphasize2) {
            console.log('emphasize2');
            this.emphasizePlayerLife(2);
        }
    };
    BoardGraphicManager.prototype.emphasizePlayerLife = function (playerNumber) {
        var _this = this;
        this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-primary', 150);
        this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-danger', 150);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 150);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-primary', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-danger', 150);
        }, 300);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 450);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-primary', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-danger', 150);
        }, 600);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 750);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-primary', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-danger', 150);
        }, 900);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 1050);
    };
    BoardGraphicManager.prototype.resetGameInfo = function () {
        if (this.gameInfoMemo)
            this.updateGameInfo(this.gameInfoMemo);
    };
    BoardGraphicManager.prototype.computeDuration = function (ms) {
        if (ms < 0)
            ms = 0;
        var h = parseInt(new String(Math.floor(ms / 3600000) + 100).substring(1));
        var m = parseInt(new String(Math.floor((ms - h * 3600000) / 60000) + 100).substring(1));
        var s = new String(Math.floor((ms - h * 3600000 - m * 60000) / 1000) + 100).substring(1);
        return m + ':' + s;
    };
    return BoardGraphicManager;
})(Artifact.Artifact);
exports.BoardGraphicManager = BoardGraphicManager;

},{"../potaore/artifact":1,"./Base":4}],6:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var img = require('./ImageManager');
var sound = require('./SoundManager');
var usr = require('./User');
var base = require('./Base');
var board = require('./BoardGraphic');
var _window = window;
var playSound;
var domFinder = {
    getPlayerNameDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerImageDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getGameTurnDiv: function () {
        return $('#game-turn-label');
    },
    getPlayerTimeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeLabel: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getFlipItem: function (playerNumber, flip) {
        return $('.flip-item');
    },
    getNotFlipItem: function (playerNumber, flip) {
        return $('.not-flip-item');
    }
};
$(window).load(function () {
    var imageManager = new img.ImageManager();
    var user = new usr.User();
    var view = new View(imageManager, user);
    view.listen(user);
    var gameInfoView = new GameInfoView();
    gameInfoView.connect(view).listen(user);
    var gameBoard = new board.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager, domFinder);
    gameBoard.listen(user).connect(view).connect(gameInfoView);
    var soundManager = new sound.SoundManager();
    soundManager
        .listen(user)
        .listen(view)
        .listen(gameInfoView)
        .listen(gameBoard);
    app = user;
    {
        var logger = new Artifact.Listner({ info: function (data) { return console.log(data); } }, false);
        logger.listen(soundManager);
        logger.listen(user);
        logger.listen(view);
    }
    imageManager.load(function () { return view.getKifu(); });
});
var View = (function (_super) {
    __extends(View, _super);
    function View(imageManager, user) {
        _super.call(this, 'view');
        this.imageManager = imageManager;
        this.user = user;
    }
    View.prototype.getKifu = function () {
        var _this = this;
        var arg = {};
        var pair = location.search.substring(1).split('&');
        for (var i = 0; pair[i]; i++) {
            var kv = pair[i].split('=');
            arg[kv[0]] = kv[1];
        }
        $.ajax({
            url: 'kifu/find?gameId=' + arg.gameId
        }).done(function (data) {
            if (data.error) {
                _this.showModal(['棋譜が見つかりませんでした'], [], { ok: false, cancel: false, notApplyToGameMessageArea: true });
            }
            else {
                _this.notify('receive-kifu-replay', { kifu: data });
                if (arg.viewpoint) {
                    _this.notify('change-viewpoint', arg.viewpoint);
                }
                if (arg.index) {
                    _this.notify('kifu-to', arg.index);
                }
            }
            console.log(data);
        }).fail(function (data) {
            _this.showModal(['棋譜が見つかりませんでした'], [], { ok: false, cancel: false, notApplyToGameMessageArea: true });
        });
    };
    View.prototype.showModal = function (texts, boxes, option, afterOk, afterCancel) {
        if (afterOk === void 0) { afterOk = function () { }; }
        if (afterCancel === void 0) { afterCancel = function () { }; }
        this.afterModalOk = afterOk;
        this.afterModalCancel = afterCancel;
        if (option.ok) {
            $("#modalConfirmButton").show();
        }
        else {
            $("#modalConfirmButton").hide();
        }
        if (option.cancel) {
            $("#modalCancelButton").show();
        }
        else {
            $("#modalCancelButton").hide();
        }
        if (!option.notApplyToGameMessageArea) {
            $("#game-message-area").empty();
        }
        $("#modal-message").empty();
        $("#modal-box").empty();
        $("#modal-window").show();
        $("#modal-overlay").show();
        $("#modal-window").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
        $("#modal-window").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
        $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
        texts.forEach(function (text) {
            if (!option.notApplyToGameMessageArea) {
                $("#game-message-area").append($('<span>').text(text));
            }
            $("#modal-message").append($('<div>').text(text));
        });
        if (boxes) {
            boxes.forEach(function (text) {
                if (!option.notApplyToGameMessageArea) {
                    $("#game-message-area").append($('<span>').text(text));
                }
                if (option.boxReadonly) {
                    $("#modal-box").append($('<input type="text" class="modal-textbox" readonly>').val(text));
                }
                else {
                    $("#modal-box").append($('<input type="text" class="modal-textbox">').val(text));
                }
            });
        }
    };
    return View;
})(Artifact.Artifact);
var GameInfoView = (function (_super) {
    __extends(GameInfoView, _super);
    function GameInfoView() {
        var _this = this;
        _super.call(this, 'game-info-view');
        this.playerInfoMemo = null;
        this.on('receive-game-message', function (eventName, sender, message) { return _this.showGameMessage(message); });
        this.on('receive-kifu-replay', function (eventName, sender, data) { return _this.replayKifu(data.kifu); });
        this.on('kifu-add', function (eventName, sender, data) { return _this.addKifu(data.te); });
        this.on('kifu-to-start', function (eventName, sender, options) { return _this.kifuToStart(); });
        this.on('kifu-back', function (eventName, sender, options) { return _this.kifuBack(); });
        this.on('kifu-next', function (eventName, sender, options) { return _this.kifuNext(); });
        this.on('kifu-to-end', function (eventName, sender, options) { return _this.kifuToEnd(); });
        this.on('kifu-to', function (eventName, sender, index) { return _this.kifu.toIndex(index); });
        this.on('change-viewpoint', function (eventName, sender, viewPointCode) {
            if (viewPointCode == '1') {
                $('#viewPointSelectBox').val('先手視点');
            }
            else if (viewPointCode == '2') {
                $('#viewPointSelectBox').val('後手視点');
            }
            else if (viewPointCode == '3') {
                $('#viewPointSelectBox').val('先手視点（ブラインド）');
            }
            else if (viewPointCode == '4') {
                $('#viewPointSelectBox').val('後手視点（ブラインド）');
            }
            _this.notify('game-blind-koma', _this.getViewPoint());
        });
        this.on('user-game-change-viewpoint', function () {
            _this.notify('game-blind-koma', _this.getViewPoint());
        });
    }
    GameInfoView.prototype.showGameMessage = function (message) {
        this.showGameMessageConsole(message);
    };
    GameInfoView.prototype.showGameMessageConsole = function (message) {
        $("#game-message-area").empty();
        message.texts.forEach(function (text) { return $("#game-message-area").append($('<li>').text(text + '.　')); });
        message.texts.forEach(function (text) { return $("#game-history").append($('<li>').text(text + '.　')); });
        $('#game-history-div').scrollTop($("#game-history-area").height());
    };
    GameInfoView.prototype.showEvents = function (board, blind) {
        var texts = [];
        var myTurn = (board.turn % 2 + 1) == blind;
        var foul = false;
        var end = false;
        if (blind != 0 && board.events) {
            board.events.forEach(function (ev) {
                if (ev.type == "get") {
                    if (myTurn) {
                        texts.push(base.getKomaName(ev.komaType) + "を取りました");
                    }
                    else {
                        texts.push(base.komaTypes[ev.komaType].name + "を取られました");
                    }
                }
                else if (ev.type == "oute") {
                    if (myTurn) {
                        texts.push("王手をかけました");
                    }
                    else {
                        texts.push("王手をかけられました");
                    }
                }
                else if (ev.type == "foul") {
                    foul = true;
                    if (myTurn) {
                        texts.push("反則です");
                    }
                    else {
                        texts.push("相手が反則手を指しました");
                    }
                }
                else if (ev.type == "endGame") {
                    end = true;
                    if (ev.winPlayerNumber == 3 - blind) {
                        texts.push("あなたの勝ちです");
                    }
                    else {
                        texts.push("あなたの負けです");
                    }
                }
            });
            if (!end) {
                if ((myTurn && !foul) || (!myTurn && foul)) {
                    texts.push("相手の手番です");
                }
                else {
                    texts.push("あなたの手番です");
                }
            }
        }
        this.showGameMessage({ type: 'plain', texts: texts });
    };
    GameInfoView.prototype.getViewPoint = function () {
        var value = $('#viewPointSelectBox option:selected').val();
        if (value == "先手視点") {
            return { "flip": false, "blind": 0 };
        }
        else if (value == "後手視点") {
            return { "flip": true, "blind": 0 };
        }
        else if (value == "先手視点（ブラインド）") {
            return { "flip": false, "blind": 2 };
        }
        else if (value == "後手視点（ブラインド）") {
            return { "flip": true, "blind": 1 };
        }
    };
    GameInfoView.prototype.replayKifu = function (arg) {
        var _this = this;
        this.notify('game-board-init');
        $('#kifuSelectBox').empty();
        $("#kifuInfoDiv").show();
        $("#kifuReplayDiv").show();
        $('.game-playing').hide();
        $('.game-watching').show();
        var refresh = function (index, board) {
            _this.notify('game-update-info', { player1: board.player1, player2: board.player2 });
            _this.notify('game-set-turn', board.turn ? board.turn : 0);
            $('#kifuSelectBox').val(index);
            var kifuLength = $("#kifuSelectBox").children().length - 1;
            _this.notify('game-board-set-invated-position', board.emphasisPosition);
            _this.notify('game-redraw-board', { board: board, turn: 1, state: 'replay' });
            $('#toStartButton').prop('disabled', index == 0);
            $('#backButton').prop('disabled', index == 0);
            $('#nextButton').prop('disabled', kifuLength == index);
            $('#toEndButton').prop('disabled', kifuLength == index);
            var viewPoint = _this.getViewPoint();
            _this.showEvents(board, viewPoint.blind);
        };
        this.disposeKifu();
        this.kifu = new base.Kifu(arg.kifu, 1, refresh, arg.playerInfo.player1, arg.playerInfo.player2);
        this.currentGameId = arg.gameId;
        this.kifu.synchronizeConnectionInfo(this).connect(this);
        this.kifu.activateUpdateTimer();
        this.notify('game-redraw-board', { board: this.kifu.getCurrentBoard(), turn: 1, state: "replay" });
        var index = 0;
        $('#kifuSelectBox').unbind("change");
        $('#kifuSelectBox').bind("change", function () {
            index = $('#kifuSelectBox option:selected').val();
            _this.kifu.toIndex(parseInt(index));
        });
        var option = $("<option value='" + index + "'>対局開始</option>");
        index++;
        $("#kifuSelectBox").append(option);
        arg.kifu.forEach(function (te) {
            if (te.info.type == "moveKoma") {
                if (!te.foul) {
                    var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
                }
                else {
                    var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
                }
                option = $("<option value='" + index + "'>" + value + "</option>");
            }
            else if (te.info.type == "endGame") {
                value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
                option = $("<option value='" + index + "'>" + value + "</option>");
            }
            index++;
            $("#kifuSelectBox").append(option);
        });
        this.kifu.toEnd();
        this.notify('game-blind-koma', this.getViewPoint());
    };
    GameInfoView.prototype.disposeKifu = function () {
        if (this.kifu) {
            this.kifu.disconnectAll();
        }
    };
    GameInfoView.prototype.addKifu = function (te) {
        this.kifu.addKifu(te);
        var i = $("#kifuSelectBox").children().length;
        if (te.info.type == "moveKoma") {
            if (!te.foul) {
                this.notify("sound-koma");
                var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
            }
            else {
                this.notify("sound-foul");
                var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
            }
            var option = $("<option value='" + i + "'>" + value + "</option>");
        }
        else if (te.info.type == "endGame") {
            value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
            var option = $("<option value=' " + i + "}'>" + value + "</option>");
        }
        $("#kifuSelectBox").append(option);
    };
    GameInfoView.prototype.kifuToStart = function () {
        this.kifu.toStart();
    };
    GameInfoView.prototype.kifuBack = function () {
        this.kifu.back();
    };
    GameInfoView.prototype.kifuNext = function () {
        this.kifu.next();
    };
    GameInfoView.prototype.kifuToEnd = function () {
        this.kifu.toEnd();
    };
    return GameInfoView;
})(Artifact.Artifact);

},{"../potaore/artifact":1,"./Base":4,"./BoardGraphic":5,"./ImageManager":7,"./SoundManager":8,"./User":9}],7:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var Prom = (function () {
    function Prom() {
    }
    Prom.prototype.next = function (predicate) {
        this.predicate = predicate;
        this.nextProm = new Prom();
        this.nextProm.predicate = predicate();
        return this.nextProm;
    };
    Prom.prototype.exec = function (cont) {
        this.predicate();
    };
    return Prom;
})();
var ImageManager = (function () {
    function ImageManager() {
        this.komaImages = {
            'koma1': {},
            'koma2': {}
        };
        /*
        this.komaImages = {
          'koma1': {
            "ou": ImageManager.createKomaImage("sgl01.png"),
            "hi": ImageManager.createKomaImage("sgl02.png"),
            "ka": ImageManager.createKomaImage("sgl03.png"),
            "ki": ImageManager.createKomaImage("sgl04.png"),
            "gi": ImageManager.createKomaImage("sgl05.png"),
            "ke": ImageManager.createKomaImage("sgl06.png"),
            "ky": ImageManager.createKomaImage("sgl07.png"),
            "hu": ImageManager.createKomaImage("sgl08.png"),
            "ry": ImageManager.createKomaImage("sgl12.png"),
            "um": ImageManager.createKomaImage("sgl13.png"),
            "ng": ImageManager.createKomaImage("sgl15.png"),
            "nk": ImageManager.createKomaImage("sgl16.png"),
            "ny": ImageManager.createKomaImage("sgl17.png"),
            "to": ImageManager.createKomaImage("sgl18.png")
          },
          'koma2': {
            "ou": ImageManager.createKomaImage("sgl31.png"),
            "hi": ImageManager.createKomaImage("sgl32.png"),
            "ka": ImageManager.createKomaImage("sgl33.png"),
            "ki": ImageManager.createKomaImage("sgl34.png"),
            "gi": ImageManager.createKomaImage("sgl35.png"),
            "ke": ImageManager.createKomaImage("sgl36.png"),
            "ky": ImageManager.createKomaImage("sgl37.png"),
            "hu": ImageManager.createKomaImage("sgl38.png"),
            "ry": ImageManager.createKomaImage("sgl42.png"),
            "um": ImageManager.createKomaImage("sgl43.png"),
            "ng": ImageManager.createKomaImage("sgl45.png"),
            "nk": ImageManager.createKomaImage("sgl46.png"),
            "ny": ImageManager.createKomaImage("sgl47.png"),
            "to": ImageManager.createKomaImage("sgl48.png")
          }
        };
        */
    }
    ImageManager.prototype.load = function (cont) {
        var _this = this;
        this.loadKoma('koma1', 'ou', 'sgl01.png')
            .next(function () { return _this.loadKoma('koma1', 'hi', 'sgl02.png')
            .next(function () { return _this.loadKoma('koma1', 'ka', 'sgl03.png')
            .next(function () { return _this.loadKoma('koma1', 'ki', 'sgl04.png')
            .next(function () { return _this.loadKoma('koma1', 'gi', 'sgl05.png')
            .next(function () { return _this.loadKoma('koma1', 'ke', 'sgl06.png')
            .next(function () { return _this.loadKoma('koma1', 'ky', 'sgl07.png')
            .next(function () { return _this.loadKoma('koma1', 'hu', 'sgl08.png')
            .next(function () { return _this.loadKoma('koma1', 'ry', 'sgl12.png')
            .next(function () { return _this.loadKoma('koma1', 'um', 'sgl13.png')
            .next(function () { return _this.loadKoma('koma1', 'ng', 'sgl15.png')
            .next(function () { return _this.loadKoma('koma1', 'nk', 'sgl16.png')
            .next(function () { return _this.loadKoma('koma1', 'ny', 'sgl17.png')
            .next(function () { return _this.loadKoma('koma1', 'to', 'sgl18.png')
            .next(function () { return _this.loadKoma('koma2', 'ou', 'sgl31.png')
            .next(function () { return _this.loadKoma('koma2', 'hi', 'sgl32.png')
            .next(function () { return _this.loadKoma('koma2', 'ka', 'sgl33.png')
            .next(function () { return _this.loadKoma('koma2', 'ki', 'sgl34.png')
            .next(function () { return _this.loadKoma('koma2', 'gi', 'sgl35.png')
            .next(function () { return _this.loadKoma('koma2', 'ke', 'sgl36.png')
            .next(function () { return _this.loadKoma('koma2', 'ky', 'sgl37.png')
            .next(function () { return _this.loadKoma('koma2', 'hu', 'sgl38.png')
            .next(function () { return _this.loadKoma('koma2', 'ry', 'sgl42.png')
            .next(function () { return _this.loadKoma('koma2', 'um', 'sgl43.png')
            .next(function () { return _this.loadKoma('koma2', 'ng', 'sgl45.png')
            .next(function () { return _this.loadKoma('koma2', 'nk', 'sgl46.png')
            .next(function () { return _this.loadKoma('koma2', 'ny', 'sgl47.png')
            .next(function () { return _this.loadKoma('koma2', 'to', 'sgl48.png')
            .next(function () { return cont(); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); }); });
    };
    ImageManager.prototype.loadKoma = function (key, name, fileName) {
        var _this = this;
        var next = null;
        var cont = {
            next: function (predicate) {
                next = predicate;
            }
        };
        var img = ImageManager.createKomaImage(fileName);
        img.onload = function () {
            _this.komaImages[key][name] = img;
            img = null;
            if (next)
                next();
        };
        return cont;
    };
    ImageManager.createKomaImage = function (name) {
        var image = new Image();
        image.src = "./images/koma/60x64/" + name;
        image.width = 60;
        image.height = 64;
        return image;
    };
    ImageManager.prototype.getKomaImage = function (koma, flip) {
        if (flip) {
            if (koma.playerNumber === 2) {
                return this.komaImages.koma1[koma.komaType];
            }
            else {
                return this.komaImages.koma2[koma.komaType];
            }
        }
        else {
            if (koma.playerNumber === 1) {
                return this.komaImages.koma1[koma.komaType];
            }
            else {
                return this.komaImages.koma2[koma.komaType];
            }
        }
    };
    ImageManager.prototype.getIconImage = function (url, character) {
        var str;
        var image = new Image();
        if (character !== void 0 && character !== null) {
            str = ("0000" + character).slice(-4);
            image.src = "./images/icon/snap" + str + ".png";
            return image;
        }
        else if (url) {
            image.src = url;
            return image;
        }
        else {
            image.src = './images/icon/noname.jpeg';
            return image;
        }
    };
    ImageManager.prototype.getRandomCharacter = function () {
        return Math.floor(Math.random() * 55);
    };
    ImageManager.prototype.getRandomNums = function (length) {
        var result = [];
        var newNum = 0;
        while (result.length < length) {
            newNum = this.getRandomCharacter();
            if (!result.some(function (item) { return item == newNum; })) {
                result.push(newNum);
            }
        }
        return result;
    };
    ImageManager.prototype.getRandomCharacters = function (num) {
        var _this = this;
        return this.getRandomNums(10).map(function (num) { return { num: num, image: _this.getIconImage(null, num) }; });
    };
    return ImageManager;
})();
exports.ImageManager = ImageManager;

},{}],8:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var _window = window;
var SoundManager = (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager() {
        _super.call(this, 'sound-manager');
        this.sounds = {};
        this.context = null;
        this.audio = null;
        this.soundsDefs = [
            { name: "koma", system: true, source: "./sound/japanese-chess-piece1.mp3" },
            { name: "foul", system: true, source: "./sound/cancel2.mp3" },
            { name: "start", system: true, source: "./sound/drum-japanese1.mp3" },
            { name: "oute", system: true, source: "./sound/decision10.mp3" },
            { name: "get", system: true, source: "./sound/cursor9.mp3" },
            { name: "拍手1", system: false, source: "./sound/clapping-hands1.mp3" },
            { name: "拍手2", system: false, source: "./sound/clapping-hands2.mp3" }
        ];
        this.loadSounds();
        this.setEvent();
    }
    SoundManager.prototype.loadSounds = function () {
        var _this = this;
        _window.AudioContext = _window.AudioContext || _window.webkitAudioContext;
        if (_window.AudioContext) {
            this.context = new AudioContext();
            this.soundsDefs.forEach(function (soundDef) {
                _this.loadSound(soundDef.source, function (buffer) { _this.sounds[soundDef.name] = buffer; });
            });
        }
        else if (Audio) {
            this.audio = new Audio();
            if (this.audio.canPlayType("audio/mp3") == 'maybe') {
                this.soundsDefs.forEach(function (soundDef) {
                    _this.sounds[soundDef.name] = new Audio(soundDef.source);
                });
            }
            else {
                alert("ご利用のブラウザは、音声出力に対応していません。");
            }
        }
        else {
            alert("ご利用のブラウザは、音声出力に対応していません。");
        }
    };
    SoundManager.prototype.loadSound = function (url, cont) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.responseType = 'arraybuffer';
        request.onload = function () {
            _this.context.decodeAudioData(request.response, function (buffer) {
                cont(buffer);
            }, function (err) {
                console.log(err);
            });
        };
        request.send();
    };
    SoundManager.prototype.playSound = function (name) {
        if (_window.AudioContext) {
            var buffer = this.sounds[name];
            if (buffer) {
                var source = this.context.createBufferSource();
                source.buffer = buffer;
                source.connect(this.context.destination);
                source.start(0);
            }
        }
        else if (Audio) {
            var _audio = this.sounds[name];
            if (_audio)
                _audio.play();
        }
        else {
        }
    };
    SoundManager.prototype.setEvent = function () {
        var _this = this;
        this.on("sound-start", function () { return _this.playSound("start"); });
        this.on("sound-koma", function () { return _this.playSound("koma"); });
        this.on("sound-oute", function () { return _this.playSound("oute"); });
        this.on("sound-get", function () { return _this.playSound("get"); });
        this.on("sound-foul", function () { return _this.playSound("foul"); });
    };
    return SoundManager;
})(Artifact.Artifact);
exports.SoundManager = SoundManager;

},{"../potaore/artifact":1}],9:[function(require,module,exports){
//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var User = (function (_super) {
    __extends(User, _super);
    function User() {
        _super.call(this, 'user');
    }
    User.prototype.startGame = function (type) {
        this.notify('user-game-start', type);
    };
    User.prototype.cancelGame = function () {
        this.notify('user-game-cancel');
    };
    User.prototype.watch = function (gameId) {
        this.notify('user-game-watch', gameId);
    };
    User.prototype.watchByGameId = function () {
        this.notify('user-watch-by-game-id');
    };
    User.prototype.changeViewPoint = function () {
        this.notify('user-game-change-viewpoint');
    };
    User.prototype.tryResign = function () {
        this.notify('user-game-try-resign');
    };
    User.prototype.resign = function () {
        this.notify('user-game-resign');
    };
    User.prototype.back = function () {
        this.notify('user-back');
    };
    User.prototype.listNext = function () {
        this.notify('user-list-next');
    };
    User.prototype.listBack = function () {
        this.notify('user-list-back');
    };
    User.prototype.exitRoom = function () {
        this.notify('user-game-exit');
    };
    User.prototype.nariModalNari = function () {
        this.notify('user-nari-modal-nari');
    };
    User.prototype.nariModalNotNari = function () {
        this.notify('user-nari-modal-not-nari');
    };
    User.prototype.nariModalCancel = function () {
        this.notify('user-nari-modal-cancel');
    };
    User.prototype.kifuToStart = function () {
        this.notify('kifu-to-start');
    };
    User.prototype.kifuNext = function () {
        this.notify('kifu-next');
    };
    User.prototype.kifuBack = function () {
        this.notify('kifu-back');
    };
    User.prototype.kifuToEnd = function () {
        this.notify('kifu-to-end');
    };
    User.prototype.showGameId = function () {
        this.notify('user-need-game-id');
    };
    User.prototype.showEmbeddedTag = function () {
        this.notify('user-need-embedded-tag');
    };
    User.prototype.modalOk = function () {
        this.notify('user-modal-ok');
    };
    User.prototype.modalCancel = function () {
        this.notify('user-modal-cancel');
    };
    User.prototype.requestConnect = function (type) {
        this.notify('user-request-connect', type);
        return false;
    };
    User.prototype.logout = function (type) {
        this.notify('user-logout');
        return false;
    };
    User.prototype.requestPlayingGames = function () {
        this.notify('user-request-playing-games');
    };
    User.prototype.requestOwnHistory = function () {
        this.notify('user-request-own-history');
    };
    User.prototype.requestOwnProfile = function () {
        this.notify('user-request-own-profile');
    };
    User.prototype.sendMessage = function (message) {
        this.notify('user-send-message');
    };
    return User;
})(Artifact.Artifact);
exports.User = User;

},{"../potaore/artifact":1}]},{},[6]);

//Copyright (c) 2015-2016 potalong_oreo All rights reserved

import Artifact = require('../potaore/artifact');
import base = require('./Base');
import wbase = require('./WebBase');
import img = require('./ImageManager');

export class BoardCanvas extends Artifact.Artifact {
  canvas;
  ctx;
  mouseX;
  mouseY;
  eventEffective: boolean = true;
  constructor(canvas) {
    super('board-canvas');
    this.canvas = canvas;
    this.canvas.get(0).width = 640;
    this.canvas.get(0).height = 640;
    canvas.on('mousemove', (e) => {
      let rect = e.target.getBoundingClientRect();
      this.mouseX = e.clientX - rect.left;
      this.mouseY = e.clientY - rect.top;
    });

    canvas.on('mousedown', (e) => {
      if (this.eventEffective) {
        let pos = base.util.posFlipX({ x: Math.floor((this.mouseX - 5) / 70), y: Math.floor((this.mouseY - 5) / 70) });
        this.notify('user-mousedown-on-board', pos);
      } else {
        if (this.mouseX < 320) {
          this.notify('board-kifu-back');
        } else {
          this.notify('board-kifu-next');
        }
      }
    });
  }

  init() {
    this.ctx = this.canvas.get(0).getContext('2d');
    this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
    this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
  }

  putKoma(image, physicalPosition) {
    this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
  }

  checkCell(position) {
    this.ctx.fillStyle = 'rgba(256, 150, 150, 0.5)';
    this.ctx.fillRect(5 + position.x * 70, 5 + position.y * 70, 70, 70);
  }

  noticeOute() {
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = 'rgba(256, 80, 80, 0.9)';
    this.ctx.fillStyle = 'rgba(255, 10, 30, 0.7)';
    this.ctx.font = 'bold 48px "Arial"';
    this.ctx.textAlign = 'center';
    this.ctx.strokeText('王手！', this.canvas.get(0).width / 2, this.canvas.get(0).height / 2);
  }
}


export class KomadaiCanvas extends Artifact.Artifact {
  num: number;
  canvas;
  ctx;
  mouseX;
  mouseY;
  rects = [];
  imageManager: img.ImageManager;
  emphasizeKoma;
  eventEffective: boolean = false;
  constructor(canvas, num: number, imageManager: img.ImageManager) {
    super('komadai');
    this.num = num;
    this.canvas = canvas;
    this.imageManager = imageManager;

    this.canvas.get(0).width = 310;
    this.canvas.get(0).height = 64;
    canvas.on('mousemove', (e) => {
      if (!this.eventEffective) return;
      let rect = e.target.getBoundingClientRect()
      this.mouseX = e.clientX - rect.left
      this.mouseY = e.clientY - rect.top
    });
    canvas.on('mousedown', (e) => {
      if (!this.eventEffective) return;
      let hittedRect = this.rects.filter(rect => rect.x < this.mouseX && this.mouseX < rect.x + rect.width && rect.y < this.mouseY && this.mouseY < rect.y + rect.height);
      if (hittedRect.length > 0) {
        this.notify('user-mousedown-on-komadai', hittedRect[0]);
      }
    });
  }

  init() {
    this.ctx = this.canvas.get(0).getContext('2d');
    this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
    this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
  }

  drawKoma(komalist, flip) {
    this.init();
    let tmp = '';
    let list = null
    let komaTypeList = []
    komalist.forEach(koma => {
      if (tmp != koma.komaType) {
        if (list != null) komaTypeList.push(list);
        list = [koma];
      } else {
        list.push(koma);
      }
      tmp = koma.komaType;
    });
    if (list != null) komaTypeList.push(list);

    let typeCount = 0;
    let ctx = this.ctx;
    this.rects = [];
    let rects = this.rects;
    komaTypeList.forEach(list => {
      let x = 10 + typeCount * 42;
      let y = 10;
      ctx.drawImage(this.imageManager.getKomaImage(list[0], flip), x, y, 45, 45);
      let rect = { koma: list[0], x: x, width: 42, y: y, height: 45 };
      rects.push(rect);

      if (this.emphasizeKoma) {
        if (this.emphasizeKoma.komaType == list[0].komaType) {
          this.emphasizeRect(rect);
          this.emphasizeKoma = null;
        }
      }

      if (list.length > 1) {
        this.circle(25 + typeCount * 42, 10, 7);
        this.ctx.fillStyle = 'rgba(255, 255, 255, 0.99)';
        this.ctx.font = 'bold 15px "Arial"';
        this.ctx.textAlign = 'center';
        this.ctx.fillText(list.length, 25 + typeCount * 42, 15);
      }

      typeCount++;
    });
  }

  circle(x, y, r) {
    let ctx = this.canvas.get(0).getContext('2d');
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI, false);
    ctx.fillStyle = '#0275d8';
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#0275d8';
    ctx.stroke();
  }

  checkRect(rect) {
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = 'rgba(256, 150, 150, 0.5)';
    this.ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
  }

  emphasizeRect(rect) {
    this.ctx.lineWidth = 5;
    this.ctx.font = 'bold 15px "Arial"';
    this.ctx.strokeStyle = 'rgba(0, 255, 255, 0.8)';
    this.ctx.strokeRect(rect.x + 2, rect.y, rect.width, rect.height);
  }

  emphasize(koma) {
    this.emphasizeKoma = koma;
  }
}

export class BoardGraphicManager extends Artifact.Artifact {
  boardCanvas: BoardCanvas;
  komadai1Canvas: KomadaiCanvas;
  komadai2Canvas: KomadaiCanvas;
  params = { flip: false, oute: false, blind: 0 };
  memo: { game: base.Game } = { game: null };
  playerInfoMemo = null;
  gameInfoMemo = null;
  invatedPosition: { x: number, y: number };
  imageManager: img.ImageManager;
  moveToPositions: any[] = [];
  putToPositions: any[] = [];
  putKomaType: any;
  fromPosition: { x: number, y: number };
  domFinder;
  constructor(board, komadai1, komadai2, imageManager: img.ImageManager, domFinder) {
    super('BoardGraphicManager');
    this.boardCanvas = new BoardCanvas(board);
    this.komadai1Canvas = new KomadaiCanvas(komadai1, 1, imageManager);
    this.komadai2Canvas = new KomadaiCanvas(komadai2, 2, imageManager);
    this.imageManager = imageManager;
    this.domFinder = domFinder;
    this.addEvents();

    this.listen(this.boardCanvas)
      .listen(this.komadai1Canvas)
      .listen(this.komadai2Canvas);
  }

  addEvents() {

    this.on('game-redraw-board', (eventName, sender, options) => this.redrawKoma(options));

    this.on('game-board-initialize-table-state', () => this.initializeTableState());

    this.on('game-board-set-invated-position', (eventName, sender, invatedPosition) => {
      this.invatedPosition = invatedPosition;
    });

    //graphicApi.blindKoma
    this.on('game-blind-koma', (eventName, sender, arg) => {
      this.params.blind = arg.blind;
      this.flipBoard(arg.flip);
      this.resetPlayerInfo();
      this.resetGameInfo();
    });


    this.on('game-board-init', () => this.init());

    this.on('game-board-flip', (eventName, sender, flip) => this.flipBoard(flip));

    this.on('game-notice-oute', (eventName, sender, command) => this.params.oute = command.oute);

    this.on('receive-game-start', (eventName, sender, arg) => {
      this.invatedPosition = null;
      this.boardCanvas.eventEffective = true;
      this.komadai1Canvas.eventEffective = true;
      this.komadai2Canvas.eventEffective = true;
      this.setPlayerInfo(arg);
      this.setTurn(0);
    });

    this.on('game-restart', (eventName, sender, arg) => {
      this.invatedPosition = null;
      this.boardCanvas.eventEffective = true;
      this.komadai1Canvas.eventEffective = true;
      this.komadai2Canvas.eventEffective = true;
      this.setPlayerInfo(arg);
      this.updateGameInfo(arg.playerInfo);
      this.setTurn(arg.turn - 1);
      if (arg.state && arg.state.oute) {
        this.params.oute = true;
      }
      if (arg.state && arg.state.getKoma) {
        this.invatedPosition = arg.state.emphasisPosition;
      }
    });

    this.on('receive-kifu-replay', (eventName, sender, arg) => {
      this.boardCanvas.eventEffective = false;
      this.komadai1Canvas.eventEffective = false;
      this.komadai2Canvas.eventEffective = false;
      this.setPlayerInfo(arg.kifu);
    });

    this.on('receive-game-end', (eventName, sender, arg) => {
      this.boardCanvas.eventEffective = false;
      this.komadai1Canvas.eventEffective = false;
      this.komadai2Canvas.eventEffective = false;
      this.eraseTurnEmphasization();
    });

    this.on('game-set-turn', (eventName, sender, turn) => {
      this.setTurn(turn);
    });

    this.on('game-update-info', (eventName, sender, data) => {
      this.updateGameInfo(data);
    });

    this.on('game-get-koma', (eventName, sender, command) => {
      if (command.playerNumber == 1) {
        let komadai1 = this.params.flip ? this.komadai2Canvas : this.komadai1Canvas;
        komadai1.emphasize(command.koma);
      } else {
        let komadai2 = this.params.flip ? this.komadai1Canvas : this.komadai2Canvas;
        komadai2.emphasize(command.koma);
      }
    });


    this.on('user-mousedown-on-board', (eventName, sender, pos) => {
      pos = base.util.posFlip(pos, this.params.flip);
      let moveToPos = base.util.find(this.moveToPositions, p => base.util.posEq(pos, p));
      if (moveToPos) {
        this.moveKoma(moveToPos);
        return;
      }
      let putToPos = base.util.find(this.putToPositions, p => base.util.posEq(pos, p));
      if (putToPos) {
        this.putKoma(putToPos);
        return;
      }
      this.showMovableCell(pos);
    });
    this.on('user-mousedown-on-komadai', (eventName, sender, rect) => {
      this.showPutableCell(rect);
    });

    this.on('board-kifu-next', () => this.notify('kifu-next'));
    this.on('board-kifu-back', () => this.notify('kifu-back'));
  }

  init() {
    this.invatedPosition = null
    this.params = { flip: false, oute: false, blind: 0 };
    this.memo = { game: null }
    this.flipBoard(false)
  }

  redrawKoma(arg) {
    this.memo.game = arg;
    this.initializeTableState();
    this.clearKoma();
    this.drawKoma(arg);
  }

  reredrawKoma() {
    if (this.memo.game) {
      this.redrawKoma(this.memo.game);
    }
  }

  drawKoma(arg) {
    let blind = arg.board.endGame ? 0 : this.params.blind;

    if (this.invatedPosition) {
      let cancel = false;
      if (blind) {
        let myTurn = (arg.board.turn % 2 + 1) == this.params.blind;
        if (!arg.board.events || !myTurn && !arg.board.events.some(ev => ev.type == 'get')) {
          cancel = true;
        }
      }
      if (!cancel) {
        this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(this.invatedPosition, this.params.flip)))
      }
    }

    let komadai1 = this.params.flip ? this.komadai2Canvas : this.komadai1Canvas;
    let komadai2 = this.params.flip ? this.komadai1Canvas : this.komadai2Canvas;
    if (blind != 1) {
      komadai1.drawKoma(arg.board.komaDai[0], this.params.flip)
    }
    if (blind != 2) {
      komadai2.drawKoma(arg.board.komaDai[1], this.params.flip)
    }

    base.util.allPos((x, y) => {
      let koma = arg.board.getKoma({ x: x, y: y })
      if (koma && koma.playerNumber != blind) {
        this.boardCanvas.putKoma(this.imageManager.getKomaImage(koma, this.params.flip),
          base.util.posFlipX(base.util.posFlip({ x: x, y: y }, this.params.flip)))
      }
    });
    if (this.params.oute) {
      this.boardCanvas.noticeOute();
    }
  }

  showMovableCell(fromPos) {
    //gameApi.getGame
    this.notify('get-game', null, result => {
      let game = result.options;
      let koma = game.board.getKoma(fromPos);
      if (!koma || koma.playerNumber == (game.turn % 2 + 1) || game.state != 'playing') return;
      this.initializeTableState();
      this.fromPosition = fromPos;
      let positions = base.moveConvertor.getMovablePos(koma.playerNumber, koma.komaType, fromPos, game.board);
      this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(fromPos, this.params.flip)));
      positions.forEach(pos => this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(pos, this.params.flip))));
      this.moveToPositions = positions;
      this.drawKoma(game);
    });
  }

  moveKoma(pos) {
    this.notify('get-game', null, result => {
      let game = result.options;
      let fromPos = base.util.posDup(this.fromPosition);
      let toPos = base.util.posDup(pos);
      let fromKoma = game.board.getKoma(fromPos);
      let moveInfo = {
        from: {
          position: fromPos,
          komaType: fromKoma.komaType
        },
        to: {
          position: toPos,
          komaType: fromKoma.komaType
        },
        receiveCommnadCount : game.receiveCommnadCount
      };

      let nari = base.moveConvertor.getNari(fromKoma.playerNumber, fromKoma.komaType, moveInfo.from.position, moveInfo.to.position);
      if (nari == 'force') {
        moveInfo.to.komaType = base.komaTypes[moveInfo.to.komaType].nariId
      } else if (nari == 'possible') {
        this.notify('board-show-nari-modal', {
          nari: () => {
            moveInfo.to.komaType = base.komaTypes[moveInfo.to.komaType].nariId;
            this.notify('user-game-try-move-koma', moveInfo);
          },
          notNari: () => {
            this.notify('user-game-try-move-koma', moveInfo);
          },
          cancel: () => {
          }
        });
        return;
      }
      this.notify('user-game-try-move-koma', moveInfo)
    });
  }

  showPutableCell(rect) {
    let playerNumber = rect.koma.playerNumber;
    let komaType = rect.koma.komaType;

    this.notify('get-game', null, result => {
      let game = result.options;
      if (playerNumber == (game.turn % 2 + 1) || game.state != 'playing') return;
      this.initializeTableState();
      let positions = base.moveConvertor.getPutableCell(playerNumber, komaType, game.board)
      if (komaType == 'hu') {
        let columns = game.board.getExistsHuColumn(playerNumber);
        positions = positions.filter(p => columns[p.x] ? false : true);
      }
      positions.forEach(pos => this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(pos, this.params.flip))))
      this.putToPositions = positions;
      this.drawKoma(game)
      this.putKomaType = komaType;
      this.komadai1Canvas.checkRect(rect);
    });
  }

  putKoma(pos) {
    let moveInfo = {
      to: {
        position: pos,
        komaType: this.putKomaType
      }
    }
    //gameApi.tryPutKoma
    this.notify('user-game-try-put-koma', moveInfo)
  }

  clearKoma() {
    this.boardCanvas.init();
    this.komadai1Canvas.init();
    this.komadai2Canvas.init();
  }

  initializeTableState() {
    this.moveToPositions = [];
    this.fromPosition = null;
    this.putToPositions = [];
    this.putKomaType = '';
    this.boardCanvas.init()
  }

  flipBoard(flip) {
    this.params.flip = flip;
    this.reredrawKoma();
    if (flip) {
      this.domFinder.getFlipItem().show();
      this.domFinder.getNotFlipItem().hide();
    } else {
      this.domFinder.getFlipItem().hide();
      this.domFinder.getNotFlipItem().show();
    }
  }

  setPlayerInfo(command) {
    this.playerInfoMemo = command.account;
    this.domFinder.getPlayerImageDiv(1, this.params.flip).empty();
    this.domFinder.getPlayerImageDiv(2, this.params.flip).empty();
    this.domFinder.getPlayerNameDiv(1, this.params.flip).empty();
    this.domFinder.getPlayerNameDiv(2, this.params.flip).empty();
    this.domFinder.getPlayerImageDiv(1, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player1.profile_url, this.playerInfoMemo.player1.character));
    this.domFinder.getPlayerNameDiv(1, this.params.flip).append(wbase.replaceToRuby(this.playerInfoMemo.player1.name));
    this.domFinder.getPlayerImageDiv(2, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player2.profile_url, this.playerInfoMemo.player2.character));
    this.domFinder.getPlayerNameDiv(2, this.params.flip).append(wbase.replaceToRuby(this.playerInfoMemo.player2.name));
  }

  resetPlayerInfo() {
    if (this.playerInfoMemo) this.setPlayerInfo({ account: this.playerInfoMemo });
  }

  setTurn(turn) {
    this.domFinder.getGameTurnDiv().empty();
    if (turn) {
      this.domFinder.getGameTurnDiv().append(turn + '手');
    } else {
      this.domFinder.getGameTurnDiv().append('対局開始');
    }

    let playerTurn = turn % 2;
    this.domFinder.getPlayerNameDiv(1 + playerTurn, this.params.flip).addClass('label-success');
    this.domFinder.getPlayerNameDiv(playerTurn, this.params.flip).removeClass('label-success');
  }

  eraseTurnEmphasization() {
    this.domFinder.getPlayerNameDiv(1, this.params.flip).removeClass('label-success');
    this.domFinder.getPlayerNameDiv(2, this.params.flip).removeClass('label-success');
  }

  updateGameInfo(command) {
    let emphasize1 = false;
    let emphasize2 = false;
    if (this.gameInfoMemo) {
      emphasize1 = command.player1.life < this.gameInfoMemo.player1.life;
      emphasize2 = command.player2.life < this.gameInfoMemo.player2.life;
    }
    this.gameInfoMemo = {
      player1: {
        time: command.player1.time,
        life: command.player1.life
      },
      player2: {
        time: command.player2.time,
        life: command.player2.life
      }
    };
    this.domFinder.getPlayerTimeDiv(1, this.params.flip).empty();
    this.domFinder.getPlayerTimeDiv(2, this.params.flip).empty();
    this.domFinder.getPlayerLifeLabel(1, this.params.flip).empty();
    this.domFinder.getPlayerLifeLabel(2, this.params.flip).empty();
    this.domFinder.getPlayerTimeDiv(1, this.params.flip).append(this.computeDuration(command.player1.time));
    this.domFinder.getPlayerTimeDiv(2, this.params.flip).append(this.computeDuration(command.player2.time));
    this.domFinder.getPlayerLifeLabel(1, this.params.flip).append(command.player1.life);
    this.domFinder.getPlayerLifeLabel(2, this.params.flip).append(command.player2.life);

    if (emphasize1) {
      console.log('emphasize1');
      this.emphasizePlayerLife(1);
    }
    if (emphasize2) {
      console.log('emphasize2');
      this.emphasizePlayerLife(2);
    }
  }

  emphasizePlayerLife(playerNumber) {
    this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-primary', 150);
    this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-danger', 150);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-danger', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-primary', 150);
    }, 150);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-primary', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-danger', 150);
    }, 300);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-danger', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-primary', 150);
    }, 450);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-primary', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-danger', 150);
    }, 600);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-danger', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-primary', 150);
    }, 750);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-primary', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-danger', 150);
    }, 900);

    setTimeout(() => {
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-danger', 150);
      this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-primary', 150);
    }, 1050);
  }

  resetGameInfo() {
    if (this.gameInfoMemo) this.updateGameInfo(this.gameInfoMemo);
  }

  computeDuration(ms) {
    if (ms < 0) ms = 0;
    let h = parseInt(new String(Math.floor(ms / 3600000) + 100).substring(1));
    let m = parseInt(new String(Math.floor((ms - h * 3600000) / 60000) + 100).substring(1));
    let s = new String(Math.floor((ms - h * 3600000 - m * 60000) / 1000) + 100).substring(1);
    return m + ':' + s;
  }
}
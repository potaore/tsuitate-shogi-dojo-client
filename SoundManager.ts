//Copyright (c) 2015-2016 potalong_oreo All rights reserved

import Artifact = require('../potaore/artifact');
let _window: any = window;

export class SoundManager extends Artifact.Artifact {
  sounds = {};
  context = null;
  audio = null;
  soundsDefs = [
    { name: "koma", system: true, source: "./sound/japanese-chess-piece1.mp3" },
    { name: "foul", system: true, source: "./sound/cancel2.mp3" },
    { name: "start", system: true, source: "./sound/drum-japanese1.mp3" },
    { name: "oute", system: true, source: "./sound/decision10.mp3" },
    { name: "get", system: true, source: "./sound/cursor9.mp3" },
    { name: "拍手1", system: false, source: "./sound/clapping-hands1.mp3" },
    { name: "拍手2", system: false, source: "./sound/clapping-hands2.mp3" }
  ];
  constructor() {
    super('sound-manager');
    this.loadSounds();
    this.setEvent();
  }

  loadSounds() {
    _window.AudioContext = _window.AudioContext || _window.webkitAudioContext;

    if (_window.AudioContext) {
      this.context = new AudioContext();
      this.soundsDefs.forEach( soundDef => {
        this.loadSound(soundDef.source, (buffer) => { this.sounds[soundDef.name] = buffer; });
      });
    } else if (Audio) {
      this.audio = new Audio();
      if (this.audio.canPlayType("audio/mp3") == 'maybe') {
        this.soundsDefs.forEach( soundDef => {
          this.sounds[soundDef.name] = new Audio(soundDef.source);
        });
      } else {
        alert("ご利用のブラウザは、音声出力に対応していません。");
      }
    } else {
      alert("ご利用のブラウザは、音声出力に対応していません。");
    }
  }

  loadSound(url, cont) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    request.onload = () => {
      this.context.decodeAudioData(request.response, function(buffer) {
        cont(buffer);
      }, function(err) {
        console.log(err);
      });
    }
    request.send();
  }

  playSound(name) {
    if (_window.AudioContext) {
      let buffer = this.sounds[name];
      if (buffer) {
        let source = this.context.createBufferSource();
        source.buffer = buffer;
        source.connect(this.context.destination);
        source.start(0);
      }
    } else if (Audio) {
      let _audio = this.sounds[name];
      if (_audio) _audio.play();
    } else {
      //音声出力未対応：何もしない
    }
  }

  setEvent() {
    this.on("sound-start", () => this.playSound("start"));
    this.on("sound-koma", () => this.playSound("koma"));
    this.on("sound-oute", () => this.playSound("oute"));
    this.on("sound-get", () => this.playSound("get"));
    this.on("sound-foul", () => this.playSound("foul"));
  }
}



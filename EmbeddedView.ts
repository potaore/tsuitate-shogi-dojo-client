//Copyright (c) 2015-2016 potalong_oreo All rights reserved

import Artifact = require('../potaore/artifact');
import img = require('./ImageManager');
import sound = require('./SoundManager');
import usr = require('./User');
import base = require('./Base');
import board = require('./BoardGraphic');
declare let app: any;
declare let $: any;

let _window: any = window;
var playSound;


let domFinder = {
  getPlayerNameDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerImageDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getGameTurnDiv: function () {
    return $('#game-turn-label');
  },
  getPlayerTimeDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerLifeLabel: function (playerNumber, flip) {
    if (flip) {
      return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerLifeDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getFlipItem: function (playerNumber, flip) {
    return $('.flip-item');
  },
  getNotFlipItem: function (playerNumber, flip) {
    return $('.not-flip-item');
  }
};


$(window).load(function () {
  let imageManager = new img.ImageManager();
  let user = new usr.User();
  let view = new View(imageManager, user);
  view.listen(user);
  let gameInfoView = new GameInfoView();
  gameInfoView.connect(view).listen(user);
  let gameBoard = new board.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager, domFinder);
  gameBoard.listen(user).connect(view).connect(gameInfoView);

  let soundManager = new sound.SoundManager();
  soundManager
    .listen(user)
    .listen(view)
    .listen(gameInfoView)
    .listen(gameBoard);


  app = user;

  {
    let logger = new Artifact.Listner({ info: (data) => console.log(data) }, false);
    logger.listen(soundManager);
    logger.listen(user);
    logger.listen(view);
  }
  imageManager.load(() => view.getKifu());
});

class View extends Artifact.Artifact {
  imageManager: img.ImageManager
  afterModalOk: { (): any };
  afterModalCancel: { (): any };
  selectedCharacter: number;
  user: usr.User;

  constructor(imageManager: img.ImageManager, user: usr.User) {
    super('view');
    this.imageManager = imageManager;
    this.user = user;
  }

  getKifu() {
    let arg: any = {};
    let pair = location.search.substring(1).split('&');
    for (var i = 0; pair[i]; i++) {
      var kv = pair[i].split('=');
      arg[kv[0]] = kv[1];
    }
    $.ajax({
      url: 'kifu/find?gameId=' + arg.gameId
    }).done((data) => {
      if (data.error) {
        this.showModal(
              ['棋譜が見つかりませんでした'],
              [],
              { ok: false, cancel: false, notApplyToGameMessageArea: true });
      } else {
        this.notify('receive-kifu-replay', { kifu: data });
        if (arg.viewpoint) {
          this.notify('change-viewpoint', arg.viewpoint);
        }
        if (arg.index) {
          this.notify('kifu-to', arg.index);
        }

      }
      console.log(data);
    }).fail((data) => {
        this.showModal(
              ['棋譜が見つかりませんでした'],
              [],
              { ok: false, cancel: false, notApplyToGameMessageArea: true });
    });
  }
  
  showModal(texts, boxes, option, afterOk = () => { }, afterCancel = () => { }) {
    this.afterModalOk = afterOk;
    this.afterModalCancel = afterCancel;
    if (option.ok) {
      $("#modalConfirmButton").show();
    } else {
      $("#modalConfirmButton").hide();
    }
    if (option.cancel) {
      $("#modalCancelButton").show();
    } else {
      $("#modalCancelButton").hide();
    }

    if (!option.notApplyToGameMessageArea) {
      $("#game-message-area").empty();
    }
    $("#modal-message").empty();
    $("#modal-box").empty();
    $("#modal-window").show();
    $("#modal-overlay").show();
    $("#modal-window").css({ "background-color": "#000000", "opacity": 0 });
    $("#modal-overlay").css({ "background-color": "#000000", "opacity": 0 });
    $("#modal-window").animate({ "background-color": "#000000", "opacity": 0.95 }, 120);
    $("#modal-overlay").animate({ "background-color": "#000000", "opacity": 0.3 }, 120);
    texts.forEach(text => {
      if (!option.notApplyToGameMessageArea) {
        $("#game-message-area").append($('<span>').text(text));
      }
      $("#modal-message").append($('<div>').text(text));
    });

    if (boxes) {
      boxes.forEach(text => {
        if (!option.notApplyToGameMessageArea) {
          $("#game-message-area").append($('<span>').text(text));
        }
        if (option.boxReadonly) {
          $("#modal-box").append($('<input type="text" class="modal-textbox" readonly>').val(text));
        } else {
          $("#modal-box").append($('<input type="text" class="modal-textbox">').val(text));
        }

      });
    }
  }
}

class GameInfoView extends Artifact.Artifact {
  afterNariModalNari: { (): any };
  afterNariModalNotNari: { (): any };
  afterNariModalCancel: { (): any };
  kifu: base.Kifu;
  game: base.Game;
  playerInfoMemo = null;
  currentGameId: string;

  constructor() {
    super('game-info-view');

    this.on('receive-game-message', (eventName, sender, message) => this.showGameMessage(message));

    this.on('receive-kifu-replay', (eventName, sender, data) => this.replayKifu(data.kifu));
    this.on('kifu-add', (eventName, sender, data) => this.addKifu(data.te));
    this.on('kifu-to-start', (eventName, sender, options) => this.kifuToStart());
    this.on('kifu-back', (eventName, sender, options) => this.kifuBack());
    this.on('kifu-next', (eventName, sender, options) => this.kifuNext());
    this.on('kifu-to-end', (eventName, sender, options) => this.kifuToEnd());
    this.on('kifu-to', (eventName, sender, index) => this.kifu.toIndex(index));

    this.on('change-viewpoint', (eventName, sender, viewPointCode) => {
      if (viewPointCode == '1') {
        $('#viewPointSelectBox').val('先手視点');
      } else if (viewPointCode == '2') {
        $('#viewPointSelectBox').val('後手視点');
      } else if (viewPointCode == '3') {
        $('#viewPointSelectBox').val('先手視点（ブラインド）');
      } else if (viewPointCode == '4') {
        $('#viewPointSelectBox').val('後手視点（ブラインド）');
      }
      this.notify('game-blind-koma', this.getViewPoint());
    });

    this.on('user-game-change-viewpoint', () => {
      this.notify('game-blind-koma', this.getViewPoint());
    });
  }

  showGameMessage(message) {
    this.showGameMessageConsole(message);
  }

  showGameMessageConsole(message) {
    $("#game-message-area").empty();
    message.texts.forEach(text => $("#game-message-area").append($('<li>').text(text + '.　')));
    message.texts.forEach(text => $("#game-history").append($('<li>').text(text + '.　')));
    $('#game-history-div').scrollTop($("#game-history-area").height());
  }

  showEvents(board, blind) {
    let texts = [];
    let myTurn = (board.turn % 2 + 1) == blind;
    let foul = false;
    let end = false;
    if (blind != 0 && board.events) {
      board.events.forEach(ev => {
        if (ev.type == "get") {
          if (myTurn) {
            texts.push(base.getKomaName(ev.komaType) + "を取りました");
          } else {
            texts.push(base.komaTypes[ev.komaType].name + "を取られました");
          }
        } else if (ev.type == "oute") {
          if (myTurn) {
            texts.push("王手をかけました");
          } else {
            texts.push("王手をかけられました");
          }
        } else if (ev.type == "foul") {
          foul = true
          if (myTurn) {
            texts.push("反則です")
          } else {
            texts.push("相手が反則手を指しました")
          }
        } else if (ev.type == "endGame") {
          end = true
          if (ev.winPlayerNumber == 3 - blind) {
            texts.push("あなたの勝ちです");
          } else {
            texts.push("あなたの負けです");
          }
        }
      });
      if (!end) {
        if ((myTurn && !foul) || (!myTurn && foul)) {
          texts.push("相手の手番です");
        } else {
          texts.push("あなたの手番です");
        }
      }
    }

    this.showGameMessage({ type: 'plain', texts: texts })
  }

  getViewPoint() {
    var value = $('#viewPointSelectBox option:selected').val();
    if (value == "先手視点") {
      return { "flip": false, "blind": 0 };
    } else if (value == "後手視点") {
      return { "flip": true, "blind": 0 };
    } else if (value == "先手視点（ブラインド）") {
      return { "flip": false, "blind": 2 };
    } else if (value == "後手視点（ブラインド）") {
      return { "flip": true, "blind": 1 };
    }
  }

  replayKifu(arg) {
    this.notify('game-board-init');
    $('#kifuSelectBox').empty();
    $("#kifuInfoDiv").show();
    $("#kifuReplayDiv").show();
    $('.game-playing').hide();
    $('.game-watching').show();
    let refresh = (index, board) => {
      this.notify('game-update-info', { player1: board.player1, player2: board.player2 });
      this.notify('game-set-turn', board.turn ? board.turn : 0);
      $('#kifuSelectBox').val(index);
      let kifuLength = $("#kifuSelectBox").children().length - 1;

      this.notify('game-board-set-invated-position', board.emphasisPosition);

      this.notify('game-redraw-board', { board: board, turn: 1, state: 'replay' })
      $('#toStartButton').prop('disabled', index == 0)
      $('#backButton').prop('disabled', index == 0)
      $('#nextButton').prop('disabled', kifuLength == index)
      $('#toEndButton').prop('disabled', kifuLength == index)
      let viewPoint = this.getViewPoint();
      this.showEvents(board, viewPoint.blind);
    }

    this.disposeKifu();
    this.kifu = new base.Kifu(arg.kifu, 1, refresh, arg.playerInfo.player1, arg.playerInfo.player2, arg.custom);
    this.currentGameId = arg.gameId;
    this.kifu.synchronizeConnectionInfo(this).connect(this);

    this.kifu.activateUpdateTimer();
    this.notify('game-redraw-board', { board: this.kifu.getCurrentBoard(), turn: 1, state: "replay" })
    let index: any = 0

    $('#kifuSelectBox').unbind("change");
    $('#kifuSelectBox').bind("change", () => {
      index = $('#kifuSelectBox option:selected').val();
      this.kifu.toIndex(parseInt(index));
    });
    let option = $("<option value='" + index + "'>対局開始</option>");
    index++;
    $("#kifuSelectBox").append(option);
    arg.kifu.forEach(te => {
      if (te.info.type == "moveKoma") {
        if (!te.foul) {
          var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
        } else {
          var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma;
        }
        option = $("<option value='" + index + "'>" + value + "</option>");
      } else if (te.info.type == "endGame") {
        value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
        option = $("<option value='" + index + "'>" + value + "</option>");
      }
      index++;
      $("#kifuSelectBox").append(option);
    });
    this.kifu.toEnd();
    this.notify('game-blind-koma', this.getViewPoint());
  }

  disposeKifu() {
    if (this.kifu) {
      this.kifu.disconnectAll();
    }
  }

  addKifu(te) {
    this.kifu.addKifu(te);
    let i = $("#kifuSelectBox").children().length;
    if (te.info.type == "moveKoma") {
      if (!te.foul) {
        this.notify("sound-koma");
        var value = te.turn + " : " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma
      } else {
        this.notify("sound-foul");
        var value = te.turn + " : [反則] " + (te.info.from.x + 1) + (te.info.from.y + 1) + (te.info.to.x + 1) + (te.info.to.y + 1) + te.info.koma
      }
      var option = $("<option value='" + i + "'>" + value + "</option>");
    } else if (te.info.type == "endGame") {
      value = "player" + te.info.winPlayerNumber + " win. (" + te.info.reason + ")";
      var option = $("<option value=' " + i + "}'>" + value + "</option>");
    }
    $("#kifuSelectBox").append(option);
  }

  kifuToStart() {
    this.kifu.toStart();
  }

  kifuBack() {
    this.kifu.back();
  }

  kifuNext() {
    this.kifu.next();
  }

  kifuToEnd() {
    this.kifu.toEnd();
  }
}



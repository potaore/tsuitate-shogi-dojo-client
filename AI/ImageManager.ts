export class ImageManager {
  komaImages;
  constructor() {
    this.komaImages = {
      'koma1': {
        "ou": ImageManager.createKomaImage("sgl01.png"),
        "hi": ImageManager.createKomaImage("sgl02.png"),
        "ka": ImageManager.createKomaImage("sgl03.png"),
        "ki": ImageManager.createKomaImage("sgl04.png"),
        "gi": ImageManager.createKomaImage("sgl05.png"),
        "ke": ImageManager.createKomaImage("sgl06.png"),
        "ky": ImageManager.createKomaImage("sgl07.png"),
        "hu": ImageManager.createKomaImage("sgl08.png"),
        "ry": ImageManager.createKomaImage("sgl12.png"),
        "um": ImageManager.createKomaImage("sgl13.png"),
        "ng": ImageManager.createKomaImage("sgl15.png"),
        "nk": ImageManager.createKomaImage("sgl16.png"),
        "ny": ImageManager.createKomaImage("sgl17.png"),
        "to": ImageManager.createKomaImage("sgl18.png")
      },
      'koma2': {
        "ou": ImageManager.createKomaImage("sgl31.png"),
        "hi": ImageManager.createKomaImage("sgl32.png"),
        "ka": ImageManager.createKomaImage("sgl33.png"),
        "ki": ImageManager.createKomaImage("sgl34.png"),
        "gi": ImageManager.createKomaImage("sgl35.png"),
        "ke": ImageManager.createKomaImage("sgl36.png"),
        "ky": ImageManager.createKomaImage("sgl37.png"),
        "hu": ImageManager.createKomaImage("sgl38.png"),
        "ry": ImageManager.createKomaImage("sgl42.png"),
        "um": ImageManager.createKomaImage("sgl43.png"),
        "ng": ImageManager.createKomaImage("sgl45.png"),
        "nk": ImageManager.createKomaImage("sgl46.png"),
        "ny": ImageManager.createKomaImage("sgl47.png"),
        "to": ImageManager.createKomaImage("sgl48.png")
      }
    };
  }

  static createKomaImage(name) {
    let image = new Image()
    image.src = "./images/koma/60x64/" + name;
    image.width = 60;
    image.height = 64;
    return image;
  }

  getKomaImage(koma, flip) {
    if (flip) {
      if (koma.playerNumber === 2) {
        return this.komaImages.koma1[koma.komaType];
      } else {
        return this.komaImages.koma2[koma.komaType];
      }
    } else {
      if (koma.playerNumber === 1) {
        return this.komaImages.koma1[koma.komaType];
      } else {
        return this.komaImages.koma2[koma.komaType];
      }
    }
  }

  getIconImage(url, character): HTMLImageElement {
    var str;
    let image = new Image();
    if (character !== void 0 && character !== null) {
      str = ("0000" + character).slice(-4);
      image.src = "./images/icon/snap" + str + ".png";
      return image;
    } else if (url) {
      image.src = url;
      return image;
    } else {
      image.src = './images/icon/noname.jpeg';
      return image;
    }
  }

  getRandomCharacter() {
    return Math.floor(Math.random() * 55);
  }

  getRandomNums(length) {
    let result = [];
    let newNum = 0;
    while (result.length < length) {
      newNum = this.getRandomCharacter();
      if (!result.some(item => item == newNum)) {
        result.push(newNum);
      }
    }
    return result;
  }

  getRandomCharacters(num: number): { num: number, image: HTMLImageElement }[] {
    return this.getRandomNums(10).map((num) => { return { num: num, image: this.getIconImage(null, num) }; });
  }
}

declare function require(x: string): any;
import Artifact = require('../../potaore/artifact');
import Base = require('./Base');

let getRandom = (array: any[], predicate) => {
  if (array.length == 0) return;
  predicate(array[Math.floor(array.length * Math.random())]);
};

interface MoveInfo {
  from: {
    position: any,
    komaType: any
  }
  to: {
    position: any,
    komaType: any
  }
}

class GameState {
  oute: boolean;
  foul: boolean;
  getKoma: boolean;
}

export class BotClient extends Artifact.Artifact {
  socket: any;
  game: Base.Game;
  board: Base.Board;
  state: GameState = new GameState();
  moveConvertor = new Base.MoveConvertor();
  strategy: Strategy;
  constructor() {
    super('bot-client');
  }

  start() {
    //var socket = require('socket.io-client')('http://133.130.72.92');
    var socket = require('socket.io-client')('http://localhost:8080');
    socket.on('connect', function () { });
    socket.on('event', function (data) { });
    socket.on('disconnect', function () { });
    socket.on('noticePlayerNumber', (arg) => {
      this.startGame(arg);
      this.executeIfMyTurn();
    });

    socket.on('command', (command) => {
      this.updateGame(command);
      this.executeIfMyTurn();
    });
    socket.on('synchronize', (command) => {
    });
    socket.on('endGame', (arg) => {
      this.next(() => {
        if (arg.kifu[arg.kifu.length - 1].info.winPlayerNumber == this.game.playerNumber) {
          socket.emit('message', 'あたしの勝ちね！');
        } else {
          socket.emit('message', 'まけちゃった。。。');
        }
        this.next(() => {
          socket.emit('room', { method: 'exitRoom' });
          this.next(() => {
            socket.emit('room', { method: 'cancelGame' });
            this.next(() => {
              socket.emit('room', { method: 'startGame', type: 'bot' });
            });
          });
        });
      });
    });

    socket.emit('auth.guestlogin.bot', { name: 'Regina (lv2.1)', character: 54 });
    socket.emit('room', { method: 'startGame', type: 'bot' });

    this.socket = socket;
  }

  startGame(arg) {
    this.game = new Base.Game(arg.playerNumber, arg.playerInfo.player1, arg.playerInfo.player2, this.socket);
    this.game.start();
    this.state = new GameState();
    if (this.strategy) this.strategy.dispose();
    this.strategy = new Strategy(this.game, this.moveConvertor, this.state);
  }

  updateGame(commands: any[]) {
    this.state.foul = false;
    let getKomaFlag = false;
    let outeFlag = false;
    let contradiction = false;
    commands.forEach((command) => {
      if (command.method == 'removeKomaFromKomadai') {
        this.game.board.removeKomaFromKomadai(command.playerNumber, command.koma.komaType);
      } else if (command.method == 'putKomaToKomadai') {
        this.game.board.putKomaToKomadai(command.playerNumber, command.koma);
      } else if (command.method == 'removeKoma') {
        this.game.board.removeKoma(command.position);
        this.strategy.getKoma(command.position);
      } else if (command.method == 'putKoma') {
        this.game.board.putKoma(command.koma, command.position);
      } else if (command.method == 'adjustTime') {
        this.game.board.player1.time = command.player1.time;
        this.game.board.player2.time = command.player2.time;
      } else if (command.method == 'foul') {
        this.game.getCurrentPlayer().life--;
        this.state.foul = true;
        this.strategy.foul();
      } else if (command.method == 'noticeOute') {
        outeFlag = command.oute;
      } else if (command.method == 'noticeGetKoma') {
        getKomaFlag = command.getKoma;
      } else if (command.method == 'contradiction') {
        contradiction = true;
      }
    });
    if (!contradiction) {
      if (this.state.foul) {
      } else {
        this.state.getKoma = getKomaFlag;
        this.state.oute = outeFlag;
        this.game.nextTurn();
        this.strategy.next();
        if (this.state.oute) {
          this.strategy.oute();
        }

        //console.log(JSON.stringify(this.strategy.predictionMap.board));
        //console.log(JSON.stringify(this.strategy.suppressionMap.booleanBoard));
      }
    } else {
      this.socket.emit('game', { method: 'synchronize' });
    }
  }

  executeIfMyTurn() {
    if (this.game.isPlayerTurn()) {
      this.next(() => this.execute());
    }
    this.notify('draw-prediction', this.strategy.predictionMap);
  }

  execute() {
    let moveInfo = this.strategy.execute();
    let action = moveInfo.from.position.x == -1 && moveInfo.from.position.y == -1 ? 'putKoma' : 'moveKoma';
    this.socket.emit('game', {
      method: action,
      args: moveInfo
    });
  }

  next(predicate) {
    setTimeout(function () {
      predicate();
    }, 500);
  }
}

export class Strategy {
  game: Base.Game;
  isKomaHitted: boolean = false;
  komaHittedCount: number = 0;
  foulHands: {} = {};
  tryPositions: any[] = [];
  moveInfoCache: MoveInfo;
  prevMoveInfo: MoveInfo;
  moveConvertor: Base.MoveConvertor;
  state: GameState;
  invatedPosition: { x: number, y: number };
  predictionMap: PredictionMap;
  moveHisyaInOpening: boolean = false;
  prevGetKoma: boolean = false;
  suppressionMap: SuppressionMap = new SuppressionMap();
  invatedMap: SuppressionMap = new SuppressionMap();
  foulCount: number = 0;
  constructor(game: Base.Game, moveConvertor: Base.MoveConvertor, state: GameState) {
    this.game = game;
    this.moveConvertor = moveConvertor;
    this.state = state;
    this.predictionMap = new PredictionMap('ou', moveConvertor, game, state, this);
    this.predictionMap.clearBoard();
    if (this.game.playerNumber == 1) {
      this.predictionMap.setPostion({ x: 4, y: 0 });
    } else {
      this.predictionMap.setPostion({ x: 4, y: 8 });
    }
  }

  dispose() {
    this.game = undefined;
    this.foulHands = undefined;
    this.tryPositions = undefined;
    this.moveInfoCache = undefined;
    this.prevMoveInfo = undefined;
    this.moveConvertor = undefined;
    this.invatedPosition = undefined;
    this.predictionMap.dispose();
    this.predictionMap = undefined;
    this.suppressionMap = undefined;
    this.invatedMap = undefined;
  }

  foul() {
    this.foulHands[JSON.stringify(this.moveInfoCache)] = true;
    this.foulCount++;
    if (this.moveInfoCache.from.komaType == 'ou') {
      this.invatedMap.surpress(this.moveInfoCache.to.position);
    }
  }

  next() {
    this.foulHands = {};
    this.tryPositions = [];
    if (this.game.isPlayerTurn()) {
      this.predictionMap.moveProbability(0.1);
      this.predictionMap.normalize();
    } else {
      this.prevGetKoma = this.state.getKoma;
      this.prevMoveInfo = this.moveInfoCache;

      this.toMoveInfo(this.game.board.getPlayerKoma(this.game.playerNumber), true).forEach(moveInfo => {
        this.suppressionMap.surpress(moveInfo.to.position);
        this.suppressionMap.surpress(moveInfo.from.position);
      });
    }
  }

  oute() {
    if (!this.game.isPlayerTurn()) {
      console.log('oute');
      let movablePositions = this.moveConvertor.getMovablePos(this.game.playerNumber, this.moveInfoCache.to.komaType, this.moveInfoCache.to.position, this.game.board);
      this.predictionMap.setPostions(movablePositions);
    }
  }

  getKoma(position) {
    this.invatedPosition = position;
  }

  isOpening(): boolean {
    if (this.game.turn < 8) return true;
    if (this.game.turn > 32) return false;
    let isOpening = 50;
    let isntOpening = this.game.turn * 4;
    if (this.isKomaHitted) isntOpening += 5;
    isntOpening += this.komaHittedCount * 2;
    return Math.random() < (isOpening / (isOpening + isntOpening));
  }

  execute(): MoveInfo {
    let _komaArray: any[] = this.game.board.getPlayerKoma(this.game.playerNumber);
    let komaArray: any[] = this.game.board.getPlayerKoma(this.game.playerNumber);
    let _komadaiArray: any[] = this.game.board.getPlayerKomadai(this.game.playerNumber);
    let komadaiArray: any[] = this.game.board.getPlayerKomadai(this.game.playerNumber);
    let moveInfoArray: MoveInfo[] = null;
    let executed = false;
    let result: MoveInfo = null;
    let count = 0;
    while (!executed) {
      count++;
      let senpou = '';

      if (count > 10) {
        senpou = 'なにをしたら良いかわからない';
        moveInfoArray = this.toMoveInfo(_komaArray);
      } else if (this.state.oute) {
        //王手時の対応
        senpou = '王手時の対応';

        //とりあえず駒は打たない
        komadaiArray = [];

        if (this.state.getKoma) {
          if (Math.random() > 0.2) {
            //取り返す
            moveInfoArray = this.toMoveInfo(_komaArray);
            moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
          } else {
            //王を動かす
            komaArray = this.use('ou', _komaArray);
            moveInfoArray = this.toMoveInfo(komaArray);
          }
        } else {
          //反則回数が多い状態では王を動かすことを優先する
          if (Math.random() > 0.6 - 0.06 * this.foulCount) {
            //王を動かす
            komaArray = this.use('ou', _komaArray);
            moveInfoArray = this.toMoveInfo(komaArray);

            //一度でも相手の利きのあった場所には移動しない
            let newMoveInfoArray = this.dontMoveToInvatedArea(moveInfoArray);
            if (newMoveInfoArray.length > 0) {
              moveInfoArray = newMoveInfoArray;
            }

          } else {
            //王以外を動かす
            let ou = this.game.board.getOu(this.game.playerNumber);

            //王の周りに駒を動かす
            let arroundPostions = Base.util.getArroundPos(ou.position);
            komaArray = this.dontUse('ou', _komaArray);
            moveInfoArray = this.toMoveInfo(komaArray);
            moveInfoArray = this.moveToPositions(arroundPostions, moveInfoArray);

            //一度試した場所は除外する（空き王手がらみがあるので本当はこの処理はだめ）
            let newMoveInfoArray = this.dontMoveToPositions(this.tryPositions, moveInfoArray);
            if (newMoveInfoArray.length > 0) {
              moveInfoArray = newMoveInfoArray;
            }
          }
        }
      } else if (this.state.getKoma && Math.random() > 0.2) {
        //駒を取られた場合の対応
        senpou = '駒を取られた場合の対応';

        //とりあえず駒は打たない
        komadaiArray = [];

        //取り返す
        moveInfoArray = this.toMoveInfo(_komaArray);
        moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
      } else if (this.prevGetKoma && Math.random() > 0.2) {
        //駒を取った直後の対応
        senpou = '駒を取った直後の対応';

        if (Math.random() > 0.5) {
          //指定した場所から動かす(取り返されないように駒をかわす)
          moveInfoArray = this.toMoveInfo(_komaArray);
          moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
        } else {
          //駒にひもをつける
          moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
          moveInfoArray = this.targetToPosition(this.prevMoveInfo.to.position, moveInfoArray);

          if (moveInfoArray.length == 0) {
            //指定した場所から動かす(取り返されないように駒をかわす)
            moveInfoArray = this.toMoveInfo(_komaArray);
            moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
          }
        }


      } else if (this.isOpening()) {
        //序盤の対応
        senpou = '序盤の対応';
        if (Math.random() > 0.7) {
          //王を動かす
          komaArray = this.use('ou', _komaArray);

          moveInfoArray = this.toMoveInfo(komaArray);
          //5筋から離れる
          moveInfoArray = this.leaveCenterColumn(moveInfoArray);
          //後退しない
          moveInfoArray = this.maynotBack(moveInfoArray);
        } else {
          //王を使わない
          komaArray = this.dontUse('ou', _komaArray);
          //香車を使わない
          komaArray = this.dontUse('ky', komaArray);
          //序盤は一度しか飛車を動かさない
          if (this.moveHisyaInOpening) {
            console.log(' dontUse hi');
            komaArray = this.dontUse('hi', komaArray);
          }

          moveInfoArray = this.toMoveInfo(komaArray);

          //金銀は繰り出す
          moveInfoArray = this.advanceKinGin2(moveInfoArray);
          //飛車は端に移動しない
          moveInfoArray = this.dontMoveHisyaToEnd(moveInfoArray);
        }
      } else {
        //その他の場合
        senpou = 'その他の場合';

        if (Math.random() > 0.3) {
          //王を使わない
          komaArray = this.dontUse('ou', _komaArray);
        } else {
          komaArray = _komaArray;
        }

        //反則回数が多いときは持ち駒の使用を控える
        //if (Math.random() > (9 - this.foulCount) / 10) {

        if (this.foulCount > 7) {
          moveInfoArray = this.toMoveInfo(komaArray);
        } else {
          moveInfoArray = this.toMoveInfo(komaArray.concat(_komadaiArray));
        }

        if (Math.random() > 0.4) {
          console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
          //制圧済み領域を広げる
          let moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);

          if (Math.random() > 0.4) {
            //金銀を繰り出す
            let moveInfoArray2 = this.advanceKinGin(moveInfoArray);
            moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
          } else {
            moveInfoArray = moveInfoArray1;
          }

          if (moveInfoArray.length == 0) {
            console.log('-> 予備：金銀は繰り出す');
            moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
            moveInfoArray = this.advanceKinGin2(moveInfoArray);
          }

        } else {
          console.log('混合戦略2：と金をつくる or 王の探索効率が最も良い手を指す');
          //と金をつくる手
          let moveInfoArray_tokin = this.makeTokin(moveInfoArray);
          //探索範囲の最大の手
          let moveInfoArray_most = this.mostEffective(moveInfoArray);

          moveInfoArray = moveInfoArray_tokin.concat(moveInfoArray_most);

          if (moveInfoArray.length == 0) {
            console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
            //制圧済み領域を広げる
            let moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);

            if (Math.random() > 0.4) {
              //金銀を繰り出す
              let moveInfoArray2 = this.advanceKinGin(moveInfoArray);
              moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
            } else {
              moveInfoArray = moveInfoArray1;
            }
          }
        }
      }

      moveInfoArray = this.dontRedoFoulHand(moveInfoArray);
      getRandom(moveInfoArray, moveInfo => {
        this.moveInfoCache = moveInfo;
        result = moveInfo;
        executed = true;
        console.log(this.game.turn + ' : ' + senpou);
        console.log(JSON.stringify(moveInfo));
        console.log('他候補 : ' + moveInfoArray.length);
        if (senpou == '序盤の対応' && moveInfo.from.komaType == 'hi') {
          this.moveHisyaInOpening = true;
        }
        if (senpou == '王手時の対応' && moveInfo.from.komaType != 'ou') {
          this.tryPositions.push(moveInfo.to.position);
        }
      });
    }

    return result;
  }

  toMoveInfo(komaArray: any[], short: boolean = false): MoveInfo[] {
    let result: MoveInfo[] = [];
    komaArray.forEach(koma => {
      let isKomadai = this.isKomadai(koma.position);
      let movablePosArray = isKomadai
        ? this.moveConvertor.getPutableCell(this.game.playerNumber, koma.koma.komaType, this.game.board)
        : short
          ? this.moveConvertor.getMovablePos(this.game.playerNumber, koma.koma.komaType, koma.position, this.game.board)
          : this.moveConvertor.getMovablePosShort(this.game.playerNumber, koma.koma.komaType, koma.position, this.game.board);
      movablePosArray.forEach(movablePos => {
        let moveInfo: MoveInfo = {
          from: {
            position: koma.position,
            komaType: koma.koma.komaType
          },
          to: {
            position: movablePos,
            komaType: koma.koma.komaType
          }
        };
        if (!isKomadai) {
          let nari = this.moveConvertor.getNari(koma.koma.playerNumber, koma.koma.komaType, moveInfo.from.position, moveInfo.to.position);
          if (nari == 'force' || nari == 'possible') {
            moveInfo.to.komaType = Base.komaTypes[moveInfo.to.komaType].nariId;
          }
        }
        result.push(moveInfo);
      });
    });
    return result;
  }

  toMovablePositions(moveInfo: MoveInfo) {
    return this.moveConvertor.getMovablePos(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
  }

  //座標が(-1, -1)であるかどうかを判定する
  isKomadai(position: { x: number, y: number }) {
    return position.x == -1 && position.y == -1;
  }

  //前に進める動きかどうかを判定する
  isAdvance(moveInfo: MoveInfo) {
    if (this.game.playerNumber == 1) {
      return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0;
    } else {
      return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0;
    }
  }

  //特定の駒を使わない
  dontUse(komaType: string, komaArray: any[]) {
    return komaArray.filter(koma => koma.koma.komaType != komaType);
  }

  //特定の駒を使う
  use(komaType: string, komaArray: any[]) {
    return komaArray.filter(koma => koma.koma.komaType == komaType);
  }

  //後退しない
  dontBack(moveInfoArray: MoveInfo[]) {
    if (this.game.playerNumber == 1) {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0);
    } else {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0);
    }
  }

  //あまり後退しない
  maynotBack(moveInfoArray: MoveInfo[]) {
    if (Math.random() > 0.5) return moveInfoArray;
    if (this.game.playerNumber == 1) {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y <= 0);
    } else {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y >= 0);
    }
  }

  //5筋から離れる
  leaveCenterColumn(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      if (moveInfo.from.position.x == 4) {
        return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x != 0;
      } else if (moveInfo.from.position.x > 4) {
        return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x >= 0;
      } else {
        return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x <= 0;
      }
    });
  }

  //指定した場所から動かす
  moveFromPosition(postion: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => Base.util.posEq(moveInfo.from.position, postion));
  }

  //指定した場所から動かす
  moveFromPositions(positions: { x: number, y: number }[], moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => positions.some(position => Base.util.posEq(moveInfo.from.position, position)));
  }

  //指定した場所に動かす
  moveToPosition(postion: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => Base.util.posEq(moveInfo.to.position, postion));
  }

  //指定した場所に動かす2
  moveToPositions(positions: { x: number, y: number }[], moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => positions.some(position => Base.util.posEq(moveInfo.to.position, position)));
  }

  //指定した場所に動かさない
  dontMoveToPosition(postion: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => !Base.util.posEq(moveInfo.to.position, postion));
  }

  //指定した場所に動かさない2
  dontMoveToPositions(positions: { x: number, y: number }[], moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => !positions.some(position => Base.util.posEq(moveInfo.to.position, position)));
  }

  //一度反則になった手を指さない
  dontRedoFoulHand(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      return !this.foulHands.hasOwnProperty(JSON.stringify(moveInfo));
    });
  }

  //一度でも相手の利きのあった場所には移動しない
  dontMoveToInvatedArea(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      return !this.invatedMap.hasSuppressed(moveInfo.to.position);
    });
  }

  //指定した場所にひもがつくようにする
  targetToPosition(position: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      let movablepositions = this.toMovablePositions(moveInfo);
      return movablepositions.some(p => Base.util.posEq(p, position));
    });
  }

  //制圧済み領域が広がる手を指す
  expandSuppressedArea(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      return this.toMovablePositions(moveInfo).some(p => !this.suppressionMap.hasSuppressed(p));
    });
  }

  //王の探索範囲が変わらない手を指さない
  dontMoveUseless(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      let movablePositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
      let sum = 0;
      movablePositions.forEach(p => {
        sum += this.predictionMap.board[p.y][p.x];
      });
      return sum != 0;
    });
  }

  //王の探索効率が最も良い手を指す
  mostEffective(moveInfoArray: MoveInfo[]) {
    let top = 0;
    let topMoveInfo = null;
    moveInfoArray.forEach(moveInfo => {
      let movablePositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
      let sum = 0;
      movablePositions.forEach(p => {
        sum += this.predictionMap.board[p.y][p.x];
      });
      if (sum > top) {
        top = sum;
        topMoveInfo = moveInfo;
      }
    });
    if (topMoveInfo == null) {
      return [];
    } else {
      return [topMoveInfo];
    }
  }

  //飛車は端に移動しない
  dontMoveHisyaToEnd(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => moveInfo.from.komaType != 'hi' || (moveInfo.to.position.x != 0 && moveInfo.to.position.x != 8));
  }

  //と金つくりの手を抽出する
  makeTokin(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => moveInfo.from.komaType == 'hu' && moveInfo.from.komaType == 'to');
  }

  //金銀を繰り出す
  advanceKinGin(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
        return this.isAdvance(moveInfo);
      }
      return false;
    });
  }

  //金銀は繰り出す
  advanceKinGin2(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
        return this.isAdvance(moveInfo);
      }
      return true;
    });
  }
}

export class PredictionMap {
  komaType: string;
  board: number[][];
  moveConvertor: Base.MoveConvertor;
  game: Base.Game;
  state: GameState;
  strategy: Strategy;
  constructor(komaType: string, moveConvertor: Base.MoveConvertor, game: Base.Game, state: GameState, strategy: Strategy) {
    this.komaType = komaType;
    this.moveConvertor = moveConvertor;
    this.game = game;
    this.state = state;
    this.strategy = strategy;
  }

  dispose() {
    this.board = undefined;
    this.moveConvertor = undefined;
    this.game = undefined;
    this.state = undefined;
    this.strategy = undefined;
  }

  clearBoard() {
    this.board = this.getNewBoard();
  }

  getNewBoard() {
    return [
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
  }

  setPostion(position: { x: number, y: number }) {
    this.clearBoard();
    this.board[position.y][position.x] = 1;
  }

  setPostions(positions: { x: number, y: number }[]) {
    let board = this.getNewBoard();
    positions.forEach(p => {
      board[p.y][p.x] = this.board[p.y][p.x] / positions.length;
    });
    this.board = board;
    this.normalize();
  }

  normalize() {
    let sum = 0;
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.board[y][x] < 1 / 1000000) this.board[y][x] = 0;
        sum += this.board[y][x];
      }
    }
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.board[y][x] == 0) continue;
        this.board[y][x] = this.board[y][x] / sum;
      }
    }
  }

  moveProbability(rate) {
    let board = this.getNewBoard();
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.state.getKoma) {
          board[this.strategy.invatedPosition.y][this.strategy.invatedPosition.x] += rate * this.board[y][x];
        } else {
          let movablePositions = this.moveConvertor.getMovablePos(
            3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
          movablePositions.forEach(pos => {
            board[pos.y][pos.x] += rate / movablePositions.length * this.board[y][x];
          });
        }
        board[y][x] += this.board[y][x] * (1 - rate);
        if (this.game.board.getKoma({ x: x, y: y })) {
          board[y][x] = 0;
        }
      }
    }
    this.board = board;
    let komaArray: any[] = this.game.board.getPlayerKoma(this.game.playerNumber);
    komaArray.forEach(koma => {
      let movablepositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, koma.koma.komaType, koma.position, this.game.board);
      movablepositions.forEach(p => {
        this.board[p.y][p.x] = 0;
      });
    });
  }
}

class SuppressionMap {
  board: number[][]
  booleanBoard: boolean[][]
  constructor() {
    this.board = this.getNewBoard();
    this.booleanBoard = this.getNewBoolenaBoard();
  }
  getNewBoard() {
    return [
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
  }

  getNewBoolenaBoard() {
    return [
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false]
    ];
  }

  surpress(position: { x: number; y: number }) {
    this.board[position.y][position.x] = -1;
    this.booleanBoard[position.y][position.x] = true;
  }

  next() {
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        this.board[y][x]++;
      }
    }
  }

  getUnsuppressedPosition() {
    let result: { x: number, y: number }[] = [];
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (!this.booleanBoard[y][x]) {
          result.push({ x: x, y: y });
        }
      }
    }
    return result;
  }

  hasSuppressed(position: { x: number, y: number }): boolean {
    return this.booleanBoard[position.y][position.x];
  }
}

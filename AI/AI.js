var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var Base = require('./Base');
var getRandom = function (array, predicate) {
    if (array.length == 0)
        return;
    predicate(array[Math.floor(array.length * Math.random())]);
};
var GameState = (function () {
    function GameState() {
    }
    return GameState;
})();
var BotClient = (function (_super) {
    __extends(BotClient, _super);
    function BotClient() {
        _super.call(this, 'bot-client');
        this.state = new GameState();
        this.moveConvertor = new Base.MoveConvertor();
    }
    BotClient.prototype.start = function () {
        var _this = this;
        //var socket = require('socket.io-client')('http://133.130.72.92');
        var socket = require('socket.io-client')('http://localhost:8080');
        socket.on('connect', function () { });
        socket.on('event', function (data) { });
        socket.on('disconnect', function () { });
        socket.on('noticePlayerNumber', function (arg) {
            _this.startGame(arg);
            _this.executeIfMyTurn();
        });
        socket.on('command', function (command) {
            _this.updateGame(command);
            _this.executeIfMyTurn();
        });
        socket.on('synchronize', function (command) {
        });
        socket.on('endGame', function (arg) {
            _this.next(function () {
                if (arg.kifu[arg.kifu.length - 1].info.winPlayerNumber == _this.game.playerNumber) {
                    socket.emit('message', 'あたしの勝ちね！');
                }
                else {
                    socket.emit('message', 'まけちゃった。。。');
                }
                _this.next(function () {
                    socket.emit('room', { method: 'exitRoom' });
                    _this.next(function () {
                        socket.emit('room', { method: 'cancelGame' });
                        _this.next(function () {
                            socket.emit('room', { method: 'startGame', type: 'bot' });
                        });
                    });
                });
            });
        });
        socket.emit('auth.guestlogin.bot', { name: 'Regina (lv2.1)', character: 54 });
        socket.emit('room', { method: 'startGame', type: 'bot' });
        this.socket = socket;
    };
    BotClient.prototype.startGame = function (arg) {
        this.game = new Base.Game(arg.playerNumber, arg.playerInfo.player1, arg.playerInfo.player2, this.socket);
        this.game.start();
        this.state = new GameState();
        if (this.strategy)
            this.strategy.dispose();
        this.strategy = new Strategy(this.game, this.moveConvertor, this.state);
    };
    BotClient.prototype.updateGame = function (commands) {
        var _this = this;
        this.state.foul = false;
        var getKomaFlag = false;
        var outeFlag = false;
        var contradiction = false;
        commands.forEach(function (command) {
            if (command.method == 'removeKomaFromKomadai') {
                _this.game.board.removeKomaFromKomadai(command.playerNumber, command.koma.komaType);
            }
            else if (command.method == 'putKomaToKomadai') {
                _this.game.board.putKomaToKomadai(command.playerNumber, command.koma);
            }
            else if (command.method == 'removeKoma') {
                _this.game.board.removeKoma(command.position);
                _this.strategy.getKoma(command.position);
            }
            else if (command.method == 'putKoma') {
                _this.game.board.putKoma(command.koma, command.position);
            }
            else if (command.method == 'adjustTime') {
                _this.game.board.player1.time = command.player1.time;
                _this.game.board.player2.time = command.player2.time;
            }
            else if (command.method == 'foul') {
                _this.game.getCurrentPlayer().life--;
                _this.state.foul = true;
                _this.strategy.foul();
            }
            else if (command.method == 'noticeOute') {
                outeFlag = command.oute;
            }
            else if (command.method == 'noticeGetKoma') {
                getKomaFlag = command.getKoma;
            }
            else if (command.method == 'contradiction') {
                contradiction = true;
            }
        });
        if (!contradiction) {
            if (this.state.foul) {
            }
            else {
                this.state.getKoma = getKomaFlag;
                this.state.oute = outeFlag;
                this.game.nextTurn();
                this.strategy.next();
                if (this.state.oute) {
                    this.strategy.oute();
                }
            }
        }
        else {
            this.socket.emit('game', { method: 'synchronize' });
        }
    };
    BotClient.prototype.executeIfMyTurn = function () {
        var _this = this;
        if (this.game.isPlayerTurn()) {
            this.next(function () { return _this.execute(); });
        }
        this.notify('draw-prediction', this.strategy.predictionMap);
    };
    BotClient.prototype.execute = function () {
        var moveInfo = this.strategy.execute();
        var action = moveInfo.from.position.x == -1 && moveInfo.from.position.y == -1 ? 'putKoma' : 'moveKoma';
        this.socket.emit('game', {
            method: action,
            args: moveInfo
        });
    };
    BotClient.prototype.next = function (predicate) {
        setTimeout(function () {
            predicate();
        }, 500);
    };
    return BotClient;
})(Artifact.Artifact);
exports.BotClient = BotClient;
var Strategy = (function () {
    function Strategy(game, moveConvertor, state) {
        this.isKomaHitted = false;
        this.komaHittedCount = 0;
        this.foulHands = {};
        this.tryPositions = [];
        this.moveHisyaInOpening = false;
        this.prevGetKoma = false;
        this.suppressionMap = new SuppressionMap();
        this.invatedMap = new SuppressionMap();
        this.foulCount = 0;
        this.game = game;
        this.moveConvertor = moveConvertor;
        this.state = state;
        this.predictionMap = new PredictionMap('ou', moveConvertor, game, state, this);
        this.predictionMap.clearBoard();
        if (this.game.playerNumber == 1) {
            this.predictionMap.setPostion({ x: 4, y: 0 });
        }
        else {
            this.predictionMap.setPostion({ x: 4, y: 8 });
        }
    }
    Strategy.prototype.dispose = function () {
        this.game = undefined;
        this.foulHands = undefined;
        this.tryPositions = undefined;
        this.moveInfoCache = undefined;
        this.prevMoveInfo = undefined;
        this.moveConvertor = undefined;
        this.invatedPosition = undefined;
        this.predictionMap.dispose();
        this.predictionMap = undefined;
        this.suppressionMap = undefined;
        this.invatedMap = undefined;
    };
    Strategy.prototype.foul = function () {
        this.foulHands[JSON.stringify(this.moveInfoCache)] = true;
        this.foulCount++;
        if (this.moveInfoCache.from.komaType == 'ou') {
            this.invatedMap.surpress(this.moveInfoCache.to.position);
        }
    };
    Strategy.prototype.next = function () {
        var _this = this;
        this.foulHands = {};
        this.tryPositions = [];
        if (this.game.isPlayerTurn()) {
            this.predictionMap.moveProbability(0.1);
            this.predictionMap.normalize();
        }
        else {
            this.prevGetKoma = this.state.getKoma;
            this.prevMoveInfo = this.moveInfoCache;
            this.toMoveInfo(this.game.board.getPlayerKoma(this.game.playerNumber), true).forEach(function (moveInfo) {
                _this.suppressionMap.surpress(moveInfo.to.position);
                _this.suppressionMap.surpress(moveInfo.from.position);
            });
        }
    };
    Strategy.prototype.oute = function () {
        if (!this.game.isPlayerTurn()) {
            console.log('oute');
            var movablePositions = this.moveConvertor.getMovablePos(this.game.playerNumber, this.moveInfoCache.to.komaType, this.moveInfoCache.to.position, this.game.board);
            this.predictionMap.setPostions(movablePositions);
        }
    };
    Strategy.prototype.getKoma = function (position) {
        this.invatedPosition = position;
    };
    Strategy.prototype.isOpening = function () {
        if (this.game.turn < 8)
            return true;
        if (this.game.turn > 32)
            return false;
        var isOpening = 50;
        var isntOpening = this.game.turn * 4;
        if (this.isKomaHitted)
            isntOpening += 5;
        isntOpening += this.komaHittedCount * 2;
        return Math.random() < (isOpening / (isOpening + isntOpening));
    };
    Strategy.prototype.execute = function () {
        var _this = this;
        var _komaArray = this.game.board.getPlayerKoma(this.game.playerNumber);
        var komaArray = this.game.board.getPlayerKoma(this.game.playerNumber);
        var _komadaiArray = this.game.board.getPlayerKomadai(this.game.playerNumber);
        var komadaiArray = this.game.board.getPlayerKomadai(this.game.playerNumber);
        var moveInfoArray = null;
        var executed = false;
        var result = null;
        var count = 0;
        while (!executed) {
            count++;
            var senpou = '';
            if (count > 10) {
                senpou = 'なにをしたら良いかわからない';
                moveInfoArray = this.toMoveInfo(_komaArray);
            }
            else if (this.state.oute) {
                //王手時の対応
                senpou = '王手時の対応';
                //とりあえず駒は打たない
                komadaiArray = [];
                if (this.state.getKoma) {
                    if (Math.random() > 0.2) {
                        //取り返す
                        moveInfoArray = this.toMoveInfo(_komaArray);
                        moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
                    }
                    else {
                        //王を動かす
                        komaArray = this.use('ou', _komaArray);
                        moveInfoArray = this.toMoveInfo(komaArray);
                    }
                }
                else {
                    //反則回数が多い状態では王を動かすことを優先する
                    if (Math.random() > 0.6 - 0.06 * this.foulCount) {
                        //王を動かす
                        komaArray = this.use('ou', _komaArray);
                        moveInfoArray = this.toMoveInfo(komaArray);
                        //一度でも相手の利きのあった場所には移動しない
                        var newMoveInfoArray = this.dontMoveToInvatedArea(moveInfoArray);
                        if (newMoveInfoArray.length > 0) {
                            moveInfoArray = newMoveInfoArray;
                        }
                    }
                    else {
                        //王以外を動かす
                        var ou = this.game.board.getOu(this.game.playerNumber);
                        //王の周りに駒を動かす
                        var arroundPostions = Base.util.getArroundPos(ou.position);
                        komaArray = this.dontUse('ou', _komaArray);
                        moveInfoArray = this.toMoveInfo(komaArray);
                        moveInfoArray = this.moveToPositions(arroundPostions, moveInfoArray);
                        //一度試した場所は除外する（空き王手がらみがあるので本当はこの処理はだめ）
                        var newMoveInfoArray = this.dontMoveToPositions(this.tryPositions, moveInfoArray);
                        if (newMoveInfoArray.length > 0) {
                            moveInfoArray = newMoveInfoArray;
                        }
                    }
                }
            }
            else if (this.state.getKoma && Math.random() > 0.2) {
                //駒を取られた場合の対応
                senpou = '駒を取られた場合の対応';
                //とりあえず駒は打たない
                komadaiArray = [];
                //取り返す
                moveInfoArray = this.toMoveInfo(_komaArray);
                moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
            }
            else if (this.prevGetKoma && Math.random() > 0.2) {
                //駒を取った直後の対応
                senpou = '駒を取った直後の対応';
                if (Math.random() > 0.5) {
                    //指定した場所から動かす(取り返されないように駒をかわす)
                    moveInfoArray = this.toMoveInfo(_komaArray);
                    moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
                }
                else {
                    //駒にひもをつける
                    moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
                    moveInfoArray = this.targetToPosition(this.prevMoveInfo.to.position, moveInfoArray);
                    if (moveInfoArray.length == 0) {
                        //指定した場所から動かす(取り返されないように駒をかわす)
                        moveInfoArray = this.toMoveInfo(_komaArray);
                        moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
                    }
                }
            }
            else if (this.isOpening()) {
                //序盤の対応
                senpou = '序盤の対応';
                if (Math.random() > 0.7) {
                    //王を動かす
                    komaArray = this.use('ou', _komaArray);
                    moveInfoArray = this.toMoveInfo(komaArray);
                    //5筋から離れる
                    moveInfoArray = this.leaveCenterColumn(moveInfoArray);
                    //後退しない
                    moveInfoArray = this.maynotBack(moveInfoArray);
                }
                else {
                    //王を使わない
                    komaArray = this.dontUse('ou', _komaArray);
                    //香車を使わない
                    komaArray = this.dontUse('ky', komaArray);
                    //序盤は一度しか飛車を動かさない
                    if (this.moveHisyaInOpening) {
                        console.log(' dontUse hi');
                        komaArray = this.dontUse('hi', komaArray);
                    }
                    moveInfoArray = this.toMoveInfo(komaArray);
                    //金銀は繰り出す
                    moveInfoArray = this.advanceKinGin2(moveInfoArray);
                    //飛車は端に移動しない
                    moveInfoArray = this.dontMoveHisyaToEnd(moveInfoArray);
                }
            }
            else {
                //その他の場合
                senpou = 'その他の場合';
                if (Math.random() > 0.3) {
                    //王を使わない
                    komaArray = this.dontUse('ou', _komaArray);
                }
                else {
                    komaArray = _komaArray;
                }
                //反則回数が多いときは持ち駒の使用を控える
                //if (Math.random() > (9 - this.foulCount) / 10) {
                if (this.foulCount > 7) {
                    moveInfoArray = this.toMoveInfo(komaArray);
                }
                else {
                    moveInfoArray = this.toMoveInfo(komaArray.concat(_komadaiArray));
                }
                if (Math.random() > 0.4) {
                    console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
                    //制圧済み領域を広げる
                    var moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);
                    if (Math.random() > 0.4) {
                        //金銀を繰り出す
                        var moveInfoArray2 = this.advanceKinGin(moveInfoArray);
                        moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
                    }
                    else {
                        moveInfoArray = moveInfoArray1;
                    }
                    if (moveInfoArray.length == 0) {
                        console.log('-> 予備：金銀は繰り出す');
                        moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
                        moveInfoArray = this.advanceKinGin2(moveInfoArray);
                    }
                }
                else {
                    console.log('混合戦略2：と金をつくる or 王の探索効率が最も良い手を指す');
                    //と金をつくる手
                    var moveInfoArray_tokin = this.makeTokin(moveInfoArray);
                    //探索範囲の最大の手
                    var moveInfoArray_most = this.mostEffective(moveInfoArray);
                    moveInfoArray = moveInfoArray_tokin.concat(moveInfoArray_most);
                    if (moveInfoArray.length == 0) {
                        console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
                        //制圧済み領域を広げる
                        var moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);
                        if (Math.random() > 0.4) {
                            //金銀を繰り出す
                            var moveInfoArray2 = this.advanceKinGin(moveInfoArray);
                            moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
                        }
                        else {
                            moveInfoArray = moveInfoArray1;
                        }
                    }
                }
            }
            moveInfoArray = this.dontRedoFoulHand(moveInfoArray);
            getRandom(moveInfoArray, function (moveInfo) {
                _this.moveInfoCache = moveInfo;
                result = moveInfo;
                executed = true;
                console.log(_this.game.turn + ' : ' + senpou);
                console.log(JSON.stringify(moveInfo));
                console.log('他候補 : ' + moveInfoArray.length);
                if (senpou == '序盤の対応' && moveInfo.from.komaType == 'hi') {
                    _this.moveHisyaInOpening = true;
                }
                if (senpou == '王手時の対応' && moveInfo.from.komaType != 'ou') {
                    _this.tryPositions.push(moveInfo.to.position);
                }
            });
        }
        return result;
    };
    Strategy.prototype.toMoveInfo = function (komaArray, short) {
        var _this = this;
        if (short === void 0) { short = false; }
        var result = [];
        komaArray.forEach(function (koma) {
            var isKomadai = _this.isKomadai(koma.position);
            var movablePosArray = isKomadai
                ? _this.moveConvertor.getPutableCell(_this.game.playerNumber, koma.koma.komaType, _this.game.board)
                : short
                    ? _this.moveConvertor.getMovablePos(_this.game.playerNumber, koma.koma.komaType, koma.position, _this.game.board)
                    : _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, koma.koma.komaType, koma.position, _this.game.board);
            movablePosArray.forEach(function (movablePos) {
                var moveInfo = {
                    from: {
                        position: koma.position,
                        komaType: koma.koma.komaType
                    },
                    to: {
                        position: movablePos,
                        komaType: koma.koma.komaType
                    }
                };
                if (!isKomadai) {
                    var nari = _this.moveConvertor.getNari(koma.koma.playerNumber, koma.koma.komaType, moveInfo.from.position, moveInfo.to.position);
                    if (nari == 'force' || nari == 'possible') {
                        moveInfo.to.komaType = Base.komaTypes[moveInfo.to.komaType].nariId;
                    }
                }
                result.push(moveInfo);
            });
        });
        return result;
    };
    Strategy.prototype.toMovablePositions = function (moveInfo) {
        return this.moveConvertor.getMovablePos(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
    };
    //座標が(-1, -1)であるかどうかを判定する
    Strategy.prototype.isKomadai = function (position) {
        return position.x == -1 && position.y == -1;
    };
    //前に進める動きかどうかを判定する
    Strategy.prototype.isAdvance = function (moveInfo) {
        if (this.game.playerNumber == 1) {
            return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0;
        }
        else {
            return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0;
        }
    };
    //特定の駒を使わない
    Strategy.prototype.dontUse = function (komaType, komaArray) {
        return komaArray.filter(function (koma) { return koma.koma.komaType != komaType; });
    };
    //特定の駒を使う
    Strategy.prototype.use = function (komaType, komaArray) {
        return komaArray.filter(function (koma) { return koma.koma.komaType == komaType; });
    };
    //後退しない
    Strategy.prototype.dontBack = function (moveInfoArray) {
        var _this = this;
        if (this.game.playerNumber == 1) {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0; });
        }
        else {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0; });
        }
    };
    //あまり後退しない
    Strategy.prototype.maynotBack = function (moveInfoArray) {
        var _this = this;
        if (Math.random() > 0.5)
            return moveInfoArray;
        if (this.game.playerNumber == 1) {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y <= 0; });
        }
        else {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y >= 0; });
        }
    };
    //5筋から離れる
    Strategy.prototype.leaveCenterColumn = function (moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) {
            if (moveInfo.from.position.x == 4) {
                return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x != 0;
            }
            else if (moveInfo.from.position.x > 4) {
                return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x >= 0;
            }
            else {
                return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x <= 0;
            }
        });
    };
    //指定した場所から動かす
    Strategy.prototype.moveFromPosition = function (postion, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return Base.util.posEq(moveInfo.from.position, postion); });
    };
    //指定した場所から動かす
    Strategy.prototype.moveFromPositions = function (positions, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return positions.some(function (position) { return Base.util.posEq(moveInfo.from.position, position); }); });
    };
    //指定した場所に動かす
    Strategy.prototype.moveToPosition = function (postion, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return Base.util.posEq(moveInfo.to.position, postion); });
    };
    //指定した場所に動かす2
    Strategy.prototype.moveToPositions = function (positions, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return positions.some(function (position) { return Base.util.posEq(moveInfo.to.position, position); }); });
    };
    //指定した場所に動かさない
    Strategy.prototype.dontMoveToPosition = function (postion, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return !Base.util.posEq(moveInfo.to.position, postion); });
    };
    //指定した場所に動かさない2
    Strategy.prototype.dontMoveToPositions = function (positions, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return !positions.some(function (position) { return Base.util.posEq(moveInfo.to.position, position); }); });
    };
    //一度反則になった手を指さない
    Strategy.prototype.dontRedoFoulHand = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            return !_this.foulHands.hasOwnProperty(JSON.stringify(moveInfo));
        });
    };
    //一度でも相手の利きのあった場所には移動しない
    Strategy.prototype.dontMoveToInvatedArea = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            return !_this.invatedMap.hasSuppressed(moveInfo.to.position);
        });
    };
    //指定した場所にひもがつくようにする
    Strategy.prototype.targetToPosition = function (position, moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            var movablepositions = _this.toMovablePositions(moveInfo);
            return movablepositions.some(function (p) { return Base.util.posEq(p, position); });
        });
    };
    //制圧済み領域が広がる手を指す
    Strategy.prototype.expandSuppressedArea = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            return _this.toMovablePositions(moveInfo).some(function (p) { return !_this.suppressionMap.hasSuppressed(p); });
        });
    };
    //王の探索範囲が変わらない手を指さない
    Strategy.prototype.dontMoveUseless = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            var movablePositions = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, _this.game.board);
            var sum = 0;
            movablePositions.forEach(function (p) {
                sum += _this.predictionMap.board[p.y][p.x];
            });
            return sum != 0;
        });
    };
    //王の探索効率が最も良い手を指す
    Strategy.prototype.mostEffective = function (moveInfoArray) {
        var _this = this;
        var top = 0;
        var topMoveInfo = null;
        moveInfoArray.forEach(function (moveInfo) {
            var movablePositions = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, _this.game.board);
            var sum = 0;
            movablePositions.forEach(function (p) {
                sum += _this.predictionMap.board[p.y][p.x];
            });
            if (sum > top) {
                top = sum;
                topMoveInfo = moveInfo;
            }
        });
        if (topMoveInfo == null) {
            return [];
        }
        else {
            return [topMoveInfo];
        }
    };
    //飛車は端に移動しない
    Strategy.prototype.dontMoveHisyaToEnd = function (moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return moveInfo.from.komaType != 'hi' || (moveInfo.to.position.x != 0 && moveInfo.to.position.x != 8); });
    };
    //と金つくりの手を抽出する
    Strategy.prototype.makeTokin = function (moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return moveInfo.from.komaType == 'hu' && moveInfo.from.komaType == 'to'; });
    };
    //金銀を繰り出す
    Strategy.prototype.advanceKinGin = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
                return _this.isAdvance(moveInfo);
            }
            return false;
        });
    };
    //金銀は繰り出す
    Strategy.prototype.advanceKinGin2 = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
                return _this.isAdvance(moveInfo);
            }
            return true;
        });
    };
    return Strategy;
})();
exports.Strategy = Strategy;
var PredictionMap = (function () {
    function PredictionMap(komaType, moveConvertor, game, state, strategy) {
        this.komaType = komaType;
        this.moveConvertor = moveConvertor;
        this.game = game;
        this.state = state;
        this.strategy = strategy;
    }
    PredictionMap.prototype.dispose = function () {
        this.board = undefined;
        this.moveConvertor = undefined;
        this.game = undefined;
        this.state = undefined;
        this.strategy = undefined;
    };
    PredictionMap.prototype.clearBoard = function () {
        this.board = this.getNewBoard();
    };
    PredictionMap.prototype.getNewBoard = function () {
        return [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
    };
    PredictionMap.prototype.setPostion = function (position) {
        this.clearBoard();
        this.board[position.y][position.x] = 1;
    };
    PredictionMap.prototype.setPostions = function (positions) {
        var _this = this;
        var board = this.getNewBoard();
        positions.forEach(function (p) {
            board[p.y][p.x] = _this.board[p.y][p.x] / positions.length;
        });
        this.board = board;
        this.normalize();
    };
    PredictionMap.prototype.normalize = function () {
        var sum = 0;
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.board[y][x] < 1 / 1000000)
                    this.board[y][x] = 0;
                sum += this.board[y][x];
            }
        }
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.board[y][x] == 0)
                    continue;
                this.board[y][x] = this.board[y][x] / sum;
            }
        }
    };
    PredictionMap.prototype.moveProbability = function (rate) {
        var _this = this;
        var board = this.getNewBoard();
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.state.getKoma) {
                    board[this.strategy.invatedPosition.y][this.strategy.invatedPosition.x] += rate * this.board[y][x];
                }
                else {
                    var movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
                    movablePositions.forEach(function (pos) {
                        board[pos.y][pos.x] += rate / movablePositions.length * _this.board[y][x];
                    });
                }
                board[y][x] += this.board[y][x] * (1 - rate);
                if (this.game.board.getKoma({ x: x, y: y })) {
                    board[y][x] = 0;
                }
            }
        }
        this.board = board;
        var komaArray = this.game.board.getPlayerKoma(this.game.playerNumber);
        komaArray.forEach(function (koma) {
            var movablepositions = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, koma.koma.komaType, koma.position, _this.game.board);
            movablepositions.forEach(function (p) {
                _this.board[p.y][p.x] = 0;
            });
        });
    };
    return PredictionMap;
})();
exports.PredictionMap = PredictionMap;
var SuppressionMap = (function () {
    function SuppressionMap() {
        this.board = this.getNewBoard();
        this.booleanBoard = this.getNewBoolenaBoard();
    }
    SuppressionMap.prototype.getNewBoard = function () {
        return [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
    };
    SuppressionMap.prototype.getNewBoolenaBoard = function () {
        return [
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false]
        ];
    };
    SuppressionMap.prototype.surpress = function (position) {
        this.board[position.y][position.x] = -1;
        this.booleanBoard[position.y][position.x] = true;
    };
    SuppressionMap.prototype.next = function () {
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                this.board[y][x]++;
            }
        }
    };
    SuppressionMap.prototype.getUnsuppressedPosition = function () {
        var result = [];
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (!this.booleanBoard[y][x]) {
                    result.push({ x: x, y: y });
                }
            }
        }
        return result;
    };
    SuppressionMap.prototype.hasSuppressed = function (position) {
        return this.booleanBoard[position.y][position.x];
    };
    return SuppressionMap;
})();

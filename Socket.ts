//Copyright (c) 2015-2016 potalong_oreo All rights reserved

declare let require: any;
import Artifact = require('../potaore/artifact');
declare let io: any;

export class Socket extends Artifact.Artifact {
  socket: any;
  constructor() {
    super('socket');
    this.on('user-message-send', (eventName, sender, message) => this.socket.emit('message', message));

    this.on('user-request-playing-games', (eventName, sender, data) => this.socket.emit('getPlayingGames', data));
    this.on('user-request-own-history', (eventName, sender, data) => this.socket.emit('getHistory', data));
    this.on('user-request-own-profile', (eventName, sender, data) => this.socket.emit('getProfile', data));

    this.on('user-game-start', (eventName, sender, type) => {
      console.log('start:' + type);
      this.socket.emit('room', { method: 'startGame', type: type });
    });

    this.on('user-game-create', (eventName, sender, type) => {
      console.log('start:' + type);
      this.socket.emit('room', { method: 'startGame', type: type });
    });

    this.on('create-rute', (eventName, sender, custom) => {
      console.log('create rule:' + custom);
      this.socket.emit('room', { method: 'createGameRule', custom : custom, type: 'custom' });
    });

    this.on('user-custom-start', (eventName, sender, gameId) => {
      console.log('start :' +  'custom');
      this.socket.emit('room', { method: 'asignCustomGameRule', type: 'custom', gameId : gameId });
    });
    
    this.on('user-game-restart', (eventName, sender, type) => this.socket.emit('restartGame'));

    this.on('user-game-cancel', () => this.socket.emit('room', { method: 'cancelGame' }));

    this.on('user-game-exit', () => this.socket.emit('room', { method: 'exitRoom' }));

    this.on('user-game-watch', (eventName, sender, gameId) => this.socket.emit('room', { method: 'watch', gameId: gameId }));

    this.on('user-game-resign', (eventName, sender, options) => this.socket.emit('game', { method: 'resign' }));

    this.on('user-game-try-move-koma', (eventName, sender, moveInfo) => this.socket.emit('game', { method: 'moveKoma', args: moveInfo }));

    this.on('user-game-try-put-koma', (eventName, sender, moveInfo) => this.socket.emit('game', { method: 'putKoma', args: moveInfo }));

    this.on('connect-account', (eventName, sender, options) => {
      this.connectSocket();
      this.socket.emit('auth.login', { auth: options.loginstr, character: options.character });
    });

    this.on('connect-guest', (eventName, sender, options) => {
      this.connectSocket();
      this.socket.emit('auth.guestlogin', { name: options.loginstr, character: options.character });
    });

    this.on('create-account', (eventName, sender, options) => {
      this.connectSocket();
      this.socket.emit('auth.create', { name: options.loginstr, character: options.character });
    });
    
    this.on('connect-session', (eventName, sender, sessionId) => {
      this.connectSocket();
      this.socket.emit('auth.session', sessionId );
    });

    this.on('user-logout', () => this.socket.disconnect());

    this.on('game-timeup', (eventName, sender, options) => this.socket.emit('game', options));

    this.on('game-need-synchronize', (eventName, sender, options) => this.socket.emit('game', { method: 'synchronize' }));
  }

  connectSocket() {
    if (this.socket) { 
      this.socket.connect();
    } else {
      //var socket = io("http://133.130.72.92/");
      var socket = io();
      
      this.socket = socket;

      socket.on('auth.login-ok', (arg) => this.notify('receive-login-ok', arg));
      
      socket.on('publishSessionId', (arg) => this.notify('receive-session-id', arg));

      socket.on('auth.login-ng', (arg) => this.notify('receive-show-modal', { messages: [arg], options: { ok: true } }));

      socket.on('auth.login-create', (arg) => this.notify('receive-account-created', arg));

      socket.on('disconnect', (arg) => this.notify('logout', arg));

      socket.on('message', (arg) => this.notify('receive-message', arg));

      socket.on('connectionInfo', (info) => this.notify('receive-connection-info', info));

      socket.on('receivePlayingGames', (data) => this.notify('receive-playing-games', data));

      socket.on('receiveProfile', (data) => this.notify('receive-profile', data));

      socket.on('noticePlayerNumber', arg => this.notify('receive-game-start', arg));

      socket.on('gameMessage', message => this.notify('receive-game-message', message));

      socket.on('command', command => this.notify('receive-game-command', command));

      socket.on('synchronize', command => this.notify('receive-game-synchronize', command));
      
      socket.on('needRestartGame', command => this.notify('receive-game-need-restart'));
      
      socket.on('restartGame', command => this.notify('receive-game-restart', command));

      socket.on('endGame', (arg) => this.notify('receive-game-end', arg));

      socket.on('kifu', (arg) => this.notify('receive-kifu-replay', arg));

      socket.on('distributeKifu', (arg) => {
        this.notify('kifu-add', arg);
        this.notify('kifu-to-end', arg);
      });
    }

  }
}

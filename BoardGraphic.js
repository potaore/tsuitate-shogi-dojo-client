//Copyright (c) 2015-2016 potalong_oreo All rights reserved
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../potaore/artifact');
var base = require('./Base');
var wbase = require('./WebBase');
var BoardCanvas = (function (_super) {
    __extends(BoardCanvas, _super);
    function BoardCanvas(canvas) {
        var _this = this;
        _super.call(this, 'board-canvas');
        this.eventEffective = true;
        this.canvas = canvas;
        this.canvas.get(0).width = 640;
        this.canvas.get(0).height = 640;
        canvas.on('mousemove', function (e) {
            var rect = e.target.getBoundingClientRect();
            _this.mouseX = e.clientX - rect.left;
            _this.mouseY = e.clientY - rect.top;
        });
        canvas.on('mousedown', function (e) {
            if (_this.eventEffective) {
                var pos = base.util.posFlipX({ x: Math.floor((_this.mouseX - 5) / 70), y: Math.floor((_this.mouseY - 5) / 70) });
                _this.notify('user-mousedown-on-board', pos);
            }
            else {
                if (_this.mouseX < 320) {
                    _this.notify('board-kifu-back');
                }
                else {
                    _this.notify('board-kifu-next');
                }
            }
        });
    }
    BoardCanvas.prototype.init = function () {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
        this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
        this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    };
    BoardCanvas.prototype.putKoma = function (image, physicalPosition) {
        this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
    };
    BoardCanvas.prototype.checkCell = function (position) {
        this.ctx.fillStyle = 'rgba(256, 150, 150, 0.5)';
        this.ctx.fillRect(5 + position.x * 70, 5 + position.y * 70, 70, 70);
    };
    BoardCanvas.prototype.noticeOute = function () {
        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = 'rgba(256, 80, 80, 0.9)';
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0.7)';
        this.ctx.font = 'bold 48px "Arial"';
        this.ctx.textAlign = 'center';
        this.ctx.strokeText('王手！', this.canvas.get(0).width / 2, this.canvas.get(0).height / 2);
    };
    return BoardCanvas;
})(Artifact.Artifact);
exports.BoardCanvas = BoardCanvas;
var KomadaiCanvas = (function (_super) {
    __extends(KomadaiCanvas, _super);
    function KomadaiCanvas(canvas, num, imageManager) {
        var _this = this;
        _super.call(this, 'komadai');
        this.rects = [];
        this.eventEffective = false;
        this.num = num;
        this.canvas = canvas;
        this.imageManager = imageManager;
        this.canvas.get(0).width = 310;
        this.canvas.get(0).height = 64;
        canvas.on('mousemove', function (e) {
            if (!_this.eventEffective)
                return;
            var rect = e.target.getBoundingClientRect();
            _this.mouseX = e.clientX - rect.left;
            _this.mouseY = e.clientY - rect.top;
        });
        canvas.on('mousedown', function (e) {
            if (!_this.eventEffective)
                return;
            var hittedRect = _this.rects.filter(function (rect) { return rect.x < _this.mouseX && _this.mouseX < rect.x + rect.width && rect.y < _this.mouseY && _this.mouseY < rect.y + rect.height; });
            if (hittedRect.length > 0) {
                _this.notify('user-mousedown-on-komadai', hittedRect[0]);
            }
        });
    }
    KomadaiCanvas.prototype.init = function () {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
        this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
        this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    };
    KomadaiCanvas.prototype.drawKoma = function (komalist, flip) {
        var _this = this;
        this.init();
        var tmp = '';
        var list = null;
        var komaTypeList = [];
        komalist.forEach(function (koma) {
            if (tmp != koma.komaType) {
                if (list != null)
                    komaTypeList.push(list);
                list = [koma];
            }
            else {
                list.push(koma);
            }
            tmp = koma.komaType;
        });
        if (list != null)
            komaTypeList.push(list);
        var typeCount = 0;
        var ctx = this.ctx;
        this.rects = [];
        var rects = this.rects;
        komaTypeList.forEach(function (list) {
            var x = 10 + typeCount * 42;
            var y = 10;
            ctx.drawImage(_this.imageManager.getKomaImage(list[0], flip), x, y, 45, 45);
            var rect = { koma: list[0], x: x, width: 42, y: y, height: 45 };
            rects.push(rect);
            if (_this.emphasizeKoma) {
                if (_this.emphasizeKoma.komaType == list[0].komaType) {
                    _this.emphasizeRect(rect);
                    _this.emphasizeKoma = null;
                }
            }
            if (list.length > 1) {
                _this.circle(25 + typeCount * 42, 10, 7);
                _this.ctx.fillStyle = 'rgba(255, 255, 255, 0.99)';
                _this.ctx.font = 'bold 15px "Arial"';
                _this.ctx.textAlign = 'center';
                _this.ctx.fillText(list.length, 25 + typeCount * 42, 15);
            }
            typeCount++;
        });
    };
    KomadaiCanvas.prototype.circle = function (x, y, r) {
        var ctx = this.canvas.get(0).getContext('2d');
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#0275d8';
        ctx.fill();
        ctx.lineWidth = 5;
        ctx.strokeStyle = '#0275d8';
        ctx.stroke();
    };
    KomadaiCanvas.prototype.checkRect = function (rect) {
        this.ctx.lineWidth = 5;
        this.ctx.strokeStyle = 'rgba(256, 150, 150, 0.5)';
        this.ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
    };
    KomadaiCanvas.prototype.emphasizeRect = function (rect) {
        this.ctx.lineWidth = 5;
        this.ctx.font = 'bold 15px "Arial"';
        this.ctx.strokeStyle = 'rgba(0, 255, 255, 0.8)';
        this.ctx.strokeRect(rect.x + 2, rect.y, rect.width, rect.height);
    };
    KomadaiCanvas.prototype.emphasize = function (koma) {
        this.emphasizeKoma = koma;
    };
    return KomadaiCanvas;
})(Artifact.Artifact);
exports.KomadaiCanvas = KomadaiCanvas;
var BoardGraphicManager = (function (_super) {
    __extends(BoardGraphicManager, _super);
    function BoardGraphicManager(board, komadai1, komadai2, imageManager, domFinder) {
        _super.call(this, 'BoardGraphicManager');
        this.params = { flip: false, oute: false, blind: 0 };
        this.memo = { game: null };
        this.playerInfoMemo = null;
        this.gameInfoMemo = null;
        this.moveToPositions = [];
        this.putToPositions = [];
        this.boardCanvas = new BoardCanvas(board);
        this.komadai1Canvas = new KomadaiCanvas(komadai1, 1, imageManager);
        this.komadai2Canvas = new KomadaiCanvas(komadai2, 2, imageManager);
        this.imageManager = imageManager;
        this.domFinder = domFinder;
        this.addEvents();
        this.listen(this.boardCanvas)
            .listen(this.komadai1Canvas)
            .listen(this.komadai2Canvas);
    }
    BoardGraphicManager.prototype.addEvents = function () {
        var _this = this;
        this.on('game-redraw-board', function (eventName, sender, options) { return _this.redrawKoma(options); });
        this.on('game-board-initialize-table-state', function () { return _this.initializeTableState(); });
        this.on('game-board-set-invated-position', function (eventName, sender, invatedPosition) {
            _this.invatedPosition = invatedPosition;
        });
        //graphicApi.blindKoma
        this.on('game-blind-koma', function (eventName, sender, arg) {
            _this.params.blind = arg.blind;
            _this.flipBoard(arg.flip);
            _this.resetPlayerInfo();
            _this.resetGameInfo();
        });
        this.on('game-board-init', function () { return _this.init(); });
        this.on('game-board-flip', function (eventName, sender, flip) { return _this.flipBoard(flip); });
        this.on('game-notice-oute', function (eventName, sender, command) { return _this.params.oute = command.oute; });
        this.on('receive-game-start', function (eventName, sender, arg) {
            _this.invatedPosition = null;
            _this.boardCanvas.eventEffective = true;
            _this.komadai1Canvas.eventEffective = true;
            _this.komadai2Canvas.eventEffective = true;
            _this.setPlayerInfo(arg);
            _this.setTurn(0);
        });
        this.on('game-restart', function (eventName, sender, arg) {
            _this.invatedPosition = null;
            _this.boardCanvas.eventEffective = true;
            _this.komadai1Canvas.eventEffective = true;
            _this.komadai2Canvas.eventEffective = true;
            _this.setPlayerInfo(arg);
            _this.updateGameInfo(arg.playerInfo);
            _this.setTurn(arg.turn - 1);
            if (arg.state && arg.state.oute) {
                _this.params.oute = true;
            }
            if (arg.state && arg.state.getKoma) {
                _this.invatedPosition = arg.state.emphasisPosition;
            }
        });
        this.on('receive-kifu-replay', function (eventName, sender, arg) {
            _this.boardCanvas.eventEffective = false;
            _this.komadai1Canvas.eventEffective = false;
            _this.komadai2Canvas.eventEffective = false;
            _this.setPlayerInfo(arg.kifu);
        });
        this.on('receive-game-end', function (eventName, sender, arg) {
            _this.boardCanvas.eventEffective = false;
            _this.komadai1Canvas.eventEffective = false;
            _this.komadai2Canvas.eventEffective = false;
            _this.eraseTurnEmphasization();
        });
        this.on('game-set-turn', function (eventName, sender, turn) {
            _this.setTurn(turn);
        });
        this.on('game-update-info', function (eventName, sender, data) {
            _this.updateGameInfo(data);
        });
        this.on('game-get-koma', function (eventName, sender, command) {
            if (command.playerNumber == 1) {
                var komadai1 = _this.params.flip ? _this.komadai2Canvas : _this.komadai1Canvas;
                komadai1.emphasize(command.koma);
            }
            else {
                var komadai2 = _this.params.flip ? _this.komadai1Canvas : _this.komadai2Canvas;
                komadai2.emphasize(command.koma);
            }
        });
        this.on('user-mousedown-on-board', function (eventName, sender, pos) {
            pos = base.util.posFlip(pos, _this.params.flip);
            var moveToPos = base.util.find(_this.moveToPositions, function (p) { return base.util.posEq(pos, p); });
            if (moveToPos) {
                _this.moveKoma(moveToPos);
                return;
            }
            var putToPos = base.util.find(_this.putToPositions, function (p) { return base.util.posEq(pos, p); });
            if (putToPos) {
                _this.putKoma(putToPos);
                return;
            }
            _this.showMovableCell(pos);
        });
        this.on('user-mousedown-on-komadai', function (eventName, sender, rect) {
            _this.showPutableCell(rect);
        });
        this.on('board-kifu-next', function () { return _this.notify('kifu-next'); });
        this.on('board-kifu-back', function () { return _this.notify('kifu-back'); });
    };
    BoardGraphicManager.prototype.init = function () {
        this.invatedPosition = null;
        this.params = { flip: false, oute: false, blind: 0 };
        this.memo = { game: null };
        this.flipBoard(false);
    };
    BoardGraphicManager.prototype.redrawKoma = function (arg) {
        this.memo.game = arg;
        this.initializeTableState();
        this.clearKoma();
        this.drawKoma(arg);
    };
    BoardGraphicManager.prototype.reredrawKoma = function () {
        if (this.memo.game) {
            this.redrawKoma(this.memo.game);
        }
    };
    BoardGraphicManager.prototype.drawKoma = function (arg) {
        var _this = this;
        var blind = arg.board.endGame ? 0 : this.params.blind;
        if (this.invatedPosition) {
            var cancel = false;
            if (blind) {
                var myTurn = (arg.board.turn % 2 + 1) == this.params.blind;
                if (!arg.board.events || !myTurn && !arg.board.events.some(function (ev) { return ev.type == 'get'; })) {
                    cancel = true;
                }
            }
            if (!cancel) {
                this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(this.invatedPosition, this.params.flip)));
            }
        }
        var komadai1 = this.params.flip ? this.komadai2Canvas : this.komadai1Canvas;
        var komadai2 = this.params.flip ? this.komadai1Canvas : this.komadai2Canvas;
        if (blind != 1) {
            komadai1.drawKoma(arg.board.komaDai[0], this.params.flip);
        }
        if (blind != 2) {
            komadai2.drawKoma(arg.board.komaDai[1], this.params.flip);
        }
        base.util.allPos(function (x, y) {
            var koma = arg.board.getKoma({ x: x, y: y });
            if (koma && koma.playerNumber != blind) {
                _this.boardCanvas.putKoma(_this.imageManager.getKomaImage(koma, _this.params.flip), base.util.posFlipX(base.util.posFlip({ x: x, y: y }, _this.params.flip)));
            }
        });
        if (this.params.oute) {
            this.boardCanvas.noticeOute();
        }
    };
    BoardGraphicManager.prototype.showMovableCell = function (fromPos) {
        var _this = this;
        //gameApi.getGame
        this.notify('get-game', null, function (result) {
            var game = result.options;
            var koma = game.board.getKoma(fromPos);
            if (!koma || koma.playerNumber == (game.turn % 2 + 1) || game.state != 'playing')
                return;
            _this.initializeTableState();
            _this.fromPosition = fromPos;
            var positions = base.moveConvertor.getMovablePos(koma.playerNumber, koma.komaType, fromPos, game.board);
            _this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(fromPos, _this.params.flip)));
            positions.forEach(function (pos) { return _this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(pos, _this.params.flip))); });
            _this.moveToPositions = positions;
            _this.drawKoma(game);
        });
    };
    BoardGraphicManager.prototype.moveKoma = function (pos) {
        var _this = this;
        this.notify('get-game', null, function (result) {
            var game = result.options;
            var fromPos = base.util.posDup(_this.fromPosition);
            var toPos = base.util.posDup(pos);
            var fromKoma = game.board.getKoma(fromPos);
            var moveInfo = {
                from: {
                    position: fromPos,
                    komaType: fromKoma.komaType
                },
                to: {
                    position: toPos,
                    komaType: fromKoma.komaType
                },
                receiveCommnadCount: game.receiveCommnadCount
            };
            var nari = base.moveConvertor.getNari(fromKoma.playerNumber, fromKoma.komaType, moveInfo.from.position, moveInfo.to.position);
            if (nari == 'force') {
                moveInfo.to.komaType = base.komaTypes[moveInfo.to.komaType].nariId;
            }
            else if (nari == 'possible') {
                _this.notify('board-show-nari-modal', {
                    nari: function () {
                        moveInfo.to.komaType = base.komaTypes[moveInfo.to.komaType].nariId;
                        _this.notify('user-game-try-move-koma', moveInfo);
                    },
                    notNari: function () {
                        _this.notify('user-game-try-move-koma', moveInfo);
                    },
                    cancel: function () {
                    }
                });
                return;
            }
            _this.notify('user-game-try-move-koma', moveInfo);
        });
    };
    BoardGraphicManager.prototype.showPutableCell = function (rect) {
        var _this = this;
        var playerNumber = rect.koma.playerNumber;
        var komaType = rect.koma.komaType;
        this.notify('get-game', null, function (result) {
            var game = result.options;
            if (playerNumber == (game.turn % 2 + 1) || game.state != 'playing')
                return;
            _this.initializeTableState();
            var positions = base.moveConvertor.getPutableCell(playerNumber, komaType, game.board);
            if (komaType == 'hu') {
                var columns = game.board.getExistsHuColumn(playerNumber);
                positions = positions.filter(function (p) { return columns[p.x] ? false : true; });
            }
            positions.forEach(function (pos) { return _this.boardCanvas.checkCell(base.util.posFlipX(base.util.posFlip(pos, _this.params.flip))); });
            _this.putToPositions = positions;
            _this.drawKoma(game);
            _this.putKomaType = komaType;
            _this.komadai1Canvas.checkRect(rect);
        });
    };
    BoardGraphicManager.prototype.putKoma = function (pos) {
        var moveInfo = {
            to: {
                position: pos,
                komaType: this.putKomaType
            }
        };
        //gameApi.tryPutKoma
        this.notify('user-game-try-put-koma', moveInfo);
    };
    BoardGraphicManager.prototype.clearKoma = function () {
        this.boardCanvas.init();
        this.komadai1Canvas.init();
        this.komadai2Canvas.init();
    };
    BoardGraphicManager.prototype.initializeTableState = function () {
        this.moveToPositions = [];
        this.fromPosition = null;
        this.putToPositions = [];
        this.putKomaType = '';
        this.boardCanvas.init();
    };
    BoardGraphicManager.prototype.flipBoard = function (flip) {
        this.params.flip = flip;
        this.reredrawKoma();
        if (flip) {
            this.domFinder.getFlipItem().show();
            this.domFinder.getNotFlipItem().hide();
        }
        else {
            this.domFinder.getFlipItem().hide();
            this.domFinder.getNotFlipItem().show();
        }
    };
    BoardGraphicManager.prototype.setPlayerInfo = function (command) {
        this.playerInfoMemo = command.account;
        this.domFinder.getPlayerImageDiv(1, this.params.flip).empty();
        this.domFinder.getPlayerImageDiv(2, this.params.flip).empty();
        this.domFinder.getPlayerNameDiv(1, this.params.flip).empty();
        this.domFinder.getPlayerNameDiv(2, this.params.flip).empty();
        this.domFinder.getPlayerImageDiv(1, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player1.profile_url, this.playerInfoMemo.player1.character));
        this.domFinder.getPlayerNameDiv(1, this.params.flip).append(wbase.replaceToRuby(this.playerInfoMemo.player1.name));
        this.domFinder.getPlayerImageDiv(2, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player2.profile_url, this.playerInfoMemo.player2.character));
        this.domFinder.getPlayerNameDiv(2, this.params.flip).append(wbase.replaceToRuby(this.playerInfoMemo.player2.name));
    };
    BoardGraphicManager.prototype.resetPlayerInfo = function () {
        if (this.playerInfoMemo)
            this.setPlayerInfo({ account: this.playerInfoMemo });
    };
    BoardGraphicManager.prototype.setTurn = function (turn) {
        this.domFinder.getGameTurnDiv().empty();
        if (turn) {
            this.domFinder.getGameTurnDiv().append(turn + '手');
        }
        else {
            this.domFinder.getGameTurnDiv().append('対局開始');
        }
        var playerTurn = turn % 2;
        this.domFinder.getPlayerNameDiv(1 + playerTurn, this.params.flip).addClass('label-success');
        this.domFinder.getPlayerNameDiv(playerTurn, this.params.flip).removeClass('label-success');
    };
    BoardGraphicManager.prototype.eraseTurnEmphasization = function () {
        this.domFinder.getPlayerNameDiv(1, this.params.flip).removeClass('label-success');
        this.domFinder.getPlayerNameDiv(2, this.params.flip).removeClass('label-success');
    };
    BoardGraphicManager.prototype.updateGameInfo = function (command) {
        var emphasize1 = false;
        var emphasize2 = false;
        if (this.gameInfoMemo) {
            emphasize1 = command.player1.life < this.gameInfoMemo.player1.life;
            emphasize2 = command.player2.life < this.gameInfoMemo.player2.life;
        }
        this.gameInfoMemo = {
            player1: {
                time: command.player1.time,
                life: command.player1.life
            },
            player2: {
                time: command.player2.time,
                life: command.player2.life
            }
        };
        this.domFinder.getPlayerTimeDiv(1, this.params.flip).empty();
        this.domFinder.getPlayerTimeDiv(2, this.params.flip).empty();
        this.domFinder.getPlayerLifeLabel(1, this.params.flip).empty();
        this.domFinder.getPlayerLifeLabel(2, this.params.flip).empty();
        this.domFinder.getPlayerTimeDiv(1, this.params.flip).append(this.computeDuration(command.player1.time));
        this.domFinder.getPlayerTimeDiv(2, this.params.flip).append(this.computeDuration(command.player2.time));
        this.domFinder.getPlayerLifeLabel(1, this.params.flip).append(command.player1.life);
        this.domFinder.getPlayerLifeLabel(2, this.params.flip).append(command.player2.life);
        if (emphasize1) {
            console.log('emphasize1');
            this.emphasizePlayerLife(1);
        }
        if (emphasize2) {
            console.log('emphasize2');
            this.emphasizePlayerLife(2);
        }
    };
    BoardGraphicManager.prototype.emphasizePlayerLife = function (playerNumber) {
        var _this = this;
        this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).removeClass('label-primary', 150);
        this.domFinder.getPlayerLifeDiv(playerNumber, this.params.flip).addClass('label-danger', 150);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 150);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-primary', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-danger', 150);
        }, 300);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 450);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-primary', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-danger', 150);
        }, 600);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 750);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-primary', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-danger', 150);
        }, 900);
        setTimeout(function () {
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).removeClass('label-danger', 150);
            _this.domFinder.getPlayerLifeDiv(playerNumber, _this.params.flip).addClass('label-primary', 150);
        }, 1050);
    };
    BoardGraphicManager.prototype.resetGameInfo = function () {
        if (this.gameInfoMemo)
            this.updateGameInfo(this.gameInfoMemo);
    };
    BoardGraphicManager.prototype.computeDuration = function (ms) {
        if (ms < 0)
            ms = 0;
        var h = parseInt(new String(Math.floor(ms / 3600000) + 100).substring(1));
        var m = parseInt(new String(Math.floor((ms - h * 3600000) / 60000) + 100).substring(1));
        var s = new String(Math.floor((ms - h * 3600000 - m * 60000) / 1000) + 100).substring(1);
        return m + ':' + s;
    };
    return BoardGraphicManager;
})(Artifact.Artifact);
exports.BoardGraphicManager = BoardGraphicManager;

exports.replaceToRuby = function (str) {
    str = exports.escape(str);
    return str.replace(/(.+?)《(.+?)》/g, '<ruby><rb>$1</rb><rp>（</rp><rt>$2</rt><rp>）</rp></ruby>');
};
exports.escape = function (str) {
    return $('<div>').text(str).html();
};
